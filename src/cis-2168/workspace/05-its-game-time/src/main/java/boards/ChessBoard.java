package boards;

/**
 * Board formatted for Chess.
 *
 * @author Eric Nguyen
 */
public class ChessBoard extends Board {
    protected boolean showLabels;

    public ChessBoard(int numRows, int numCols) {
        super(numRows, numCols);
        this.showLabels = true;
    }

    /**
     * Make a copy of this ChessBoard.
     * @return A copy of the board
     */
    public ChessBoard copy() {
        ChessBoard copy = new ChessBoard(this.numRows, this.numCols);
        for (int r = 0; r < this.numRows; r++) {
            for (int c = 0; c < this.numCols; c++) {
                copy.setElement(r, c, this.grid[r][c]);
            }
        }
        return copy;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        // Table head
        if (this.showLabels) {
            builder.append(toStringOrgHead());
            builder.append(toStringOrgHeadSeparator());
        }

        // Table rows
        builder.append(toStringOrgRows());

        return builder.toString();
    }

    private String toStringOrgHead() {
        StringBuilder builder = new StringBuilder();

        builder.append("| ");
        for (int n = 0; n < ("" + this.numRows).length(); n++) {
            builder.append(' ');
        }
        builder.append(" | ");
        for (int c = 0; c < this.numCols; c++) {
            builder.append((c + 97) > '\u0080' && (c + 97) < '¡' ? ' ' : (char) (c + 97));
            builder.append(" | ");
        }
        builder.append('\n');

        return builder.toString();
    }

    private String toStringOrgHeadSeparator() {
        StringBuilder builder = new StringBuilder();

        // Table head-rows separator first value
        builder.append("|-");
        for (int n = 0; n < ("" + this.numRows).length(); n++) {
            builder.append('-');
        }
        builder.append("-+");

        // Table head-rows separator
        for (int c = 1; c < this.numCols; c++) {
            builder.append('-');
            for (int n = 0; n < ("" + maxColumnValue(c)).length(); n++) {
                builder.append('-');
            }
            builder.append("-+");
        }

        // Table head-rows separator last value
        builder.append('-');
        for (int n = 0; n < ("" + maxColumnValue(this.numCols - 1)).length(); n++) {
            builder.append('-');
        }
        builder.append("-|\n");

        return builder.toString();
    }

    private String toStringOrgRowNumber(int row) {
        StringBuilder builder = new StringBuilder();

        builder.append("| ");
        for (int i = 0; i < ("" + this.numRows).length() - ("" + (this.numRows - row)).length(); i++) {
            builder.append(' ');
        }
        builder.append(this.numRows - row);
        builder.append(" | ");

        return builder.toString();
    }

    private String toStringOrgEntry(int row, int col) {
        StringBuilder builder = new StringBuilder();

        if (!this.showZeros && this.grid[row][col] == 0) {
            builder.append(this.zeroAlt);
        } else {
            builder.append(this.grid[row][col]);
        }

        return builder.toString();
    }

    private String toStringOrgRows() {
        StringBuilder builder = new StringBuilder();

        for (int r = 0; r < this.numRows; r++) {
            // Row number
            if (this.showLabels) {
                builder.append(toStringOrgRowNumber(r));
            } else {
                builder.append("| ");
            }

            // Row entries
            for (int c = 0; c < this.numCols - 1; c++) {
                builder.append(toStringOrgEntry(r, c));
                builder.append(" | ");
            }

            // Last entry
            builder.append(toStringOrgEntry(r, this.numCols - 1));
            builder.append(" |");

            // Don't add extra new line
            if (r < this.numRows - 1) {
                builder.append('\n');
            }
        }

        return builder.toString();
    }

    // Helper method for toString()
    private int maxColumnValue(int column) {
        if (this.getColumn(column).length <= 0) return 0;
        int max = this.getColumn(column)[0];
        for (int i = 1; i < this.getColumn(column).length; i++) {
            if (this.getColumn(column)[i] > max) {
                max = this.getColumn(column)[i];
            }
        }
        return max;
    }
}
