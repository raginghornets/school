package ohnominotaur;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayDeque;
import java.util.Deque;

public class MazeGridPanel extends JPanel {
	private int rows;
	private int cols;
	private Cell[][] maze;

	// Extra credit
	public void generateMazeByDFS() {
		boolean[][] visited;
		Deque<Cell> stack = new ArrayDeque<>();
		Cell start = maze[0][0];
		stack.push(start);
	}

	public void solveMaze() {
		Deque<Cell> stack = new ArrayDeque<Cell>();
		Cell start = maze[0][0];
		start.setBackground(Color.GREEN);
		Cell finish = maze[rows - 1][cols - 1];
		finish.setBackground(Color.RED);
		stack.push(start);

		// Implement your algorithm here
		for (int i = 0; i < rows * cols; i++) {
			int row = start.row;
			int col = start.col;
			if (start.northWall == false && !visited(row - 1, col)) {
				row -= 1;
			} else if (start.southWall == false && !visited(row + 1, col)) {
				row += 1;
			} else if (start.westWall == false && !visited(row, col - 1)) {
				col -= 1;
			} else if (start.eastWall == false && !visited(row, col + 1)) {
				col += 1;
			} else {
				start.setBackground(Color.GRAY); // Dead-end
				stack.pop();
				start = stack.peek();
				i--;
				continue;
			}

			start = maze[row][col];
			stack.push(start);

			if (start.equals(finish)) return;
			start.setBackground(Color.LIGHT_GRAY); // Current path
		}
	}

	public boolean visited(int row, int col) {
		Cell c = maze[row][col];
		Color status = c.getBackground();
		if (status.equals(Color.WHITE) || status.equals(Color.RED)) {
			return false;
		}

		return true;
	}

	public void generateMaze() {
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				if (row == 0 && col == 0) {
					continue;
				} else if (row == 0) {
					maze[row][col].westWall = false;
					maze[row][col - 1].eastWall = false;
				} else if (col == 0) {
					maze[row][col].northWall = false;
					maze[row - 1][col].southWall = false;
				} else {
					boolean north = Math.random() < 0.5;
					if (north) {
						maze[row][col].northWall = false;
						maze[row - 1][col].southWall = false;
					} else {
						maze[row][col].westWall = false;
						maze[row][col - 1].eastWall = false;
					}
					maze[row][col].repaint();
				}
			}
		}

		this.repaint();
	}

	public MazeGridPanel(int rows, int cols) {
		this.setPreferredSize(new Dimension(800, 800));
		this.rows = rows;
		this.cols = cols;
		this.setLayout(new GridLayout(rows, cols));
		this.maze = new Cell[rows][cols];
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				maze[row][col] = new Cell(row, col);
				this.add(maze[row][col]);
			}
		}

		this.generateMaze();
		this.solveMaze();
	}
}
