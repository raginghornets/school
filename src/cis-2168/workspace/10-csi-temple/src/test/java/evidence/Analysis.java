package evidence;

import sorting.SorterTest;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

public class Analysis {
    public static final String HEADERS = "size,duration,comparisons,exchanges";
    public static final int N = 17;

    public static void main(String[] args) throws IOException {
        SorterTest test = new SorterTest();
        Integer[] array;
        int i, size;
        long startTime, duration;
        Map profiler;

        // Collect data on Sort1
        PrintStream out1 = new PrintStream(new File("Sort1.csv"));
        out1.println(HEADERS);
        for (i = 1; i < N; i++) {
            size = (int) Math.pow(2, i);
            array = test.randomArray(size);
            startTime = System.nanoTime();
            profiler = Sort1.sort(array);
            duration = System.nanoTime() - startTime;
            out1.println(csvRow(size, duration, profiler.get("Comparisons"), profiler.get("Exchanges")));
        }
        out1.close();

        // Collect data on Sort2
        PrintStream out2 = new PrintStream(new File("Sort2.csv"));
        out2.println(HEADERS);
        for (i = 1; i < N; i++) {
            size = (int) Math.pow(2, i);
            array = test.randomArray(size);
            startTime = System.nanoTime();
            profiler = Sort2.sort(array);
            duration = System.nanoTime() - startTime;
            out2.println(csvRow(size, duration, profiler.get("Comparisons"), profiler.get("Exchanges")));
        }
        out2.close();

        // Collect data on Sort3
        PrintStream out3 = new PrintStream(new File("Sort3.csv"));
        out3.println(HEADERS);
        for (i = 1; i < N; i++) {
            size = (int) Math.pow(2, i);
            array = test.randomArray(size);
            startTime = System.nanoTime();
            profiler = Sort3.sort(array);
            duration = System.nanoTime() - startTime;
            out3.println(csvRow(size, duration, profiler.get("Comparisons"), profiler.get("Exchanges")));
        }
        out3.close();
    }

    public static <T> String csvRow(int size, long duration, T comparisons, T exchanges) {
        StringBuilder sb = new StringBuilder();
        sb.append(size);
        sb.append(',');
        sb.append(duration);
        sb.append(',');
        sb.append(comparisons);
        sb.append(',');
        sb.append(exchanges);
        return sb.toString();
    }
}
