package obfuscated.sorting;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Map;
import java.util.Random;

public class ObfuscatedSorterTest extends TestCase {
    public <T extends Comparable<T>> boolean isSorted(T[] list) {
        for (int i = 0; i < list.length - 1; i++) {
            if (list[i].compareTo(list[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    public <T extends Comparable<T>> long sortWithProfiler(Sorter sorter, T[] list) {
        long startTime = System.nanoTime();
        Map profiler = sorter.sort(list);
        long duration = System.nanoTime() - startTime;
        assert profiler.containsKey("comparisons");
        assert profiler.containsKey("exchanges");
        System.out.println("Number of comparisons: " + profiler.get("comparisons"));
        System.out.println("Number of exchanges: " + profiler.get("exchanges"));
        System.out.println("Duration of execution: " + duration + " ns");
        System.out.println();
        return duration;
    }

    public Integer[] randomArray(int size) {
        Random random = new Random();
        Integer[] list = new Integer[size];
        for (int i = 0; i < size; i++) {
            list[i] = random.nextInt();
        }
        return list;
    }

    public Integer[] sortArray(Sorter sorter, int size) {
        System.out.println("Sorting random array of " + size + " items");
        Integer[] list = randomArray(size);
        sortWithProfiler(sorter, list);
        return list;
    }

    public Integer[] sortReversedArray(Sorter sorter, int size) {
        System.out.println("Sorting reversed array of " + size + " items");
        Integer[] list = new Integer[size];
        for (int i = 0; i < size; i++) {
            list[i] = size - i;
        }
        sortWithProfiler(sorter, list);
        return list;
    }

    public Integer[] sortSortedArray(Sorter sorter, int size) {
        System.out.println("Sorting sorted array of " + size + " items");
        Integer[] list = new Integer[size];
        for (int i = 0; i < size; i++) {
            list[i] = i;
        }
        sortWithProfiler(sorter, list);
        return list;
    }

    public String[] sortStringArray(Sorter sorter) {
        System.out.println("Sorting array of 6 strings.");
        String[] list = {"oranges", "apples", "bananas", "lemons", "potatoes", "blueberries"};
        sortWithProfiler(sorter, list);
        return list;
    }

    @Test
    public void testSort1() {
        Sorter sorter = new Sort1();
        assertTrue(isSorted(sortArray(sorter, 10)));
        assertTrue(isSorted(sortArray(sorter, 100)));
        assertTrue(isSorted(sortArray(sorter, 1000)));
        assertTrue(isSorted(sortArray(sorter, 10000)));
        assertTrue(isSorted(sortReversedArray(sorter, 100)));
        assertTrue(isSorted(sortSortedArray(sorter, 100)));
        assertTrue(isSorted(sortStringArray(sorter)));
    }

    @Test
    public void testSort2() {
        Sorter sorter = new Sort2();
        assertTrue(isSorted(sortArray(sorter, 10)));
        assertTrue(isSorted(sortArray(sorter, 100)));
        assertTrue(isSorted(sortArray(sorter, 1000)));
        assertTrue(isSorted(sortArray(sorter, 10000)));
        assertTrue(isSorted(sortReversedArray(sorter, 100)));
        assertTrue(isSorted(sortSortedArray(sorter, 100)));
        assertTrue(isSorted(sortStringArray(sorter)));
    }

    @Test
    public void testSort3() {
        Sorter sorter = new Sort3();
        assertTrue(isSorted(sortArray(sorter, 10)));
        assertTrue(isSorted(sortArray(sorter, 100)));
        assertTrue(isSorted(sortArray(sorter, 1000)));
        assertTrue(isSorted(sortArray(sorter, 10000)));
        assertTrue(isSorted(sortReversedArray(sorter, 100)));
        assertTrue(isSorted(sortSortedArray(sorter, 100)));
        assertTrue(isSorted(sortStringArray(sorter)));
    }
}