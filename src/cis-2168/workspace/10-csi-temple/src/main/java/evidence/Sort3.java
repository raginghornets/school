package evidence;

import java.util.HashMap;
import java.util.Map;

public class Sort3 {


    public static <T extends Comparable<T>> Map<String, Integer> sort(T[] list) {
        long startTime = System.nanoTime();
        Map<String, Integer> map = new HashMap<>();

        map.put("Comparisons", 0);
        map.put("Exchanges", 0);

        for (int i = 1; i < list.length; i++) {
            T value = list[i];
            int j = i - 1;
            while (j >= 0 && value.compareTo(list[j]) < 0) {
                map.put("Comparisons", map.get("Comparisons") + 2);
                list[j + 1] = list[j];
                map.put("Exchanges", map.get("Exchanges") + 1);
                j--;
            }
            list[j + 1] = value;
            map.put("Comparisons", map.get("Comparisons") + 1);
        }
        map.put("Time", (int) (System.nanoTime() - startTime));
        return map;
    }
}