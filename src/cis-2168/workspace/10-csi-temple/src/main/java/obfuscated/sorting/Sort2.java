package obfuscated.sorting;

import java.util.HashMap;
import java.util.Map;

public class Sort2 implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] t) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);

        for (int i = 1; i < t.length; i++) {
            sortHelper(profiler, t, i);

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
        }

        return profiler;
    }

    private <T extends Comparable<T>> void sortHelper(Map<String, Integer> profiler, T[] t, int a) {
        T c = t[a];
        while (a > 0 && c.compareTo(t[a - 1]) < 0) {
            t[a] = t[a - 1];
            a--;

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            profiler.put(EXCHANGES, profiler.get(EXCHANGES) + 1);
        }
        t[a] = c;
    }
}
