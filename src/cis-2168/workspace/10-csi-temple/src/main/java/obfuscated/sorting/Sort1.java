package obfuscated.sorting;

import java.util.HashMap;
import java.util.Map;

public class Sort1 implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] t) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);

        int a = t.length / 2;
        while (a > 0) {
            for (int b = 0; b < t.length; b++) {
                sortHelper(profiler, t, b, a);

                profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            }
            a = (a == 2) ? 1 : (int) (a / 2.2);

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
        }

        return profiler;
    }

    private static <T extends Comparable<T>> void sortHelper(Map<String, Integer> profiler, T[] t, int b, int a) {
        T c = t[b];
        while (b > a - 1 && c.compareTo(t[b - a]) < 0) {
            t[b] = t[b - a];
            b -= a;

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            profiler.put(EXCHANGES, profiler.get(EXCHANGES) + 1);
        }
        t[b] = c;
    }

}
