package sorting;

import java.util.HashMap;
import java.util.Map;

/**
 * Shell sort algorithm from Pages 390-391 of the textbook.
 */
public class ShellSort implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] items) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);

        int gap = items.length / 2;
        while (gap > 0) {
            for (int start = 0; start < items.length; start++) {
                insertionSort(profiler, items, start, gap);

                profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            }
            gap = (gap == 2) ? 1 : (int) (gap / 2.2);

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
        }

        return profiler;
    }

    private static <T extends Comparable<T>> void insertionSort(Map<String, Integer> profiler, T[] items, int start, int gap) {
        T nextVal = items[start];
        while (start > gap - 1 && nextVal.compareTo(items[start - gap]) < 0) {
            items[start] = items[start - gap];
            start -= gap;

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            profiler.put(EXCHANGES, profiler.get(EXCHANGES) + 1);
        }
        items[start] = nextVal;
    }

}
