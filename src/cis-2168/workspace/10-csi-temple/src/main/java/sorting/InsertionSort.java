package sorting;

import java.util.HashMap;
import java.util.Map;

/**
 * Insertion sort algorithm from Pages 385-386 of the textbook.
 */
public class InsertionSort implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] list) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);

        for (int i = 1; i < list.length; i++) {
            insert(profiler, list, i);

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
        }

        return profiler;
    }

    private <T extends Comparable<T>> void insert(Map<String, Integer> profiler, T[] list, int nextPos) {
        T toBeInserted = list[nextPos];
        while (nextPos > 0 && toBeInserted.compareTo(list[nextPos - 1]) < 0) {
            list[nextPos] = list[nextPos - 1];
            nextPos--;

            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
            profiler.put(EXCHANGES, profiler.get(EXCHANGES) + 1);
        }
        list[nextPos] = toBeInserted;
    }
}
