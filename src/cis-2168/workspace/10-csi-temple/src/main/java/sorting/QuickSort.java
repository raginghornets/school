package sorting;

import java.util.HashMap;
import java.util.Map;

/**
 * Quicksort algorithm from Pages 411-417 of the textbook.
 */
public class QuickSort implements Sorter {

    private static final String COMPARISONS = "comparisons";
    private static final String EXCHANGES = "exchanges";

    @Override
    public <T extends Comparable<T>> Map<String, Integer> sort(T[] list) {
        Map<String, Integer> profiler = new HashMap<>();
        profiler.put(COMPARISONS, 0);
        profiler.put(EXCHANGES, 0);
        quickSort(profiler, list, 0, list.length - 1);
        return profiler;
    }

    private <T extends Comparable<T>> void quickSort(Map<String, Integer> profiler, T[] list, int first, int last) {
        if (first < last) {
            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
            int pivot = partition(profiler, list, first, last);
            quickSort(profiler, list, first, pivot - 1);
            quickSort(profiler, list, pivot + 1, last);
        }
    }

    private <T extends Comparable<T>> int partition(Map<String, Integer> profiler, T[] list, int first, int last) {
        partitionSort(profiler, list, first, last);
        swap(profiler, list, first, (first + last) / 2);

        T pivot = list[first];
        int up = first;
        int down = last;
        do {
            while (up < last && pivot.compareTo(list[up]) >= 0) {
                profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
                up++;
            }
            while (pivot.compareTo(list[down]) < 0) {
                profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 2);
                down--;
            }
            if (up < down) {
                profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
                swap(profiler, list, up, down);
            }
            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
        } while (up < down);

        swap(profiler, list, first, down);

        return down;
    }

    private <T> void swap(Map<String, Integer> profiler, T[] list, int a, int b) {
        T temp = list[a];
        list[a] = list[b];
        list[b] = temp;
        profiler.put(EXCHANGES, profiler.get(EXCHANGES) + 1);
    }

    private <T extends Comparable<T>> void partitionSort(Map<String, Integer> profiler, T[] list, int first, int last) {
        int middle = (first + last) / 2;
        if (list[first].compareTo(list[last]) >= 0) {
            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
            swap(profiler, list, first, last);
        }
        if (list[middle].compareTo(list[last]) >= 0) {
            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
            swap(profiler, list, middle, last);
        }
        if (list[first].compareTo(list[last]) >= 0) {
            profiler.put(COMPARISONS, profiler.get(COMPARISONS) + 1);
            swap(profiler, list, first, last);
        }
    }
}
