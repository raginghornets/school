public class Person extends Animal {

    private int direction;

    public Person(Model model, int row, int column) {
        super(model, row, column);
        direction = Model.STAY;
    }
    
    int decideMove() {
        int velociraptor = seesVelociraptor();
        direction = velociraptor > -1 ? escape(velociraptor) : continueMove();
        return direction;
    }

    /**
     * @return The direction in which the Person sees the velociraptor.
     * If the Person does not see the velociraptor, return -1.
     */
    int seesVelociraptor() {
        for (int i = Model.N; i < Model.STAY; i++) {
            if (look(i) == Model.VELOCIRAPTOR) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Escape the velociraptor given the direction in which it was spotted.
     * @param velociraptor The direction in which the velociraptor was spotted.
     * @return An optimal escape route direction, away from the velociraptor.
     */
    int escape(int velociraptor) {
        int escapeRoute = Model.turn(velociraptor, Model.SE);
        int maxDistance = distance(escapeRoute);
        for (int i = Model.NE; i < Model.NW; i++) {

            // Avoid dying
            if ((i < Model.E || i > Model.W) && distance(velociraptor) < 3) {
                continue;
            }
            if ((i < Model.SE || i > Model.SW) && distance(velociraptor) < 2 && isPerpendicular(velociraptor)) {
                continue;
            }

            int route = Model.turn(velociraptor, i);
            int d = distance(route);
            if (maxDistance < d || (maxDistance <= d && (look(route) != Model.EDGE || !isPerpendicular(route)))) {
                maxDistance = d;
                escapeRoute = route;
            }
        }
        return escapeRoute;
    }

    /**
     * @return The current direction if possible; otherwise a new direction.
     */
    int continueMove() {
        return (distance(direction) < 2 && direction != Model.STAY) ? changeMove() : direction;
    }

    /**
     * @return A new, different direction.
     */
    int changeMove() {
        int newRoute = Model.turn(direction, Model.NE);
        int maxDistance = distance(newRoute);
        for (int i = Model.E; i <= Model.NW; i++) {
            int route = Model.turn(direction, i);
            int d = distance(route);
            if (maxDistance < d) {
                maxDistance = d;
                newRoute = route;
            }
        }
        return distance(newRoute) < 2 ? changeMove() : newRoute;
    }

    /**
     * @param direction The input direction.
     * @return Whether or not the given direction is perpendicular to the Person.
     */
    boolean isPerpendicular(int direction) {
        switch (direction) {
            case Model.N:
            case Model.W:
            case Model.S:
            case Model.E:
                return true;
            default:
                return false;
        }
    }
}
