package indexingtext;

import java.util.ArrayList;
import java.util.List;

public class IndexNode {
	protected String word;
	protected int occurrences;
	protected List<Integer> list;
	protected IndexNode left;
	protected IndexNode right;

	protected IndexNode(String word, int lineNumber) {
		this.word = word;
		this.occurrences = 1;
		this.list = new ArrayList<>();
		this.list.add(lineNumber);
	}

	/**
	 * The string must be one line.
	 * @return The word, number of occurrences, and lines it appears on.
	 */
	public String toString() {
		return word + "(" + occurrences + ") " + list;
	}
}
