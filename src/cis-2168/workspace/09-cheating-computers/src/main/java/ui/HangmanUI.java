package ui;

import java.io.FileNotFoundException;

/**
 * Each Hangman game should have these methods.
 *
 * @author Eric Nguyen
 */
public abstract class HangmanUI {
    public abstract boolean play() throws FileNotFoundException;
    public abstract boolean playAgain() throws FileNotFoundException;
    protected abstract void promptUserForGuess();
}
