import java.util.List;
import java.util.ArrayList;

class Permutations {
    public static <E> boolean isPermutation(List<E> list1, List<E> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }

        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                if (list1.get(i) == list2.get(j)) {
                    list2.remove(j);
                    break;
                }
            }
        }

        return list2.size() <= 0;
    }

    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(4);

        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        list2.add(1);
        list2.add(4);

        System.out.println(isPermutation(list1, list2));
    }
}