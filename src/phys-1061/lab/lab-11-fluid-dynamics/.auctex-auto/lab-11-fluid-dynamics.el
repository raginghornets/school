(TeX-add-style-hook
 "lab-11-fluid-dynamics"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("geometry" "margin=1in")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "geometry")
   (LaTeX-add-labels
    "sec:org34cf0b7"
    "sec:orgcec5553"
    "sec:orgff6f9a3"
    "sec:orgf976c4e"
    "sec:org7db65f6"
    "sec:org182d989"
    "sec:org529f159"
    "sec:org92587bc"
    "sec:org647d38d"
    "sec:orge5bef94"
    "sec:org751db28"
    "sec:orge23062b"
    "sec:orgd85fdea"
    "sec:org0100bb8"
    "sec:org2832804"
    "sec:orgb364f42"
    "sec:orgb222145"
    "sec:org429ae87"
    "sec:org9dbe849"
    "sec:orgc5d7309"
    "sec:org5d49ee5"))
 :latex)

