#+TITLE: Lab Report 1 Motion in One Dimension
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* Introduction

The goal of this lab is to become familiar with video analysis of objects in motion and understand the physics of one-dimensional motion.

* Procedure

** Apparatus

Ball, tissue, smartphone, PASCO Capstone software.

** Part 1: Record the video

1. Find an open space to record the video.

2. Position the smartphone camera such that it can capture at least one meter of vertical space and then start recording.

3. Stand far enough in front of the camera, so that it captures the full motion of the falling objects.

4. Simultaneously drop the ball and tissue from the same height, around one meter above the ground.

5. Stop recording immediately after both objects have landed.

** Part 2: Video Analysis

1. Open Capstone and import the recorded video as a Movie.

2. Enter video analysis mode and set the coordinates and distance scale using the yellow axes and caliper tools respectively.

3. Repeatedly click on the ball's position as shown in the video to record its position for each frame.

4. Create a new object for the tissue and repeat the previous step, also recording its position for each frame.

** Part 3: Data Analysis

1. Make a table in Capstone including the time and the corresponding \(y\) positions of each object.

2. In Capstone, plot of the \(y\) position of each object over time within the same graph.

3. Use the highlighter tool to select the last five points of the tissue's \(y\) position on the graph.

4. Use the fitting tool to plot a linear fit with the selected points.
   This represents the terminal velocity of the tissue.

5. For the ball, click on each of its points to bring up the "Add Slope Tool" button and observe how the slope changes for each successive point.
   The slopes represent the instantaneous velocity of the ball and their change between each point represents the ball's acceleration.

6. Plot together the \(y\) velocity of each object over time in a new graph.

7. In the data table, add a new column to show the \(y\) velocity of the tissue, displaying its instantaneous velocity values.

* Precautions

- Make sure that the objects are appropriate for the experiment.

- Ensure that the camera is able to catch the full motion of both objects.

- Carefully input positions for each object, making sure nothing seems off.

* Error

There may have been errors produced in the analysis of the tissue's motion as it was spread out, rather than in the form of a ball.
Not only does this make it difficult to enter in the tissue's position, but it affects its motions in unexpected ways.
The effects of this decision on the tissue's motion is apparent in the data.

* Data

** Position vs. time

#+CAPTION: Position vs. time graph of the ball and tissue
[[./position-vs-time-ball-and-tissue.png]]

| Time (s) | \(y\), Ball (m) | \(y\), Tissue (m) |
|----------+-----------------+-------------------|
|    0.600 |            0.87 |              0.86 |
|    0.633 |            0.87 |              0.85 |
|    0.666 |            0.86 |              0.83 |
|    0.700 |            0.84 |              0.81 |
|    0.733 |            0.81 |              0.79 |
|    0.766 |            0.77 |              0.78 |
|    0.800 |            0.73 |              0.74 |
|    0.833 |            0.67 |              0.70 |
|    0.866 |            0.61 |              0.67 |
|    0.900 |            0.54 |              0.64 |
|    0.933 |            0.45 |              0.61 |
|    0.966 |            0.37 |              0.58 |
|    1.000 |            0.27 |              0.54 |
|    1.033 |            0.16 |              0.51 |
|    1.066 |            0.04 |              0.47 |

*Observation:* For each successive position of the ball, its slope seems to get steeper.
This suggests that the ball's acceleration is negative.

** Velocity vs. time

#+CAPTION: Velocity vs. time graph of the ball and tissue
[[./velocity-vs-time-ball-and-tissue.png]]

| Time (s) | \(v_y\), Ball (m/s) | \(v_y\), Tissue (m/s) |
|----------+---------------------+-----------------------|
|    0.633 |               -0.21 |                 -0.45 |
|    0.666 |               -0.48 |                 -0.54 |
|    0.700 |               -0.70 |                 -0.54 |
|    0.733 |               -0.97 |                 -0.56 |
|    0.766 |               -1.26 |                 -0.79 |
|    0.800 |               -1.49 |                 -1.09 |
|    0.833 |               -1.78 |                 -1.08 |
|    0.866 |               -1.99 |                 -0.98 |
|    0.900 |               -2.36 |                 -0.97 |
|    0.933 |               -2.63 |                 -0.94 |
|    0.966 |               -2.79 |                 -0.95 |
|    1.000 |               -3.06 |                 -1.03 |
|    1.033 |               -3.43 |                 -1.06 |

* Questions

** Question 1

| Time (s) | \(\Delta y\), Ball (m) | \(\Delta y\), Tissue (m) |
|----------+------------------------+--------------------------|
|    0.600 |                   0.00 |                     0.00 |
|    0.633 |                   0.00 |                     0.01 |
|    0.666 |                   0.01 |                     0.02 |
|    0.700 |                   0.02 |                     0.02 |
|    0.733 |                   0.03 |                     0.02 |
|    0.766 |                   0.04 |                     0.01 |
|    0.800 |                   0.04 |                     0.04 |
|    0.833 |                   0.06 |                     0.04 |
|    0.866 |                   0.06 |                     0.03 |
|    0.900 |                   0.07 |                     0.03 |
|    0.933 |                   0.09 |                     0.03 |
|    0.966 |                   0.07 |                     0.03 |
|    1.000 |                   0.10 |                     0.04 |
|    1.033 |                   0.11 |                     0.03 |
|    1.066 |                   0.12 |                     0.04 |

The successive position values do not change much as motion progresses for the tissue, however they do change significantly for the ball.
This is the expected behavior assuming that the tissue reaches a terminal velocity since that implies that the change in position over the the change in time has reached a maximum value which indeed appears to be the case as shown in the data.
For example, the tissue's successive changes in positions max out at around 0.04 m between each frame.

** Question 2

The data series in the graph appear to have a different shape.
Specifically, the tissue's data is much straighter whereas the ball's data appears to curve down.

** Question 3

The linear fit lines up well with the last five points of the tissue's data.
The slope of the linear fit represents the tissue's terminal velocity.

** Question 4

The data series in the velocity graph both appear to be straight, however along different directions.
The tissue's velocity appears to follow along a line parallel to the \(x\)-axis whereas the ball's velocity appears to follow a diagonal line going from the origin and downwards.

** Question 5

The slope of the tissue's velocity vs. time data should be zero if it does reach terminal velocity on its way down since if it has reached terminal velocity then its velocity would no longer change, thus zero slope.

** Question 6

The slope of a velocity vs. time graph represents the object's acceleration, that is, change in velocity.

** Question 7

With the framerate and distance calibration, Capstone takes the position data relative to the distance calibration and divides it over the elapsed time for each frame, calculating the velocity of the object.

** Question 8

The instantaneous velocity values for the last five data points of the tissue's velocity are a little higher than the average velocity I got from the slope of the linear fit.
They seem to not be exactly consistent with each other.

* Conclusion

In this lab, I learned how to use the PASCO Capstone software to perform video analysis on the motion of objects in one-dimension.
I discovered how position, velocity, and acceleration relate to one another and through that, have a fundamentally better understanding of each concept.

I also found that the actual results matched that of the expected results.
That is, the ball was expected to have a constant negative acceleration due to gravity and the tissue was expected to reach zero acceleration due to terminal velocity, which indeed happen in both cases.

The fit of the data matches well with the shape of the data.
Specifically, the tissue's position data is quite linear, so obviously a linear fit should match it well.

It is difficult to know the reproducibility of my results given that the behavior of the falling tissue is rather complex.
However, in general, the basic video analysis of two falling objects with drastically different behaviors when falling, can be reproduced fairly easily.

Since I decided to analyze the motion of a tissue, it fell in an unpredictable way and made it difficult to track its position.
Because of this, it most likely caused errors or different results than the lab manual expected.
