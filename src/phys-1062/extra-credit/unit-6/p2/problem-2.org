#+TITLE: Problem 2
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

*Instructions:* Solve the problem below using the IRDEA framework.
*Problem 2:* A battery with an emf of 60 V is connected to the two capacitors shown in Figure.
Afterward, the charge on capacitor 2 is 450 μC.
What is the capacitance of capacitor 2?

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - \(C_1\) and \(C_2\) are in series.

   - Use QVC theorem.

2) Represent the problem in terms of symbols and diagrams.

   #+ATTR_LATEX: :width 5cm
   [[./diagram.png]]
   \begin{align*}
     \Delta V_b &= 60 \text{ V} \\
     C_1 &= 12 \;\mu\text{F} \\
     Q_2 &= 450 \;\mu\text{C} \\
     Q_1 &= Q_2 \\
     C_2 &= \;?
   \end{align*}

3) Develop a plan for solving the problem (*Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics*; No calculations in the development stage)

   \begin{align*}
     \Delta V_1 &= \frac{Q_1}{C_1} \\
     \Delta V_b &= \Delta V_1 + \Delta V_2 \\
     \Delta V_2 &= \Delta V_b - \Delta V_1 \\
     C_2 &= \frac{Q_2}{\Delta V_2}
   \end{align*}

4) Evaluate your answer (No units involved when performing calculations)

   \begin{align*}
     \Delta V_1 &= \frac{0.00045}{0.000012} = 37.5 \text{ V} \\
     \Delta V_2 &= 60 - 37.5 = 22.5 \text{ V} \\
     C_2 &= \frac{0.00045}{22.5} = 0.000020 \text{ F} = 20 \;\mu\text{F}
   \end{align*}

5) Assess your answer (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?)

   - 2 sig figs.

   - Units in \(\mu\)F.

   - Reasonable: answer is in same order of magnitude as \(C_1\)
