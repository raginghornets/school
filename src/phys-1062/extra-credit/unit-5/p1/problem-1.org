#+TITLE: Problem 1
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent Instructions: Solve the problem below using the IRDEA framework.

\noindent *Problem 1*: Three charged particles are placed at the corners of an equilateral triangle of side 1.20 m (see Fig.).
The charges are + 7.0 μC, -8.0 μC, and -6.0 μC.
Calculate the magnitude and direction of the net force on \(Q_3\) due to the other two.

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Electric field problem.

   - \(Q_1\) will attract \(Q_3\) and \(Q_2\) will repel \(Q_3\).

   - Find magnitude and direction of net force on \(Q_3\).

   - \(Q_2\) will have a larger force than \(Q_1\) on \(Q_3\).

   - Predict. The magnitude of the net force will be less than the magnitude of force from \(Q_2\) on \(Q_3\) and the direction will be in the first quadrant of the coordinate system.

2) Represent the problem in terms of symbols and diagrams. (Make a SKETCH)

   - \(Q_1 = +7.0 \;\mu\text{C}\)
   - \(Q_2 = -8.0 \;\mu\text{C}\)
   - \(Q_3 = -6.0 \;\mu\text{C}\)
   - \(r = 1.20 \text{ m}\)
   - \(\theta = 60^{\circ}\)

     #+ATTR_LATEX: :width 4cm
     [[./diagram.png]]

3) Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics; No calculations in the development stage)

   - We can find the magnitudes of the forces on \(Q_3\) using Coulomb's Law.
     \begin{align*}
       F_{1 \text{ on } 3} &= \frac{K|Q_1||Q_3|}{r^2} \\
       F_{2 \text{ on } 3} &= \frac{K|Q_2||Q_3|}{r^2}
     \end{align*}

   - We can represent the forces as vectors.
     \begin{align*}
       \vec{F}_{1 \text{ on } 3} &= -F_{1 \text{ on } 3} \cos{\theta} \hat{i} + F_{1 \text{ on } 3} \sin{\theta} \hat{j} \\
       \vec{F}_{2 \text{ on } 3} &= F_{2 \text{ on } 3} \hat{i}
     \end{align*}

   - We can add the two vectors together to find the force on \(Q_3\).
     \[\vec{F}_3 = \vec{F}_{1 \text{ on } 3} + \vec{F}_{2 \text{ on } 3}\]

   - Finally, we use the vector components of \(\vec{F}_3\) to determine its magnitude and direction.
     \begin{align*}
       F_3 &= \sqrt{{F_{3_x}}^2 + {F_{3_y}}^2} \\
       \phi &= \tan^{-1}{\left|\frac{F_{3_y}}{F_{3_x}}\right|}
     \end{align*}

4) Evaluate your answer (No units involved in the evaluation)
   \begin{align*}
     F_{1 \text{ on } 3} &= \frac{(8.99 \times 10^9)(7.0 \times 10^{-6})(6.0 \times 10^{-6})}{(1.20)^2} = 0.262 \text{ N} \\
     F_{2 \text{ on } 3} &= \frac{(8.99 \times 10^9)(8.0 \times 10^{-6})(6.0 \times 10^{-6})}{(1.20)^2} = 0.300 \text{ N}
   \end{align*}

   \begin{align*}
     \vec{F}_{1 \text{ on } 3} &= -(2.62) \cos{(60^{\circ})} \hat{i} + (2.62) \sin{(60^{\circ})} \hat{j} \\
       &= -0.131 \hat{i} + 0.227 \hat{j} \\
     \vec{F}_{2 \text{ on } 3} &= 3.00 \hat{i} \\
     \vec{F}_3 &= (-1.31 + 3.00) \hat{i} + 2.27 \hat{j} \\
       &= 1.69 \hat{i} + 2.27 \hat{j}
   \end{align*}

   \begin{align*}
     F_3 &= \sqrt{0.169 + 2.27} = 0.28 \text{ N} \\
     \phi &= \tan^{-1}{\left|\frac{2.27}{1.69}\right|} = 53^{\circ}
   \end{align*}

5) Assess your answer (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?)

   - Units are in Newtons for force and degrees for angle.

   - 2 sig. fig. from charges.

   - Reasonable. The magnitude of the net force on \(Q_3\) is indeed less than that of the force by \(Q_2\) on \(Q_3\) and the direction is in the first quadrant.
