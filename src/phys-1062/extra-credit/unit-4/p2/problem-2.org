#+TITLE: Problem 2
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

*Instructions:* Solve the problem below using the IRDEA framework.

*Problem 2:* How much work (in kJ) does a refrigerator with COP=4.2 require to freeze 670 g of water already at its freezing point (Recall: \(L_f = 333 \text{ kJ/kg}\))

1) Interpret the problem by identifying applicable concepts and principles (no equations here)

   - We want to find the work done by the refrigerator to freeze water.

   - To find the work, we can use \(Q_C\) and the COP.

   - We are given the COP of the refrigerator, so we only need to find \(Q_C\).

   - We can find \(Q_C\) using the latent heat equation.

   - Predict: The work done should be less than 333 kJ given that \(L_f = 333 \text{ kJ/kg}\) and the mass is less than 1 kg.

2) Represent the problem in terms of symbols and diagrams. (Make a SKETCH)

   - \(K = 4.2\)

   - \(m = 670 \text{ g} = 0.67 \text{ kg}\)

   - \(L_f = 333 \text{ kJ/kg}\)

   - \(W_{\text{in}} = \; ?\)

3) Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics; No calculations in the development stage)

   - Use latent heat equation to determine \(Q_C\).

       \[Q_C = mL_f = (0.67)(333) = 223.11 \text{ kJ}\]

   - Use the definition of COP to find work done.

     \[K = \frac{Q_C}{W_{\text{in}}} \implies W_{\text{in}} = \frac{Q_C}{K}\]

4) Evaluate your answer (No units involved when performing calculations)

       \[W_{\text{in}} = \frac{223.11}{4.2} = 53.1 \text{ kJ}\]

5) Assess your answer (Is the answer properly stated? Are units and sig fig or precision correct? Is the answer reasonable? Complete?)

   - 2 sig figs.

   - Units in kJ.

   - Reasonable since 53.1 kJ < 333 kJ for a mass less than 1 kg.
