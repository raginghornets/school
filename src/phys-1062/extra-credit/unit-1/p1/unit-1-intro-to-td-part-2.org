#+TITLE: Problem 1
#+SUBTITLE: Section 003
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

\noindent *Instructions:* Solve the problem below using the /IRDEA/ framework.

\noindent *Problem 1:* An ordinary glass is filled to the brim with 350.0 mL of water at 100.0°C.
If the temperature decreased to 20.0°C, how much water could be added to the glass (in mL to the nearest tenth of mL)?
Hint: Assume that both glass and water contract when the temperature falls with \(\beta_{\text{glass}} = 27 \times 10^{-6}\) \(\beta_{\text{water}} = 210 \times 10^{-6}\)

1. Interpret the problem by identifying applicable concepts and principles (no equations here)

   - Assume that both the glass and water contract as the temperature decreases following the law of volume expansion.

   - The water temperature does not go below 4°C, so it does not expand.

   - Predict: I expect that the amount of water that could be added to the glass will be a very small amount in mL.

2. Represent the problem in terms of symbols and diagrams. (Make a SKETCH)

   - \(V = 350.0 \text{ mL}\)

   - \(\Delta T = -80^{\circ} \text{C}\)

   #+CAPTION: Thermal expansion sketch (not to scale)
   \begin{figure}[h!]
   \centering
   \scalebox{.85}{\input{p1-sketch.pdf_tex}}
   \end{figure}

3. Develop a plan for solving the problem (Verbally explain what you are doing by identifying the relevant physics model or fact of mathematics; No calculations in the development stage)

   - Find the change in volume for both the glass and the water.

   - \(\Delta V_{\text{glass}} = \beta_{\text{glass}} V \Delta T\)

   - \(\Delta V_{\text{water}} = \beta_{\text{water}} V \Delta T\)

   - The amount of water that could be added is \(\Delta V_{\text{water}} - \Delta V_{\text{glass}}\).

4. Evaluate your answer (No units involved when performing calculations)

   \begin{align*}
     \Delta V_{\text{water}} &= \beta_{\text{water}} V \Delta T \\
     &= (210 \times 10^{-6})(350.0)(-80) \\
     &= -5.88 \\
     \Delta V_{\text{glass}} &= \beta_{\text{glass}} V \Delta T \\
     &= (27 \times 10^{-6})(350.0)(-80) \\
     &= -0.756 \\
     \Delta V_{\text{water}} - \Delta V_{\text{glass}} &= (-5.88) - (-0.756) \\
     &= -5.124 \approx -5.1 \text{ mL}
   \end{align*}

   - The amount of water that can be added is 5.1 mL.

5. Assess your answer (Is the answer properly stated? Are units and precision correct? Is the answer reasonable? Complete?)

   - The units are in mL as expected.

   - There are 2 sig. figs. from the coefficients of volume expansion.

   - The answer is reasonable as the amount of water that can be added after contraction is very small, as expected.
