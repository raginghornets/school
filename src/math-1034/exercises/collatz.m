function seq = collatz(n)
    seq = [];
    while n > 1
        if mod(n, 2) == 0
            n = n / 2;
        else
            n = (3 * n) + 1;
        end
        seq = [seq n];
    end
end