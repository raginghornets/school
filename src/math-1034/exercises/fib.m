function y = fib(n)
    if n == 2
        y = [1 1];
    elseif n == 1
        y = [1];
    elseif n == 0
        y = [0];
    elseif n < 0
        y = [];
    else
        x = [1 1 2];
        for i = 3:n
            x(i) = sum(x(i - 1) + x(i - 2));
        end
        y = x;
    end
end