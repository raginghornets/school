%% Week 4 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-04-02
%

%% Exercise 1

f = @(x) x .^ 3 - exp(0.8 * x) - 20;

figure;
fplot(f, [0 8]);
grid on;

hold on;
x1 = fzero(f, 4);
x2 = fzero(f, 7);
plot([x1 x2], [f(x1) f(x2)], 'o');

%% Exercise 2

f = @(x) 3 * sin(0.5 * x) - 0.5 * x + 2;

figure;
fplot(f, [-10 10]);
grid on;

hold on;
x1 = fzero(f, 6);
plot(x1, f(x1), 'o');

%% Exercise 3

f = @(x) x .^ 3 - x .^ 2 .* exp(-0.5 * x) - 3 * x + 1;

figure;
fplot(f, [-1.5 2]);
grid on;

hold on;
x1 = fzero(f, -1);
x2 = fzero(f, 0);
x3 = fzero(f, 2);
plot([x1 x2 x3], [f(x1) f(x2) f(x3)], 'o');

%% Exercise 4

f = @(x) cos(x) .^ 2 - 0.5 * x .* exp(0.3 * x) + 5;

figure;
fplot(f, [0 6]);
grid on;

hold on;
x1 = fzero(f, 4);
plot(x1, f(x1), 'o');

%% Exercise 5
% Determine the angle given the radius and area
A = 21.2;
r = 7;
f = @(x) A - ((r .^ 2) / 2) * (x - sin(x));

figure;
fplot(f);
grid on;

hold on;
x1 = fzero(f, 2);
plot(x1, f(x1), 'o');

%% Exercise 6
% Determine the friction coefficient in the Colebrook-White equation given
% the pipe relative roughness and Reynolds number
kd = 0.0004;
Re = 2e6;
E = @(f) sqrt(1 / f) + 0.86 * log((kd / 3.7) + (2.51 / (Re * sqrt(f))));

figure;
fplot(f);
grid on;

hold on;
x1 = fzero(f, 2);

%% Exercise 7
% Determine the minimum and maximum of the function
f = @(x) (2 + (x - 1.45) .^ 2) ./ (3 + 3.5 * (0.8 * x .^ 2 - 0.6 * x + 2));
g = @(x) -f(x);
[xmin, ymin] = fminbnd(f, 1, 3);
[xmax, ymax] = fminbnd(g, -3, -1);
ymax = -ymax;

figure;
fplot(f);
grid on;

hold on;
plot([xmin xmax], [ymin ymax], 'o');

%% Exercise 8
% Determine the frequency at the max amplitude
C = 15e-6;
L = 140e-3;
R = 22;
vm = 26;
wd = @(f) 2 * pi * f;
I = @(f) vm ./ sqrt(R^2 + ((wd(f) * L) - (1 ./ (wd(f) * C)))) .^ 2;
g = @(f) -I(f);
[xmax, ymax] = fminbnd(g, 60, 110);
ymax = -ymax;

figure;
fplot(I, [60 110]);
grid on;

hold on;
plot(xmax, ymax, 'o');

%% Exercise 9
f = @(x) (x .^ 3 .* exp(-0.2 * x)) ./ (1 + x .^2);
g = @(x) (4 * x + 3 * cos(4 * x)) ./ (2 + sin(x));
disp(integral(f, 1, 11));
disp(integral(g, 2, 7));

%% Exercise 10
f = @(x) nthroot(1 + 0.5 * x .^ 3 - x .^ 2, 4);
g = @(x) (x .* cos(x) + 2 * x .^ 2) ./ exp(x);
disp(integral(f, 0, 3));
disp(integral(g, 0, 8));

%% Exercise 11
% Determine the distance the car traveled
t = 0:7;
v = [0 14 39 69 95 114 129 139];
disp(trapz(t, v));

%% Exercise 12
x = 1:10;
y = [2.5 4.2 4.6 4.4 2.5 2.5 3.8 5.5 6.2 6.2];
disp(trapz(x, y));

%% Exercise 13
% Approximate the area of Lake Erie
x = 40:20:260;
y = [40 40 40 60 60 60 60 40 40 40 20 5];
disp(trapz(x, y));

%% Exercise 14
f = @(x,y) ((x .^ 2 .* sqrt(y)) / 5) - (2 * x);
y0 = 3;
[x,y] = ode45(f, 0:0.1:5, y0);

figure;
plot(x, y);

%% Exercise 15
g = @(x) (2 * exp(-(2 * x .^ 3 - 9 * x) / 6));
f = @(x,y) (-y .* x .^ 2) + 1.5 * y;
y0 = 2;
[x,y] = ode45(f, 0:0.1:3, y0);

figure;
plot(x,y);
hold on;
plot(x, g(x), '.');

%% Exercise 16
g = @(x) -tan(log(cos(x)) - (pi / 3));
f = @(x,y) (1 + y .^ 2) .* tan(x);
y0 = sqrt(3);
[x,y] = ode45(f, 0:0.01:0.8, y0);

figure;
plot(x,y);
hold on;
plot(x, g(x), '.');

%% Exercise 17
% Find the velocity over time
g = 9.81;
m = 5;
k = 0.005;
dv = @(t,v) (-m * g + k * v .^ 2) / m;
v0 = 0;
[t,v] = ode45(dv, 0:0.1:15, v0);

figure;
plot(t,v);

%% Exercise 18
% An airplane uses a parachute to slow down
dv = @(t,v) -0.0035 * v .^ 2 - 3;
v0 = (300 * 1000) / 3600;
vf = 12;
[t,v] = ode45(dv, 0:0.1:vf, v0);

figure;
plot(t,v);
grid on;

hold on;
x = zeros(length(t), 1);
for i=(2:length(t))
    x(i) = trapz(t(1:i),v(1:i));
end
plot(t, x);
