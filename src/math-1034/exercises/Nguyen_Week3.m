%% Week 3 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-03-26
%

%% Exercise 1
% Plot the given polynomial
c = [0.9 -0.3 -15.5 7 36 -7];
x = linspace(-4, 4, 100);
y = polyval(c, x);

figure;
plot(x, y);

%% Exercise 2
% Determine the polynomial of the given roots
x = linspace(-1, 6, 100);
zeros = [-0.7 0.5 3.4 5.8];
p = poly(zeros);
y = polyval(p, x);

figure;
plot(x, y);
hold on;
plot(zeros, polyval(p, zeros), 'o', 'MarkerFaceColor', 'red');
grid on;

%% Exercise 3
% Multiply two polynomials
p1 = [2 -3 6];
p2 = [-5 4 -7];
p3 = conv(p1, p2);
disp(p3);

%% Exercise 4
% Multiply more than two polynomials
p1 = [1 0];
p2 = [1 1.8];
p3 = [1 -0.4];
p4 = [1 -1.6];
p5 = conv(conv(conv(p1, p2), p3), p4);
disp(p5);

x = linspace(-2, 2, 100);
y = polyval(p5, x);

figure;
plot(x, y);

%% Exercise 5
% 2x * (2x + 2) * (2x + 4) * (2x + 6) = 1488384
p1 = [2 0];
p2 = [2 2];
p3 = [2 4];
p4 = [2 6];
p5 = conv(conv(conv(p1, p2), p3), p4);
p5(end) = -1488384;
zs = roots(p5);
disp(zs);

%% Exercise 6
% a * b * c = 6240
% a + b + c = 85
% c - a = 57
% c = 57 + a
% 2a + b + 57 = 85
% b = 85 - 57 - 2a
% b = 28 - 2a
% a * (-2a + 28) * (a + 57) = 6240
p1 = [1 0];
p2 = [-2 28];
p3 = [1 57];
p4 = conv(conv(p1, p2), p3);
p4(end) = -6240;
zs = roots(p4);
disp(zs);
a = ceil(zs(2));
b = 28 - 2 * a;
c = 57 + a;
fprintf('a = %d\n', a);
fprintf('b = %d\n', b);
fprintf('c = %d\n', c);

%% Exercise 7
% Plot a least-squares regression line
x = [-5 -4 -1 1 4 6 9 10];
y = [12 10 6 2 -3 -6 -11 -12];
p1 = polyfit(x, y, 1);
disp(p1);

figure;
plot(x, y, 'o');

hold on;
x1 = linspace(-5, 10, 100);
y1 = polyval(p1, x1);
plot(x1, y1);

%% Exercise 8
% Boiling temperature of water at various altitudes
h = [-1000 0 3000 8000 15000 22000 28000];
T = [213.9 212 206.2 196.2 184.4 172.6 163.1];
p1 = polyfit(h, T, 1);
disp(p1);

figure;
plot(h, T, 'o');

hold on;
x1 = linspace(-1000, 28000, 100000);
y1 = polyval(p1, x1);
plot(x1, y1);

%% Exercise 9
% Estimate U.S. population between 1815 and 1965
year = [1815 1845 1875 1905 1935 1965];
population = [8.3 19.7 44.4 83.2 127.1 190.9];
p1 = polyfit(year, population, 2);

figure;
plot(year, population, 'o');

hold on;
x1 = 1815:5:1965;
y1 = polyval(p1, x1);
plot(x1, y1);

est = polyval(p1, 1915);
fprintf('The U.S. population in 1915 was roughly %.1f million.\n', est);

%% Exercise 10
% Plot different polynomial fits
x = [1 2.2 3.7 6.4 9 11.5 14.2 17.8 20.5 23.2];
y = [12 9 6.6 5.5 7.2 9.2 9.6 8.5 6.5 2.2];
p1 = polyfit(x, y, 1);
p2 = polyfit(x, y, 2);
p3 = polyfit(x, y, 3);
p4 = polyfit(x, y, 5);

figure;
plot(x, y, 'o');

hold on;
x1 = linspace(1, 24, 100);
y1 = polyval(p1, x1);
plot(x1, y1);

hold on;
x2 = linspace(1, 24, 100);
y2 = polyval(p2, x2);
plot(x2, y2);

hold on;
x3 = linspace(1, 24, 100);
y3 = polyval(p3, x3);
plot(x3, y3);

hold on;
x4 = linspace(1, 24, 100);
y4 = polyval(p4, x4);
plot(x4, y4);
%% Exercise 11
% Model of a chemical reaction
t = 0.5:0.5:6.0;
C = [1.7 3.1 5.7 9.1 6.4 3.7 2.8 1.6 1.2 0.8 0.7 0.6];
p1 = polyfit(t, 1 ./ C, 2);
p2 = polyfit(t, 1 ./ C, 3);

figure;
plot(t, C, 'o');

hold on;
x1 = linspace(0.5, 6.0, 100);
y1 = polyval(p1, x1);
plot(x1, 1 ./ y1);
disp(1 / polyval(p1, 2));

hold on;
x2 = linspace(0.5, 6.0, 100);
y2 = polyval(p2, x2);
plot(x2, 1 ./ y2);
disp(1 / polyval(p2, 2));

%% Exercise 12
% Error between spline and original cosine function
x = (-2 * pi):(pi / 2):(2 * pi);
y = cos(x);
x1 = (-2 * pi):(0.01 * pi):(2 * pi);
y1 = spline(x, y, x1);

figure;
plot(x, y, 'o');

hold on;
plot(x1, y1);

% Plot error
hold on;
y2 = cos(x1);
plot(x1, y1 - y2);

%% Exercise 13
% Plot weight over time
W = [10 27 2001 5 10 4 8
     11 19 2001 7 4 5 11
     12 03 2001 8 12 6 4
     12 20 2001 10 14 8 7
     01 09 2002 12 13 10 3
     01 23 2002 14 8 12 0
     03 06 2002 16 10 13 10];
t = datenum(W(:,[3 1 2]));
w1 = W(:,4) + W(:,5) / 16;
w2 = W(:,6) + W(:,7) / 16;

t1 = t(1):t(end);

figure;
plot(t, w1, 'o');

hold on;
plot(t, w2, 'o');

hold on;
y1 = interp1(t, w1, t1, 'pchip');
plot(t1, y1);

hold on;
y2 = interp1(t, w2, t1, 'pchip');
plot(t1, y2);

datetick('x', 'mmm yy');
title("Tom and Ben's Weight Over Time");
legend("Tom's weight", "Ben's Weight",...
    "Tom's estimated weight", "Ben's Estimated Weight",...
    'Location','NorthWest');

%% Exercise 14
% Plot of your hand
figure('position', get(0, 'screensize'));
axes('position', [0 0 1 1]);
% [x, y] = ginput
x = [0.2251
    0.2280
    0.1996
    0.2230
    0.2428
    0.2733
    0.3014
    0.3236
    0.3413
    0.3681
    0.3564
    0.3632
    0.4220
    0.4457
    0.4587
    0.4283
    0.4345
    0.4540
    0.5723
    0.5952
    0.6113
    0.5038
    0.4895
    0.4939
    0.5431
    0.6171
    0.6389
    0.6160
    0.6051
    0.5658];
y = [0.1443
    0.3838
    0.6970
    0.7528
    0.7246
    0.4375
    0.4435
    0.8612
    0.8828
    0.8532
    0.4772
    0.4706
    0.8778
    0.9014
    0.8737
    0.4576
    0.4214
    0.4149
    0.7282
    0.7397
    0.6950
    0.3321
    0.2236
    0.1463
    0.2146
    0.3426
    0.3065
    0.2402
    0.1785
    0.0585];

n = length(x);
s = (1:n)';
t = (1:0.05:n);
u = spline(s,x,t);
v = spline(s,y,t);
u1 = interp1(s, x, t, 'pchip');
v1 = interp1(s, y, t, 'pchip');

figure;
plot(x, y, '.', u, v, '-', u1, v1, '-');
legend('Input', 'spline', 'pchip');
set(gca, 'visible', 'off');
disp('interp1 with pchip is better than spline');
