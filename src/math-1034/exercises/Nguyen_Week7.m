%% Week 7 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-04-26
%

%% Exercise 1
A = [15.94 16.98 15.94 16.16 15.86 15.86 16.90 15.88];
B = [15.96 15.94 16.02 16.10 15.92 16.00 15.96 16.02];
fprintf('The mean of A is %.2f and the standard deviation is %.2f.\n', mean(A), std(A));
fprintf('The mean of B is %.2f and the standard deviation is %.2f.\n', mean(B), std(B));
disp('Since the mean is closer to 16 and the standard deviation is smaller, production line B  is better in terms of meeting the specification.');

%% Exercise 2
v = randi([1,5],1,20);
w = randi([1,500],1,20);
disp('I think the mean and median to be the same for both vectors.');
disp('I think the standard deviation will be different because they have the same size but different domain.');
disp([mean(v) median(v)]);
disp([mean(w) median(w)]);
disp([std(v) std(w)]);

figure;
hold on;
subplot(2,1,1);
histogram(v,5);
subplot(2,1,2);
histogram(w,5);

%% Exercise 3
signal = input('Signal: ');
medianFilter3 = zeros(1,length(signal));
medianFilter3(1) = signal(1);
medianFilter3(end) = signal(end);
for i=1:(length(signal) - 2)
    medianFilter3(i + 1) = median([signal(i) signal(i + 1) signal(i +2)]);
end
disp(medianFilter3);

%% Exercise 4
num_experiments = input('Number of experiments: ');
experiments = repmat(struct('data',[]), num_experiments, 1);
for i=1:num_experiments
    experiments(i) = struct('data', input('Data: '));
end
for i=1:num_experiments
    fprintf('The average of experiment %d is %.2f.\n', i, mean(experiments(i).data));
end

%% Exercise 5
data = 0.05 + (0.25 - 0.05) .* rand(5,10);
fid = fopen('nail_diameters.txt', 'w');
for r=1:length(data(:,1))
    fprintf(fid, '%f ', data(r,:));
    fprintf(fid, '\n');
end
fclose(fid);

data = zeros(5,10);

fid = fopen('nail_diameters.txt', 'r');
while (~feof(fid))
    values = fscanf(fid, '%f');
    data = reshape(values, 5, 10);
end

for i=1:length(data(:,1))
    fprintf('Sample %d: mean = %f, std = %f.\n',...
        i, mean(data(i,:)), std(data(i,:)));
end

%% Exercise 6
% Coefficient of variation in class grades
scores{1} = [99 100 95 92 98 89 72 95 100 100];
scores{2} = [83 85 77 62 68 84 91 59 60];
fid = fopen('scores.txt', 'w');
for r=1:length(scores)
    fprintf(fid, '%d ', scores{r});
    fprintf(fid, '\n');
end
fclose(fid);

fid = fopen('scores.txt', 'r');
section1 = fscanf(fid, '%d', 10);
section2 = fscanf(fid, '%d', 9);
fclose(fid);
CV1 = std(section1) / mean(section1);
CV2 = std(section2) / mean(section2);
fprintf('The coefficient of variation for section 1 is %.2f.\n', CV1);
fprintf('The coefficient of variation for section 2 is %.2f.\n', CV2);

%% Exercise 7
% Sorted DNA sequences
DNA{1} = 'TACGGCAT';
DNA{2} = 'ACCGTAC';
DNA{1} = sort(DNA{1});
DNA{2} = sort(DNA{2});
nucleobases = 'ATCG';
A = string(zeros(3,3));
for i=1:size(A,1)
    for j=1:size(A,2)
        A(i,j) = nucleobases(randi([1,length(nucleobases)], 1, 5));
    end
end

disp(A);

% Sort the DNA matrix alphabetically,
% but not the individual DNA sequences
disp(sort(A));

%% Exercise 8
% Highlight closest point
N = 10;
x = rand(1,N);
y = rand(1,N);
pt = rand(1,2);
d = zeros(1,N);
for i=1:N
    d(i) = sqrt((pt(1) - x(i))^2 + (pt(2) - y(i))^2);
end
c = find(d == min(d));

figure;
hold on;
grid on;
scatter(x(c), y(c), 90, 'o', 'MarkerEdgeColor', 'red',...
    'LineWidth', 1.5);
scatter(x, y, 'filled', 'black');
scatter(pt(1), pt(2), 90, 'x', 'MarkerEdgeColor', 'red',...
    'LineWidth', 2);

%% Exercise 9
% Estimating Areas of Intersection
% Circle
c = randi(10, 1, 2);
r = randi(9) + 1;
theta = 0:0.001:2*pi;
cx = r*cos(theta);
cy = r*sin(theta);

% Axis area
xmin = c(1)-r;
xmax = c(1)+r;
ymin = c(2)-r;
ymax = c(2)+r;

% Rectangle
w = [randi(9)+1 0];
h = [0 randi(9)+1];
blc = [randi([xmin-ceil(w(1)/2) xmax-ceil(w(1)/2)]) randi([ymin-ceil(h(2)/2) ymax-ceil(h(2)/2)])];
R = [blc; blc+h; blc+w+h; blc+w; blc];

% Monte Carlo
n = 20000;
x = xmin + (xmax - xmin) .* rand(n, 1);
y = ymin + (ymax - ymin) .* rand(n, 1);
pt = [x y];

% Points in circle
xyic = sqrt((x - c(1)) .^ 2 + (y - c(2)) .^ 2) <=  r;
xyir = x >= blc(1) & x <= blc(1)+w(1) & y >= blc(2) & y <= blc(2)+h(2);
xic = x(xyic & xyir);
yic = y(xyic & xyir);
pic = [xic yic];

% Rectangle in circle
if (ismember(x(xyir), x(xyic)))
    A = w(1) * h(2);

% Circle in rectangle
elseif (sum(xyir) >= sum(xyic))
    A = pi * r^2;

% Otherwise, estimate
else
    A = (length(pic) / n) * (xmax-xmin) * (ymax-ymin);
end

% Plot
figure;
hold on;
grid on;
title(sprintf('Area = %.4f', A));
axis([xmin-1 xmax+1 ymin ymax]);
scatter(x, y, 1, 'k');
scatter(xic, yic, 5, 'r', 'MarkerFaceColor', 'red');
plot(c(1) + cx, c(2) + cy, 'blue', 'LineWidth', 2);
plot(R(:,1), R(:,2), 'blue', 'LineWidth', 2);

%% Exercise 10
% Convert image to different formats
A = imread('cameraman.tif');
imwrite(A, 'cameraman.jpeg');
imwrite(A, 'cameraman.png');
imwrite(A, 'cameraman.bmp');

%% Exercise 11
n = input('Enter a number between 1 and 4: ');

c1 = randi(255);
c2 = randi(255);
A = ones(2) * c1;

switch (n)
    case 1
        A(1,2) = c2;
    case 2
        A(1,1) = c2;
    case 3
        A(2,1) = c2;
    case 4
        A(2,2) = c2;
    otherwise
        disp('Error. Invalid input.');
        return
end

figure;
colormap jet;
imagesc(A, [0 255]);

%% Exercise 12
% Plot RGB histogram
filename = 'parrot.jpg';
A = imread(filename);

figure;
hold on;
subplot(3,1,1);
histogram(A(:,:,1), 5, 'FaceColor', 'r');
title(sprintf('RGB frequency of %s', filename));
subplot(3,1,2);
histogram(A(:,:,2), 5, 'FaceColor', 'g');
subplot(3,1,3);
histogram(A(:,:,3), 5, 'FaceColor', 'b');

%% Exercise 13
% Overlay successively smaller versions of an image
A = imread('car2.jpg');
A = rgb2gray(A);
A1 = imresize(A, 1/2);
A2 = imresize(A, 1/4);
A3 = imresize(A, 1/8);
A4 = imresize(A, 1/16);
A5 = imresize(A, 1/32);
A6 = imresize(A, 1/64);
A(1:size(A1,1), 1:size(A1,2)) = A1;
A(1:size(A2,1), 1:size(A2,2)) = A2;
A(1:size(A3,1), 1:size(A3,2)) = A3;
A(1:size(A4,1), 1:size(A4,2)) = A4;
A(1:size(A5,1), 1:size(A5,2)) = A5;
A(1:size(A6,1), 1:size(A6,2)) = A6;

figure;
hold on;
imshow(A);

%% Exercise 14
% Animate markers following sine and cosine paths
x = 0:0.1:400;

figure;
hold on;
plot(x, sin(pi*x/100), '--k');
plot(x, cos(pi*x/100), '--k');
h1 = plot(0,0,'ks','MarkerSize', 10, 'LineWidth', 3);
h2 = plot(0,1,'r^','MarkerSize', 10, 'LineWidth', 3);
for i=x
    set(h1,'XData',i,'YData',sin(pi*i/100));
    set(h2,'XData',i,'YData',cos(pi*i/100));
    pause(0.002);
end

%% Exercise 15
c = [0 0];
r = 1;
x = linspace(0, 2*pi, 100);
cx = r*cos(x);
cy = r*sin(x);
xmin = -1.65;
xmax = 1.65;
ymin = -1.25;
ymax = 1.25;

figure;
hold on;
fill([xmin xmax xmax xmin xmin], [0 0 ymin ymin 0], 'k',...
    'FaceAlpha', 0.25, 'EdgeAlpha', 0);
axis([xmin xmax ymin ymax]);
plot(cx, cy, 'k');
h1 = plot(0, r, 'k.', 'MarkerSize', 100);
while true
    for i=x
        h1x = r*cos(i);
        h1y = r*sin(i);
        set(h1,'XData',h1x,'YData',h1y);
        if (h1y < 0)
            set(h1,'Marker','d','MarkerSize',25,'MarkerFaceColor',[.9 .5 .9]);
        else
            set(h1,'Marker','.','MarkerSize',100);
        end
        pause(0.02);
    end
end
