function y = fibrecur(n)
    if n == 0
        y = 0;
    elseif n == 1
        y = 1;
    else
        y = fibrecur(n - 2) + fibrecur(n - 1);
    end
end