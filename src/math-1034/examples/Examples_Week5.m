%% Week 5 Examples (Symbolic Computations)
%
% Author: Eric Nguyen
%
% Date: 2020-04-07
%

%% Symbolic variables and exact arithmetic

%% Declaring symbolic expressions
x = sym('x');
y = sym('y');
% or
syms x y;
disp(x);
disp(y);

%% New expression
syms a b c x y;
f = sqrt(a * x^2 + b * x + c);
disp(f);

% New arithmetic expression
a = sym(3);
b = sym(5);
e = b / a + sqrt(2);

% Evaluate the arithmetic expression
disp(double(e));

y = sym(2) * cos(pi / 4);
disp(double(y));

%% Creating new expressions
syms x y;
SA = x + y;
SB = x - y;
F = SA ^ 2 / SB ^ 3 + x ^ 2;

%% Expanding and factorizing expressions
syms x y;
f = (x + y)^3;
disp(expand(f));
disp(factor(f));
disp(collect(f,y));
z = sin(x + y);
disp(expand(z));
disp(diff(z, x, 2));

%% Integration
syms x;
z = x ^ 3 + x + 1;
disp(int(z, 0, 1));

%% Simplify functions
syms x y;
S1 = (x ^ 2 + 5 * x + 6) / (x + 2);
disp(simplify(S1));
S2 = (x + y) / (1 / x + 1 / y);
S3 = simplify(S2);
S4 = S3 - S2;
disp(simplify(S4));
z = cos(x) ^ 2 + sin(x) ^ 2;
disp(simplify(z));

%% Pretty functions
syms a b c d x;
syms a1 b1 c1 d1;
S1 = (a * x + b) / (c * x + d);
S2 = (a1 * x + b1) / (c1 * x + d1);
S3 = simplify(S1 + S2);
pretty(S1)
pretty(S2)
pretty(S3)

%% Solving linear/nonlinear equations
syms a b x y z;
eq1 = exp(2 * z) - 5;
disp(solve(eq1)); % eq1 = 0 by default
eq2 = (x - 1) * (x + 2) * (x - 4); % roots 1, -2, 4
disp(solve(eq2)); % solves eq2 = 0
eq3 = cos(2 * y) + 3 * sin(y) - 2;
disp(solve(eq3));
T = a * x ^ 2 + 5 * b * x + 10;
solve(T); % solve T = 0 with respect to x
solve(T,a); % solve T = 0 with respect to a
solve(T,b); % solve T = 0 with respect to b

%% System of equations
syms x y t;
S1 = 10 * x + 12 * y + 16 * t;
S2 = 5 * x + 10 * x + 12 * y;
[xs, ys] = solve(S1, S2, x, t);

%% ODEs solving
syms t y(t);
Dy = diff(y);
disp(dsolve(Dy == 4*t + 2*y));

%% Plotting functions
syms x y;
T = x ^ 2 + x + 1;
ezplot(T);

S = (x - 1)^2 + (y - 1)^2 - 1;
ezplot(S, [0, 2, 0, 2]);
syms t;
x = cos(2 * t);
y = 2 * sin(2 * t);
ezplot(x, y);

%% Numerical substitution of symbolic expressions
syms x;
S = 0.8 * x ^ 3 + 4 * exp(0.5 * x);
value = subs(S,x,2);
disp(double(value));
vec_value = subs(S,x,0:0.1:4);

syms a b c e x;
S = a * x ^ 2 + b * x + c;
subs(S, {a,b}, {1,2});
subs(S, {a,b}, {[1 3], [3 4]});

%% Limits of functions and sequences

%% Computing the limit of a function
syms x;
f = @(x) x^2;
disp(limit(f,x,3)); % limit x->3 x^2 = 9
g = @(x) sqrt(x^2 + x) - x; % limit x->inf sqrt(x^2 + x) - x
disp(limit(g,x,Inf));

%% One-sided limits
syms x;
f = @(x) abs(x - 3) / (x - 3);
limL = limit(f,x,3,'left');
limR = limit(f,x,3,'right');
continuity = (limL == limR);
if continuity
    disp('continuous');
else
    disp('discontinuous');
end

%% Derivative
syms x0 x;
limit((sin(x) - sin(x0)) / (x - x0), x, x0);
disp(subs(diff(sin(x)), x, x0));

%% Limits of sequences
syms n;
an = sqrt((1 + n) / n);

%% Limits of series
syms n;
an = 1 / n^2;
sum_a = symsum(an,n,1,Inf);
disp(sum_a);
