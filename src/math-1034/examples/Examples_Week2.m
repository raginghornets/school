%% Examples Week 2
%
% Author: Eric Nguyen
%

%% Non-singular Matrices

%% Example
A = [1 4; -3 1];
B = inv(A);
disp(A * B - eye(2));
C = A ^ (-1);
disp(A * C - eye(2));

%% Example
A = [1 3; 2 -9];
b = [6; 1];
B = inv(A);
x = B * b;
disp(A * x - b);

x = A \ b;
disp(A * x - b);

% Example
% x1 + 3 x2 = 6
% 2x 1 - 9 x2 = 1
% and
% x1 + 3 x2 = 1
% 2 x1 - 9 x2 = 2
B = [6 1; 1 2];
X = A \ B;

disp(A * X(:,1) - B(:,1))
disp(A * X(:,2) - B(:,2))

X = B / A;
disp(X * A - B);

%% The MATLAB Backslash Command
A = [ 2 -2  0  0
     -2  6 -2  0
      0 -2  2 -2
      0  0 -2  8];
b = [5
     0
     0
     0];

x = inv(A) * b;
disp(A * x - b);

x = A \ b;
disp(A * x - b);

%% System of symbolic linear equations
% ax + by = c
% dx + ey = f

syms a b x y d e f c;
[x, y] = solve(a*x + b*y == c, d*x + e*y == f, x, y);

%% Example 5
syms a b x y d e f c;
A = [a b
     d e];
b = [c
     f];

x = linsolve(A, b);
disp(simplify(A * x - b));

%% Undetermined systems

%% Example
syms x y z;
[x, y] = solve(x + 3*y + 2*z == 2, x + y + z == 4, x, y);
z = 2;
A = [1 3 2
     1 1 1];
b = [2; 4];

%% Example
A = [5 3 3
     3 3 3];
b = [40; 30];
x = pinv(A) * b;

%% Overdetermined systems

%% Example
% 2 = c1(0) + c2
% 6 = c1(5) + c2
% 11 = c1(10) + c2
A = [ 0 1
      5 1
     10 1];
b = [2; 6; 11];
c = A \ b;










