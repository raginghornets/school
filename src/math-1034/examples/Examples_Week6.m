%% Week 6 Examples (Strings, Cell Arrays, Structures, and Importing Data)
%
% Author: Eric Nguyen
%
% Date: 2020-04-16
%

%% Example char

disp(char(32));
str = char([77 65 84 76 65 66]); % The string "MATLAB"
disp(length(str));
disp(numel(str));

str = 'This is a string';
disp(length(str));

disp(ischar(str));
disp(ischar(1));
disp(isfloat(1));

%% Create array of strings with char
full_name = char('John', 'Doe');
full_name = strvcat('John', 'Doe'); % Same as char

disp(deblank(full_name(2,:)));
disp(deblank(' Doe ')); % Remove the whitespace on the right
disp(strtrim(' Doe ')); % Remove the whitespace on both sides

full_name = strcat('John', 'Doe');
str1 = 'john';
str2 = 'John';
str3 = 'Josh';
disp(strcmp(str1, str2));
disp(strcmpi(str1, str2)); % Ignore case
disp(strncmp(str2, str3, 2)); % Compare 'Jo' from 'John' with 'Jo' from 'Josh'
disp(strncmp(str2, str3, 3)); % Compare 'Joh' with 'Jos', which is false

%% Logical arrays of string properties
str = 'Building A1';
disp(isletter(str));
disp(isspace(str));
disp(isstrprop(str, 'digit'));
disp(isstrprop(str, 'lower'));
disp(isstrprop(str, 'lower'));

%% Finding and replacing substrings
str = 'This is a test!';
disp(strfind(str, 'is')); % Returns an array of the indices of each occurence
new_str = strrep(str, 'test', 'done');
disp(lower(str));
disp(upper(str));

%% Evaluating strings
c = int2str(4);
disp(str2double(c));
disp(ceil(str2double(c)));
c2 = num2str(pi);
disp(str2double(c2));
disp(num2str(pi, 10)); % Add option for precision
str = '2*pi + cos(pi)';
disp(eval(str));

%% Cell arrays
a{1,1} = rand(2);
a{1,2} = str;
a{2,2} = randi([0, 2], 2, 2);
a{2,1} = 1234;

disp(a{1,1}(1,1));
disp(a{1,2}(1:10));

celldisp(a); % Display each cells
cellplot(a); % Visual representation of the cell array

cellstring{1} = 'John Doe';
cellstring{2} = 'Male ';
cellstring{3} = 'SSN XXX-XX-XXXX';

data = char('John Doe', 'Jane Doe');
c = cellstr(data);
data2 = char('John Doe', 'Male', 'Jane Doe');
c2 = cellstr(data2);
disp(transpose(c2));

%% Student Structure Example
student.name = 'John Doe';
student.addr = '123 Broad Street';
student.city = 'Philadelphia';
student.state = 'PA';
student.zip = 19122;

clear student;
student = struct('name', 'John Doe',...
    'addr', '123 Broad Street',...
    'city', 'Philadelphia',...
    'state', 'PA',...
    'zip', 19122);

%% Creating an array of structures
student(2).name = 'Jane Doe';
student(2).age = 25;
student(1).age = 27;

disp(mean([student.age]));

clear student;
student(10) = struct('name', 'John Doe',...
    'addr', '123 Broad Street',...
    'city', 'Philadelphia',...
    'state', 'PA',...
    'zip', 19122);

clear student;
student = struct('name', {'John Doe', 'Jane Doe'},...
    'city', {'Philadelphia', 'Baltimore'},...
    'state', {'PA', 'Maryland'});
student(3).state = 'Virginia';

new_student = rmfield(student, 'state');
new_student = setfield(new_student, {2}, 'state', 'Vermont');

student(1).class = struct('name', {'MATH 1041', 'PHYS 1030'},...
    'instructor', {'Mr. Smith', 'Mrs. Smith'});

disp(student(1).class(2).instructor);
disp(student(2).class); % student(2) doesn't exist

%% Saving and importing mat files
mat = rand(3,5);
x = 1:6;
y = x.^2;

save my_file mat x y;
A = load('my_file.mat');
disp(A.mat);
disp(A.x);
disp(A.y);

%% Creating a file
fid = fopen('data.txt', 'w');
fprintf(fid, 'I am writing on the file data.txt\n');
fclose(fid);
fid = fopen('data2.txt', 'w');
fprintf(fid, 'I am writing on the file data2.txt\n');
fclose(fid);
fid = fopen('data.txt', 'r');
fclose(fid);

%% Creating a file for planets
names = char('Mercury', 'Venus', 'Earth', 'Mars',...
    'Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto');
position = [1; 2; 3; 4; 5; 6; 7; 8; 9];
diameter = [ 4880; 12103.6; 12756.3;...
             6794;  142984;  120536;...
            51118;   49532;    2274];

fid = fopen('planets.txt', 'w');
for i = 1:length(diameter);
    fprintf(fid, '%7s %5d %12d\n', names(i,:), position(i), diameter(i));
end
fclose(fid);

clear names position diameter;

fid = fopen('planets.txt', 'r');
name = [];
position = [];
diameter = [];

while (~feof(fid))
    str = fscanf(fid, '%7c', 1);
    name = [name; str];
    value = fscanf(fid, '%5d', 1);
    position = [position; value];
    value = fscanf(fid, '%12d', 1);
    diameter = [diameter; value];
    fscanf(fid, '%c', 1);
end

fclose(fid);
