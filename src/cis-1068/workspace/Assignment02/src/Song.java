// Eric Nguyen
// Section 008
// Assignment 2. Get You a Cat
// 2020-01-21
//
// Print the lyrics of a shortened version
// of the song, Bought Me a Cat
//

public class Song {

    public static void main(String[] args) {

        // Print the entire song
        verseOne();
        verseTwo();
        verseThree();
        verseFour();
        verseFive();
        verseSix();

    }

    public static void catGoes() {
        System.out.println("Cat goes fiddle-i-fee.");
        System.out.println();
    }

    public static void henGoes() {
        System.out.println("Hen goes chimmy-chuck, chimmy-chuck,");
        catGoes();
    }

    public static void duckGoes() {
        System.out.println("Duck goes quack, quack,");
        henGoes();
    }

    public static void gooseGoes() {
        System.out.println("Goose goes hissy, hissy,");
        duckGoes();
    }

    public static void sheepGoes() {
        System.out.println("Sheep goes baa, baa,");
        gooseGoes();
    }

    public static void verseOne() {
        System.out.println("Bought me a cat and the cat pleased me,");
        System.out.println("I fed my cat under yonder tree.");
        catGoes();
    }

    public static void verseTwo() {
        System.out.println("Bought me a hen and the hen pleased me,");
        System.out.println("I fed my hen under yonder tree.");
        henGoes();
    }

    public static void verseThree() {
        System.out.println("Bought me a duck and the duck pleased me,");
        System.out.println("I fed my duck under yonder tree.");
        duckGoes();
    }

    public static void verseFour() {
        System.out.println("Bought me a goose and the goose pleased me,");
        System.out.println("I fed my goose under yonder tree.");
        gooseGoes();
    }

    public static void verseFive() {
        System.out.println("Bought me a sheep and the sheep pleased me,");
        System.out.println("I fed my sheep under yonder tree.");
        sheepGoes();
    }

    public static void verseSix() {
        System.out.println("Bought me a pig and the pig pleased me,");
        System.out.println("I fed my pig under yonder tree.");
        System.out.println("Pig goes oink, oink,");
        sheepGoes();
    }

}
