/**
 * Eric Nguyen
 * Section 008
 * Assignment 6 Southie Styles
 * 2020-03-10
 *
 * Converts text file to use a Southie accent, as described in the README.
 * E.g. "Very rare in these waters, and definitely a maneater." would be converted to
 *      "Vehy rahe in these watehs, and definitely a maneateh."
 *
 */

import java.util.Scanner;
import java.io.*;

public class SouthieStyles {

    public static final String INPUT_FILE = "jaws.txt";
    public static final String OUTPUT_FILE = "out.txt";

    // Extra credit: Quotes only
    public static final String INPUT_FILE_EC = "frankenstein.txt";
    public static final String OUTPUT_FILE_EC = "out_ec.txt";
    public static final char OPEN_QUOTE = '“';
    public static final char END_QUOTE = '”';

    // Rule 1
    public static final String VOWELS = "AEIOU";

    // Rule 3
    public static final String WORD_TO_REPLACE = "VERY";
    public static final String REPLACEMENT_WORD = "WICKED";
    public static final int REPLACEMENT_DIFFERENCE =
        REPLACEMENT_WORD.length() - WORD_TO_REPLACE.length(); 

    public static void main(String[] args) throws FileNotFoundException {

        convertToSouthieStyle(INPUT_FILE, OUTPUT_FILE);
        convertQuotesSouthieStyle(INPUT_FILE_EC, OUTPUT_FILE_EC);
    
    }

    /**
     * Convert an entire file to its Southie Style equivalent
     */
    public static void convertToSouthieStyle(String inputFilename, String outputFilename) throws FileNotFoundException {

        PrintStream output = new PrintStream(new File(outputFilename));
        Scanner s = new Scanner(new File(inputFilename));

        while (s.hasNextLine()) {

            // The extra character allows words at the end of each line to be considered
            String line = s.nextLine() + ' ';
            
            output.println(parseLine(line));

        }

    }

    public static void convertQuotesSouthieStyle(String inputFilename, String outputFilename) throws FileNotFoundException {

        PrintStream output = new PrintStream(new File(outputFilename));
        Scanner s = new Scanner(new File(inputFilename));

        boolean inQuote = false;
        
        while (s.hasNextLine()) {

            // The extra character allows words at the end of each line to be considered
            String line = s.nextLine() + ' ';

            // The Frankenstein text provided by the Gutenberg Project uses special quotes
            int openQuotePos = line.indexOf(OPEN_QUOTE);
            int endQuotePos = line.indexOf(END_QUOTE);

            boolean existsOpenQuote = openQuotePos > -1;
            boolean existsEndQuote = endQuotePos > -1;

            if (!existsOpenQuote && !existsEndQuote && !inQuote) {
                output.println(line);
            } else if (existsOpenQuote && !existsEndQuote) {
                output.print(line.substring(0, openQuotePos));

                output.print(parseLine(line.substring(openQuotePos)));
                output.println();
                inQuote = true;
            } else if (!existsOpenQuote && existsEndQuote) {
                output.print(parseLine(line.substring(0, endQuotePos)));

                output.print(line.substring(endQuotePos));
                output.println();
                inQuote = false;
            } else if (inQuote) {
                output.println(parseLine(line));
            } else if (existsOpenQuote && existsEndQuote) {
                output.println(parseMultiQuotedLine(line, openQuotePos, endQuotePos));
            }

        }

    }

    public static String parseMultiQuotedLine(String line, int openQuotePos, int endQuotePos) {

        String output = "";

        int nextOpenQuotePos = line.indexOf(OPEN_QUOTE, endQuotePos);
        int nextEndQuotePos = line.indexOf(END_QUOTE, endQuotePos);
        boolean existsNextOpenQuote = openQuotePos > -1;
        boolean existsNextEndQuote = endQuotePos > -1;

        // Spaghetti; TODO: Recursion/loops
        if (endQuotePos > openQuotePos && nextEndQuotePos > endQuotePos) {
            output += line.substring(0, openQuotePos);

            output += parseLine(line.substring(openQuotePos, endQuotePos));

            if (existsNextOpenQuote && !existsNextEndQuote) {
                output += line.substring(endQuotePos, nextOpenQuotePos);
                output += parseLine(line.substring(nextOpenQuotePos));
            } else if (existsNextOpenQuote && existsNextEndQuote) {
                output += line.substring(endQuotePos, nextOpenQuotePos);
                output += parseLine(line.substring(nextOpenQuotePos, nextEndQuotePos));
                output += line.substring(nextEndQuotePos);
            } else {
                output += line.substring(endQuotePos);
            }
        } else if (openQuotePos > endQuotePos && nextEndQuotePos > openQuotePos) {
            output += line.substring(0, endQuotePos);

            output += parseLine(line.substring(endQuotePos, openQuotePos));

            if (existsNextOpenQuote && !existsNextEndQuote) {
                output += line.substring(openQuotePos);
                output += parseLine(line.substring(nextEndQuotePos));
            } else if (existsNextEndQuote && existsNextOpenQuote ) {
                output += line.substring(openQuotePos, nextEndQuotePos);
                output += parseLine(line.substring(nextEndQuotePos, nextOpenQuotePos));
                output += line.substring(nextOpenQuotePos);
            } else {
                output += line.substring(openQuotePos);
            }
        }

        return output;

    }

    /**
     * Return a the Southie Style equivalent of a given line
     */
    public static String parseLine(String line) {

        String cookedLine = "";
        boolean firstReplacement = true;

        for (int i = 0; i < line.length(); i++) {

            // exceptions 1 and 2 and rules 2 and 3 need i + 1 because
            // they look for end of word
            if (exceptionOne(line, i + 1)) {
                if (Character.isUpperCase(line.charAt(i))) {
                    cookedLine += "YAH";
                } else {
                    cookedLine += "yah";
                }
            } else if (exceptionTwo(line, i + 1)) {
                if (Character.isUpperCase(line.charAt(i))) {
                    cookedLine += "WAH";
                } else {
                    cookedLine += "wah";
                }
            } else if (ruleOne(line, i)
                       && !ruleThree(WORD_TO_REPLACE, line, i)) {
                if (Character.isUpperCase(line.charAt(i))) {
                    cookedLine += "H";
                } else {
                    cookedLine += "h";
                }
            } else if (ruleTwo(line, i + 1)) {
                if (Character.isUpperCase(line.charAt(i))) {
                    cookedLine += "AR";
                } else {
                    cookedLine += "ar";
                }
            } else if (ruleThree(WORD_TO_REPLACE, line, i + 1)) {

                int firstLetter = i - WORD_TO_REPLACE.length() + 1;
                int lineDiff = i - cookedLine.length(); // Adjust to modifications made by other rules/exceptions

                // Remove WORD_TO_REPLACE
                if (firstReplacement) {
                    cookedLine = cookedLine.substring(0, firstLetter - lineDiff);
                    firstReplacement = false;
                } else if (!firstReplacement) {
                    cookedLine = cookedLine.substring(0, firstLetter + REPLACEMENT_DIFFERENCE);
                }

                // Insert REPLACEMENT_WORD
                if (Character.isUpperCase(line.charAt(i - 1))) {
                    cookedLine += REPLACEMENT_WORD;
                } else if (Character.isUpperCase(line.charAt(firstLetter))) {
                    cookedLine += REPLACEMENT_WORD.substring(0, 1)
                        + REPLACEMENT_WORD.substring(1).toLowerCase();
                } else {
                    cookedLine += REPLACEMENT_WORD.toLowerCase();
                }

            } else {
                cookedLine += line.charAt(i);
            }

        }

        return cookedLine;

    }

    public static boolean isNonAlphabetic(String line, int i) {
        return !Character.isAlphabetic(line.charAt(i));
    }

    /**
     * Find if there is the letter 'r' preceded by a vowel
     * at the current index of the line.
     */
    public static boolean ruleOne(String line, int i) {
        return i > 1
            && (Character.toUpperCase(line.charAt(i)) == 'R')
            && VOWELS.contains(String.valueOf(Character.toUpperCase(line.charAt(i - 1))));
    }

    /**
     * Find if the current index of the line is a word ending with the letter 'a'.
     */
    public static boolean ruleTwo(String line, int i) {
        return i > 1
            && i < line.length()
            && isNonAlphabetic(line, i)
            && Character.toUpperCase(line.charAt(i - 1)) == 'A';
    }

    /**
     * Find a given word at the current index of the line.
     */
    public static boolean ruleThree(String word, String line, int i) {
        return i > word.length()
            && i < line.length()
            && isNonAlphabetic(line, i)
            && line.substring(i - word.length(), i).toUpperCase().equals(word)
            && isNonAlphabetic(line, i - word.length() - 1);
    }

    /**
     * Find if there is "eer" or "ir"
     */
    public static boolean exceptionOne(String line, int i) {
        return i > 3
            && i + 1 < line.length()
            && isNonAlphabetic(line, i)
            && Character.toUpperCase(line.charAt(i - 1)) == 'R'
            && ((Character.toUpperCase(line.charAt(i - 2)) == 'E'
                 && Character.toUpperCase(line.charAt(i - 3)) == 'E')
                || Character.toUpperCase(line.charAt(i - 2)) == 'I');
    }

    /**
     * Find if there is "oor"
     */
    public static boolean exceptionTwo(String line, int i) {
        return i > 3
            && i + 1 < line.length()
            && isNonAlphabetic(line, i)
            && Character.toUpperCase(line.charAt(i - 1)) == 'R'
            && (Character.toUpperCase(line.charAt(i - 2)) == 'O'
                && Character.toUpperCase(line.charAt(i - 3)) == 'O');
    }

}
