import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class ArrayPracticeTest {

  /**
   * Test of average method, of class ArrayPractice.
   */
  @Test
  public void testAverage() {
    System.out.println("average");
    int[] A = {10, 20, 30, 40, 50};
    double expResult = 30.0;
    double result = ArrayPractice.average(A);
    assertEquals(expResult, result, 0.1);

    int B[] = {31};
    expResult = 31.0;
    result = ArrayPractice.average(B);
    assertEquals(expResult, result, 0.1);
  }

  /**
   * Test of copy method, of class ArrayPractice.
   */
  @Test
  public void testCopy() {
    System.out.println("copy");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int[] expResult = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int[] result = ArrayPractice.copy(A);
    assertArrayEquals(expResult, result);

    int[] A2 = {21};
    int[] expResult2 = {21};
    int[] result2 = ArrayPractice.copy(A2);
    assertArrayEquals(expResult2, result2);
  }

  /**
   * Test of copyAll method, of class ArrayPractice.
   */
  @Test
  public void testCopyAll() {
    System.out.println("copyAll");
    int[] A = {10, 20, 30, 40, 50};
    int[] B = {60, 70, 80};
    int[] expResult = {10, 20, 30, 40, 50, 60, 70, 80};
    int[] result = ArrayPractice.copyAll(A, B);
    assertArrayEquals(expResult, result);
  }

  /**
   * Test of copyN method, of class ArrayPractice.
   */
  @Test
  public void testCopyN() {
    System.out.println("copyN");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int n = 5;
    int[] expResult = {21, 16, 5, 31, 8};
    int[] result = ArrayPractice.copyN(A, n);
    assertArrayEquals(expResult, result);

    n = A.length + 3;
    int[] expResult2 = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16, 0, 0, 0};
    result = ArrayPractice.copyN(A, n);
    assertArrayEquals(expResult2, result);

    n = -1;
    result = ArrayPractice.copyN(A, n);
    assertArrayEquals(null, result);

    n = 0;
    result = ArrayPractice.copyN(A, n);
    assertArrayEquals(null, result);
  }

  @Test
  void testCopyOdds() {
    assertArrayEquals(ArrayPractice.copyOdds(new int[] {1, 2, 3, 4, 5}), new int[] {1, 3, 5});
    assertArrayEquals(ArrayPractice.copyOdds(new int[] {2, 3, 4, 6, 8, 10}), new int[] {3});
    assertArrayEquals(ArrayPractice.copyOdds(new int[] {2, 4, 6}), null);
  }

  // If you choose to do this extra credit option
  // uncomment and fill in the body of this method
  //
  // @Test
  // void testUniques() {
  // fail("Not yet implemented");
  // }

  /**
   * Test of find method, of class ArrayPractice.
   */
  @Test
  public void testFind() {
    System.out.println("find");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int x = 37;
    int expResult = -1;
    int result = ArrayPractice.find(A, x);
    assertEquals(expResult, result);

    x = 16;
    expResult = 1;
    result = ArrayPractice.find(A, x);
    assertEquals(expResult, result);
  }

  /**
   * Test of findLast method, of class ArrayPractice.
   */
  @Test
  public void testFindLast() {
    System.out.println("findLast");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int x = 37;
    int expResult = -1;
    int result = ArrayPractice.findLast(A, x);
    assertEquals(expResult, result);

    x = 31;
    expResult = 3;
    result = ArrayPractice.findLast(A, x);
    assertEquals(expResult, result);

    x = 16;
    expResult = 11;
    result = ArrayPractice.findLast(A, x);
    assertEquals(expResult, result);
  }

  /**
   * Test of findN method, of class ArrayPractice.
   */
  @Test
  public void testFindN() {
    System.out.println("findN");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int item = 21;
    int n = 0;
    int expResult = -1;
    int result = ArrayPractice.findN(A, item, n);
    assertEquals(expResult, result);

    item = 21;
    n = A.length;
    expResult = 0;
    result = ArrayPractice.findN(A, item, n);
    assertEquals(expResult, result);

    item = 16;
    n = A.length;
    expResult = 1;
    result = ArrayPractice.findN(A, item, n);
    assertEquals(expResult, result);
  }

  /**
   * Test of indexOfLargest method, of class ArrayPractice.
   */
  @Test
  public void testIndexOfLargest() {
    System.out.println("indexOfLargest");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int expResult = 3;
    int result = ArrayPractice.indexOfLargest(A);
    assertEquals(expResult, result);
  }

  /**
   * Test of indexOfLargestOdd method, of class ArrayPractice.
   */
  @Test
  public void testIndexOfLargestOdd() {
    System.out.println("indexOfLargestOdd");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int expResult = 3;
    int result = ArrayPractice.indexOfLargestOdd(A);
    assertEquals(expResult, result);

    int[] B = {20, 16, 4, 32, 8, 2, 8, 2, 16, 4, 2, 16};
    expResult = -1;
    result = ArrayPractice.indexOfLargestOdd(B);
    assertEquals(expResult, result);
  }

  /**
   * Test of initialize method, of class ArrayPractice.
   */
  @Test
  public void testInitialize() {
    System.out.println("initialize");
    int[] A = new int[5];
    int initialValue = 5;
    ArrayPractice.initialize(A, initialValue);
    assertArrayEquals(new int[] {5, 5, 5, 5, 5}, A);
  }

  @Test
  void testIsSortedAscending() {
    assertTrue(ArrayPractice.isSortedAscending(new int[] {1}));
    assertTrue(ArrayPractice.isSortedAscending(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
    assertFalse(ArrayPractice.isSortedAscending(new int[] {2, 1, 3, 4, 5, 6, 7, 8, 9, 10}));
    assertFalse(ArrayPractice.isSortedAscending(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 8}));
  }

  /**
   * Test of largest method, of class ArrayPractice.
   */
  @Test
  public void testLargest() {
    System.out.println("largest");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int expResult = 31;
    int result = ArrayPractice.largest(A);
    assertEquals(expResult, result);
  }

  /**
   * Test of numOccurrences method, of class ArrayPractice.
   */
  @Test
  public void testNumOccurrences() {
    System.out.println("numOccurrences");
    int[] A = {21, 16, 5, 31, 8, 1, 9, 1, 16, 4, 2, 16};
    int x = 16;
    int expResult = 3;
    int result = ArrayPractice.numOccurrences(A, x);
    assertEquals(expResult, result);

    x = 37;
    expResult = 0;
    result = ArrayPractice.numOccurrences(A, x);
    assertEquals(expResult, result);

    x = 21;
    expResult = 1;
    result = ArrayPractice.numOccurrences(A, x);
    assertEquals(expResult, result);
  }

  @Test
  void testRemove() {
    int[] A = {10, 20, 30};

    ArrayPractice.remove(A, -1);
    assertArrayEquals(A, new int[] {10, 20, 30});

    ArrayPractice.remove(A, 3);
    assertArrayEquals(A, new int[] {10, 20, 30});

    ArrayPractice.remove(A, 0);
    assertArrayEquals(A, new int[] {20, 30, 0});

    int[] B = {10, 20, 30};
    ArrayPractice.remove(B, 2);
    assertArrayEquals(B, new int[] {10, 20, 0});
  }

  /**
   * Test of reverse method, of class ArrayPractice.
   */
  @Test
  public void testReverse() {
    System.out.println("reverse");
    int[] A = {10, 20, 30, 40, 50};
    int[] B = {50, 40, 30, 20, 10};
    ArrayPractice.reverse(A);
    assertArrayEquals(B, A);
  }

  @Test
  void testShiftLeft() {
    int[] A = {};
    int[] B = {};

    ArrayPractice.shiftLeft(new int[] {});
    assertArrayEquals(B, A);

    int[] C = new int[] {10, 20, 30, 40, 50};
    int[] D = new int[] {20, 30, 40, 50, 0};
    ArrayPractice.shiftLeft(C);
    assertArrayEquals(C, D);

    int[] E = new int[] {10};
    int[] F = new int[] {0};
    ArrayPractice.shiftLeft(E);
    assertArrayEquals(E, F);
  }

  @Test
  void testSortedAscendingRun() {
    int[] A = {1, 0, 1, 0, 1, 2, 1, 2, 3, 4};

    assertEquals(ArrayPractice.sortedAscendingRun(A, 0), 1);
    assertEquals(ArrayPractice.sortedAscendingRun(A, 1), 2);
    assertEquals(ArrayPractice.sortedAscendingRun(A, 3), 3);
    assertEquals(ArrayPractice.sortedAscendingRun(A, 6), 4);
  }
}
