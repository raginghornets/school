/**
 * Eric Nguyen
 * Section 008
 * Assignment 8 Warm Up with Objects
 * 2020-03-22
 *
 * Basic testing of the Task class.
 */

import java.time.LocalDateTime;

public class TaskMain {
  
  public static void main(String[] args) {
    Task t = new Task("Submit CIS 1068 Assignment 08", 1, LocalDateTime.of(2020, 3, 23, 13, 0), 180);
    System.out.println("TEST OVERDUE METHOD");
    testOverDue(t);
    System.out.println();
    
    System.out.println("TEST ACCESSOR METHODS");
    testAccessorMethods(t);
    System.out.println();
    
    System.out.println("TEST MUTATOR METHODS");
    testMutatorMethods(t);
  }

  public static void testAccessorMethods(Task t) {
    System.out.println("Task's name is \""
                       + t.getName()
                       + "\".");
    System.out.println("Task's estimated time to complete is "
                       + t.getEstMinsToComplete()
                       + " minutes.");
    System.out.println("Task's priority is "
                       + t.getPriority()
                       + ".");
    System.out.println("Task is due at "
                       + t.getWhenDue()
                       + ".");
  }

  public static void testMutatorMethods(Task t) {
    System.out.println("Before: " + t);
    t.setName("Write essay on the Epic of Gilgamesh");
    t.decreasePriority(3);
    t.setEstMinsToComplete(60);
    t.setWhenDue(LocalDateTime.of(2020, 3, 26, 11, 59));
    System.out.println("After: " + t);
  }

  public static void testOverDue(Task t) {
    boolean overdue = t.overdue();
    LocalDateTime now = LocalDateTime.now();
    
    System.out.println("My task, \""
                       + t.getName()
                       + "\" is "
                       + (overdue ? "" : "not ")
                       + "overdue.");
    System.out.println("It "
                       + (overdue ? "was " : "is ")
                       + "due at "
                       + t.getWhenDue()
                       + ".");
    System.out.println("It is now " + now + ".");
    if (!overdue) {
      System.out.println("You have "
                         + t.getWhenDue().compareTo(now)
                         + " days to complete this task.");
    }
  }
    
}
