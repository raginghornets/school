/**
 * Eric Nguyen
 * Section 008
 * Assignment 3. Draw a Pretty Picture
 * 2020-01-30
 * 
 * Print a pretty picture using
 * ASCII characters and make it
 * so that you can proportionately
 * change the picture's size using
 * a global constant, <code>SIZE</code>.
 * 
 * For my picture, I print the
 * EXO logo which looks something
 * like this:
 * <pre>
 *     ______
 *    /\    /\
 *   /  \  /  \
 *  /___ \/    \
 *  \    /\    /
 *   \  /  \  /
 *    \/____\/
 * </pre>
 * 
 * To fulfill the doubly-nested
 * loop requirement, I print it
 * consecutively.
 */

public class AsciiArt {

    static final int SIZE = 2;
    static final int NUM_LOGO = 5;
    static final int INITIAL_TOP_LENGTH = 6;
    static final int INITIAL_SPACING_AFTER_CENTER = 4;
    static final int INITIAL_SPACING_AFTER_SLASHES = 2;
    static final int INITIAL_SPOKE_LENGTH = 2;
    static final int LEFT_MARGIN = 1;
    static final int BOTTOM_MARGIN = 1;
    static final int NEW_LINE = 1;

    public static void main(String[] args) {

        if (SIZE < 0) {
  
            System.out.println("Invalid SIZE: " + SIZE);
 
        } else if (NUM_LOGO <= 0) {

            System.out.println("Invalid NUM_LOGO: " + NUM_LOGO);

        } else {

            printAllExoLogos();

        }

    }

    /**
     * Prints the EXO logo tiled in a square layout with the
     * dimensions N x N determined by the global constant, <code>SIZE</code>.
     */
    public static void printAllExoLogos() {

        // Print the first row
        printExoLogoRow();

        // Print the next rows
        if (BOTTOM_MARGIN == 0) {
       
            printJoinedExoLogoRows();

        } else {

            printSeparateExoLogoRows();

        }

    }

    /**
     * Prints each row of EXO logos, with the
     * top and bottom of each logo joined.
     * For example:
     * <pre>
     *    \/____\/
     *    /\    /\
     * </pre>
     */
    public static void printJoinedExoLogoRows() {

        for (int i = 0; i < NUM_LOGO - 1; i++) {
            printExoLogoRowNoTop();
        }

        System.out.println();

    }
     /**
     * Prints each row of EXO logos, with the
     * top and bottom of each logo separated.
     * For example:
     * <pre>
     *    \/____\/
     *     ______
     *    /\    /\
     * </pre>
     */   
    public static void printSeparateExoLogoRows() {

        for (int i = 0; i < NUM_LOGO - 1; i++) {

            if (i == 0) {
                printExoLogoBottomMargin();
            }

            printExoLogoRow();

            printExoLogoBottomMargin();

        }

    }

    /**
     * Prints extra new lines between each row.
     * For example:
     * <pre>
     *                       \/______\/
     *     \/______\/
     *      ________
     *     /\      /\         ________
     *                       /\      /\
     * </pre>
     */
    public static void printExoLogoBottomMargin() {

        for (int margin = 0; margin < BOTTOM_MARGIN + 1; margin++) {
            System.out.println();
        }

    }

    /**
     * Prints the EXO logo NUM_LOGO times horizontally.
     * For example:
     * <pre>
     *     ______
     *    /\    /\
     *   /  \  /  \
     *  /___ \/    \
     *  \    /\    /
     *   \  /  \  /
     *    \/____\/
     * </pre>
     */
    public static void printExoLogoRow() {

        // Print each top row
        for (int i = 0; i < NUM_LOGO; i++) {

            printExoLogoTop();

            // Print spacing of SIZE + 2 + LEFT_MARGIN
            printSpacingBeforeSlashes(-1);

        }

        printExoLogoRowNoTop();

        // For a single logo
        if (NUM_LOGO == 1 && NEW_LINE == 1) {
            System.out.println();
        }
        
    }

    /**
     * Prints the EXO logo without the top.
     * For example:
     *
     * <pre>
     *    /\    /\
     *   /  \  /  \
     *  /___ \/    \
     *  \    /\    /
     *   \  /  \  /
     *    \/____\/
     * </pre>
     */
    public static void printExoLogoRowNoTop() {

        System.out.println();

        // Print each body top row
        for (int row = 0; row < SIZE + 1; row++) {
            printExoLogoTopHalf(row);
        }

        // Print the body middle row
        for (int row = 0; row < NUM_LOGO; row++) {
            
            printExoLogoMiddle();

            // Print spacing of LEFT_MARGIN
            printSpacingBeforeSlashes(SIZE + 1);

        }

        System.out.println();

        // Print each body bottom row
        for (int row = SIZE + 1; row > 0; row--) {
            printExoLogoBottomHalf(row);
        }

        // Print each bottom row
        for (int row = 0; row < NUM_LOGO; row++) {

            printExoLogoBottom();

            // Print spacing of SIZE + 1 + LEFT_MARGIN
            printSpacingBeforeSlashes(0);

        }

    }

    /**
     * Prints the spacing before the first slash.
     * For example:
     * <pre>
     * /\     /\
     *   /\     /\
     *     /\     /\
     * </pre>
     */
    public static void printSpacingBeforeSlashes(int currentRow) {

        for (int i = 0; i < (SIZE + 1) - currentRow + LEFT_MARGIN; i++) {
            System.out.print(" ");
        }

    }

    /** 
     * Prints the spacing after the first set of slashes.
     * For example:
     * <pre>
     * /\ /\
     * /\  /\
     * /\   /\
     * </pre>
     */
    public static void printSpacingAfterSlashes(int currentRow) {

        int spacing = (SIZE * 2) - currentRow * 2 + INITIAL_SPACING_AFTER_SLASHES;

        for (int i = spacing; i > 0; i--) {
            System.out.print(" ");
        }

    }

    /**
     * Prints the spacing between the pairs of slashes.
     * For example:
     * <pre>
     * /\    /\
     * / \  / \
     * /  \/  \
     *
     * \  /\  /
     * \ /  \ /
     * \/    \/
     * </pre>
     */
    public static void printSpacingBetweenSlashes(int currentRow) {

        for (int i = 0; i < currentRow * 2; i++) {
            System.out.print(" ");
        }

    }

    /**
     * Prints '/' and then '\' with the spacing between
     * the slashes determined by the current row.
     * For example:
     * <pre>
     * /\
     * / \
     * /  \
     * </pre>
     */
    public static void printForwardSlashBackSlash(int currentRow) {

        System.out.print("/");
        printSpacingBetweenSlashes(currentRow);
        System.out.print("\\");

    }

    /**
     * Prints '\' and then '/' with the spacing between
     * the slashes determined by the current row.
     * For example:
     * <pre>
     * \/
     * \  /
     * \   / 
     * </pre>
     */
    public static void printBackSlashForwardSlash(int currentRow) {

        System.out.print("\\");
        printSpacingBetweenSlashes(currentRow);
        System.out.print("/");

    }

    /**
     * Prints something that looks like this:
     * <pre>
     *    /\    /\ 
     * </pre>
     */
    public static void printExoLogoTopHalfRow(int currentRow) {

        printSpacingBeforeSlashes(currentRow);

        // First '/\'
        printForwardSlashBackSlash(currentRow);

        printSpacingAfterSlashes(currentRow);

        // Second '/\'
        printForwardSlashBackSlash(currentRow);

    }

    /**
     * Prints something that looks like this:
     * <pre>
     *   \  /    \  /
     * </pre>
     */
    public static void printExoLogoBottomHalfRow(int currentRow) {

        printSpacingBeforeSlashes(currentRow);

        // First '\/'
        printBackSlashForwardSlash(currentRow);

        printSpacingAfterSlashes(currentRow);

        // Second '\/'
        printBackSlashForwardSlash(currentRow);

    }

    /** 
     * Prints something that looks like this:
     * <pre>
     *   /\    /\
     *  /  \  /  \
     * </pre>
     */
    public static void printExoLogoTopHalf(int row) {

        for (int col = 0; col < NUM_LOGO; col++) {

            printExoLogoTopHalfRow(row);
            printSpacingBeforeSlashes(row);

        }

        System.out.println();

    }

    /**
     * Prints something that looks like this:
     * <pre>
     * \    /\    /
     *  \  /  \  /
     * </pre>
     */
    public static void printExoLogoBottomHalf(int row) {

        for (int col = NUM_LOGO; col > 0; col--) {

            printExoLogoBottomHalfRow(row);
            printSpacingBeforeSlashes(row);

        }

        System.out.println();

    }

    /**
     * Prints something that looks like this:
     * <pre>
     * /___ \/    \
     * </pre>
     */
    public static void printExoLogoMiddle() {

        int spacingBetweenLastPairOfSlashes = (SIZE - 1) * 2 + INITIAL_SPACING_AFTER_CENTER;
        int spokeLength = SIZE + INITIAL_SPOKE_LENGTH;

        // Print spacing of LEFT_MARGIN
        printSpacingBeforeSlashes(SIZE + 1);

        // Print the initial slash
        System.out.print("/");

        // Print middle spoke for the letter 'E'
        for (int i = 0; i < spokeLength; i++) {
            System.out.print("_");
        }

        // Print the space between the spoke of the 'E' and the center of the logo
        for (int i = 0; i < spacingBetweenLastPairOfSlashes - spokeLength; i++) {
            System.out.print(" ");
        }

        // Print center of logo
        System.out.print("\\/");

        // Print the space between the center and the end of the middle line
        for (int i = 0; i < spacingBetweenLastPairOfSlashes; i++) {
            System.out.print(" ");
        }

        // End the line with a backslash
        System.out.print("\\");

    }

    /**
     * Prints something that looks like this:
     * <pre>
     *    ______
     * </pre>
     */
    public static void printExoLogoTop() {

        int lineLength = (SIZE - 1) * 2 + INITIAL_TOP_LENGTH;

        // Print spacing of SIZE + 2 + LEFT_MARGIN
        printSpacingBeforeSlashes(-1);

        // Print a long line
        for (int i = 0; i < lineLength; i++) {
            System.out.print("_");
        }

    }

    /**
     * Prints something that looks like this:
     * <pre>
     *   \/____\/
     * </pre>
     */
    public static void printExoLogoBottom() {

        int lineLength = (SIZE - 1) * 2 + INITIAL_TOP_LENGTH - 2;

        // Print spacing of SIZE + 1 + LEFT_MARGIN
        printSpacingBeforeSlashes(0);

        System.out.print("\\/");

        // Print a short line
        for (int i = 0; i < lineLength; i++) {
            System.out.print("_");
        }

        System.out.print("\\/");

    }

}
