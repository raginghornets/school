/**
 * Eric Nguyen
 * Section 008
 * Assignment 3. Extra credit: The Linc (parking lot and stadium)
 * 2020-02-02
 * 
 * Print a picture of the Lincoln Stadium
 * using ASCII characters and make it
 * so that you can proportionately
 * change the picture's size using
 * a global constant, <code>SIZE</code>.
 */

public class TheLinc {

    static final int SIZE = 4;
    static final int NUM_ROWS_TOP_HALF = SIZE + 5;
    static final int NUM_ROWS_BOTTOM_HALF = SIZE + 4;
    static final int BORDER_LENGTH = (SIZE + 5) * 4 - 2;
    static final int INITIAL_ROW_SPACING = 10 + (SIZE - 1) * 2;

    static final int FIELD_STARTING_ROW = SIZE - (SIZE - 4) / 2;
    static final int FIELD_WIDTH = SIZE * 2;

    static final int NUM_PARKING_COL = 2;
    static final int NUM_PARKING_ROW = 8 + SIZE * 2;
    static final int PARKING_LOT_TOP_LENGTH = SIZE * 4;
    static final int PARKING_LOT_LEFT_MARGIN = 9;
    static final int LOT_SIZE = SIZE * 2;
    
    public static void main(String[] args) {

        printParkingLot();

        System.out.println();

        printTheLinc();

    }

    public static void printTheLincRowSpacing(int row) {
 
        for (int spacing = INITIAL_ROW_SPACING - row * 2; spacing > 0; spacing--) {
            System.out.print(" ");
        }

    }

    public static void printTheLincInteriorTop(int row) {

        int fieldStart = SIZE + 2 * (row - SIZE - 1);
        int fieldEnd = fieldStart + FIELD_WIDTH;
        int interiorRowLength = (row - 1) * 2 + (row - 1) * 2 + 2;

        for (int i = 0; i < interiorRowLength; i++) {
            if (row > FIELD_STARTING_ROW && i > fieldStart && i <= fieldEnd) {
                System.out.print("*");
            } else {
                System.out.print(".");
            }
        }

    }

    public static void printTheLincInteriorBottom(int row) {

        int fieldStart = SIZE + 2 * (row - SIZE - 2) + 1;
        int fieldEnd = fieldStart + FIELD_WIDTH;
        int interiorRowLength = (row - 1) * 2 + (row - 1) * 2;

        for (int i = 0; i < interiorRowLength; i++) {
            if (row > FIELD_STARTING_ROW && i > fieldStart && i <= fieldEnd) {
                System.out.print("*");
            } else {
                System.out.print(".");
            }
        }

    }

    public static void printTheLincBorder() {

        for (int i = 0; i < BORDER_LENGTH; i++) {
            System.out.print("_");
        }

    }

    public static void printTheLincTopHalf() {

        for (int row = 0; row < NUM_ROWS_TOP_HALF; row++) {

            // Print the left border
            System.out.print("|");

            // Print the interior
            printTheLincRowSpacing(row);
            System.out.print("_");

            if (row > 0) {
                System.out.print("/");
            }

            printTheLincInteriorTop(row);
            
            if (row > 0) {
                System.out.print("\\");
            }

            System.out.print("_");
            printTheLincRowSpacing(row);

            // Print the right border
            System.out.print("|");
            System.out.println();

        }

    }

    public static void printTheLincBottomHalf() {

        for (int row = NUM_ROWS_BOTTOM_HALF; row > 0; row--) {

            // Print the left border
            System.out.print("|");

            // Print the interior
            printTheLincRowSpacing(row);
            System.out.print(" \\_");
            printTheLincInteriorBottom(row);
            System.out.print("_/ ");
            printTheLincRowSpacing(row);

            // Print the right border
            System.out.print("|");
            System.out.println();

        }

    }

    public static void printTheLincTopBorder() {

        System.out.print(" ");
        printTheLincBorder();
        System.out.println();

    }

    public static void printTheLincBottomBorder() {

        System.out.print("|");
        printTheLincBorder();
        System.out.print("|");
        System.out.println();

    }
    
    public static void printTheLinc() {

        printTheLincTopBorder();
        printTheLincTopHalf();
        printTheLincBottomHalf();
        printTheLincBottomBorder();
       
    }

    public static void printParkingLotMargin() {

        for (int i = 0; i < PARKING_LOT_LEFT_MARGIN; i++) {
            System.out.print(" ");
        }

    }

    public static void printParkingLotTop() {

        printParkingLotMargin();
        System.out.print(" ");

        for (int i = 0; i <= PARKING_LOT_TOP_LENGTH; i++) {
            System.out.print("_");
        }

        System.out.println();

    }

    public static void printParkingLotRows() {

        for (int row = 0; row < NUM_PARKING_ROW; row++) {

            printParkingLotMargin();

            // Print each lot for the row
            for (int col = 0; col < NUM_PARKING_COL; col++) {

                System.out.print("|");

                for (int lot = 0; lot < LOT_SIZE; lot++) {
                    System.out.print("_");
                }
                
            }

            // Print the ending border of the last lot in the row
            System.out.print("|");
            System.out.println();

        }

    }

    public static void printParkingLot() {

        printParkingLotTop();
        printParkingLotRows();
        
    }

}
