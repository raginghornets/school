/**
 * Eric Nguyen
 * Section 008
 * Assignment 4. A Game
 * 2020-02-10
 *
 * A rock-paper-scissors copycat
 * with five moves instead of three.
 */

import java.util.Scanner;
import java.util.Random;

public class Björksnäs {

    static final int GODISHUS = 0;
    static final int DERFLURG = 1;
    static final int KULLEN = 2;
    static final int KOPPANG = 3;
    static final int SONGESAND = 4;
    static final int[] MOVES = {
        GODISHUS, DERFLURG, KULLEN, KOPPANG, SONGESAND
    };
    static final int NUM_MOVES = MOVES.length;

    static final int[][] RULES = {
        {KOPPANG, KULLEN},     // beaten by Gödishus
        {SONGESAND, GODISHUS}, // beaten by Derflürg
        {DERFLURG, KOPPANG},   // beaten by Kullen
        {SONGESAND, DERFLURG}, // beaten by Koppang
        {KULLEN, GODISHUS}     // beaten by Songesand
    };

    static final String[] STR_MOVES = {
        "Gödishus", "Derflürg", "Kullen", "Koppang", "Songesand"
    };

    static final String PROMPT_PLAY_ROUND = "Do you wanna play? (y/n) ";
    static final String PROMPT_PLAY_ROUND_AGAIN = "Do you wanna play again? (y/n) ";
    static final String PROMPT_CHOOSE_MOVE = "What's your move? Choose one:";
    static final String PROMPT_USER_INPUT = "Your move: ";

    static final String MSG_INTRODUCTION =
        "Hi, everyone.\n"
        + "Today, we are going to play Björksnäs.";
    static final String MSG_LETS_PLAY =
        "Okay, let's play!\n"
        + "Here are the rules:\n";
    static final String MSG_PLAYER_WON = "You won!";
    static final String MSG_COMPUTER_WON = "The computer won!";
    static final String MSG_TIE = "It's a tie!";
    static final String MSG_GOODBYE = "Goodbye!";

    static int roundsPlayed = 0;
    static int timesPlayerWon = 0;
    static int timesComputerWon = 0;

    public static void main(String[] args) {

        Scanner kbd = new Scanner(System.in);
        Random rand = new Random();

        System.out.println(MSG_INTRODUCTION);

        playGame(kbd, rand);

        printGoodbye();

    }

    public static void welcome() {

        System.out.println(MSG_LETS_PLAY);
        printRules();
        System.out.println();

    }
   
    public static boolean playGame(Scanner s, Random r) {

        if (askUserToPlayARound(s)) {

            welcome();

            int playerMove = playerChooseMove(s);
            int computerMove = r.nextInt(NUM_MOVES);

            announceAndUpdateResults(playerMove, computerMove);

            return playGame(s, r);

        }

        return false;

    }

    public static int getPlayerMove(Scanner s) {

        int playerMove;

        do {

            System.out.print(PROMPT_USER_INPUT);

            while (!s.hasNextInt()) {

                System.out.print(PROMPT_USER_INPUT);
                s.next();

            }

            playerMove = s.nextInt();

        } while(playerMove < 0 || playerMove > 4);

        System.out.println();

        return playerMove;

    }

    public static int playerChooseMove(Scanner s) {

        System.out.println(PROMPT_CHOOSE_MOVE);
        System.out.println();

        for (int i = 0; i < MOVES.length; i++) {
            System.out.println("[" + i + "] " + STR_MOVES[i]);
        }

        System.out.println();

        return getPlayerMove(s);

    }

    public static boolean askUserToPlayARound(Scanner s) {

        if (roundsPlayed > 0) {
            System.out.print(PROMPT_PLAY_ROUND_AGAIN);
        } else {
            System.out.print(PROMPT_PLAY_ROUND);
        }

        return s.next().equals("y");

    }

    public static void printRule(int move, int[] otherMoves) {

        if (otherMoves.length > 0) {

            System.out.print(STR_MOVES[move]
                             + " beats " + STR_MOVES[otherMoves[0]]
                             + (otherMoves.length > 1 ?
                                " and " + STR_MOVES[otherMoves[1]] : "")
                             + ".");

            System.out.println();

        }

    }

    public static void printRules() {

        for (int i = 0; i < RULES.length; i++) {
            printRule(i, RULES[i]);
        }

    }

    public static boolean compareMoves(int move1, int move2) {

        boolean[] conditions = {
            (move2 == KOPPANG || move2 == KULLEN),     // Gödishus
            (move2 == SONGESAND || move2 == GODISHUS), // Derflürg
            (move2 == DERFLURG || move2 == KOPPANG),   // Kullen
            (move2 == SONGESAND || move2 == DERFLURG), // Koppang
            (move2 == KULLEN || move2 == GODISHUS)     // Songesand
        };

        return conditions[move1];

    };

    public static void announceWinner(int winningMove, int losingMove, String winningMsg) {

        printRule(winningMove, new int[] {losingMove});
        System.out.println(winningMsg);

    }

    public static void announceAndUpdateResults(int playerMove, int computerMove) {

        System.out.println("Your move: " + STR_MOVES[playerMove]);
        System.out.println("Computer's move: " + STR_MOVES[computerMove]);
        System.out.println();

        // Announce winner: player? computer? tie.
        if (compareMoves(playerMove, computerMove)) {

            timesPlayerWon += 1;
            announceWinner(playerMove, computerMove, MSG_PLAYER_WON);

        } else if (compareMoves(computerMove, playerMove)) {

            timesComputerWon += 1;
            announceWinner(computerMove, playerMove, MSG_COMPUTER_WON);

        } else {
            System.out.println(MSG_TIE);
        }

        System.out.println();

        roundsPlayed += 1;

    }

    public static void printGoodbye() {

        System.out.println("You played " + roundsPlayed + " rounds.");
        System.out.println("You won " + timesPlayerWon + " times.");
        System.out.println("The computer won " + timesComputerWon + " times.");
        System.out.println(MSG_GOODBYE);

    }

}
