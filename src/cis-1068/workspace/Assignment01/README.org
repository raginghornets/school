#+title: CIS 1068 Assignment 1 Greetings

Due: Friday, January 24

20 points

* description

Your program will consist of a single Java file, which you should call Greeting.java. The program should print a message mentioning your lab instructor by name
For example, your program might print something like

#+begin_example
Good morning, Professor Dumbledore.
#+end_example

(that is, if you're in Professor Dumbledore's lab section this semester.)
Include a comment at the beginning of your program with some basic information about the assignment, for example:

#+begin_src java :eval no
// Your Name
// The Date
// Assignment 1. Greetings
//
// Very brief description of what the program does
//
#+end_src

The purpose of this assignment is to make sure that you have the needed university account, that you can edit, compile, and run a Java program, and that you are able to upload your assignment. Though the official due date is not until Friday, January 24, you are strongly encouraged to set up your development environment and begin to practice writing short programs as soon as possible.

* how to submit

Please submit your assignment through Canvas. Your laboratory instructor will go over how do to this in lab.
