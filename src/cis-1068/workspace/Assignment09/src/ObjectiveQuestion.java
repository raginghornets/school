/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Extends the Question class to include a correct answer.
 *
 */

class ObjectiveQuestion extends Question {
  protected String correctAnswer;
  public ObjectiveQuestion(int points, int difficulty, int answerSpace, String questionText, String correctAnswer) {
    super(points, difficulty, answerSpace, questionText);
    this.correctAnswer = correctAnswer;
  }
  public ObjectiveQuestion(String questionText, String correctAnswer) {
    super(questionText);
    this.correctAnswer = correctAnswer;
  }
  public String getCorrectAnswer() {
    return this.correctAnswer;
  }
  public String answerToString() {
    return this.questionText + "\n\n**** " + this.correctAnswer + " ****";
  }
}
