/**
 * Eric Nguyen
 * Section 008
 * Assignment 9 
 * 2020-04-05
 *
 * Extends the ObjectiveQuestion to format the correct answer into 
 * a fill in the blank style question.
 *
 */

class FillInTheBlankQuestion extends ObjectiveQuestion {
  private static final String BLANK = "______";
  private String questionText1;
  private String questionText2;
  public FillInTheBlankQuestion(int points, int difficulty, int answerSpace, String questionText1, String correctAnswer, String questionText2) {
    super(points, difficulty, answerSpace, questionText1 + BLANK + questionText2, correctAnswer);
    this.questionText1 = questionText1;
    this.questionText2 = questionText2;
  }
  public FillInTheBlankQuestion(String questionText1, String correctAnswer, String questionText2) {
    super(questionText1 + BLANK + questionText2, correctAnswer);
    this.questionText1 = questionText1;
    this.questionText2 = questionText2;
  }
  public String answerToString() {
    return this.questionText1 + BLANK.substring(0,3) + this.correctAnswer + BLANK.substring(0,3) + this.questionText2;
  }
}
