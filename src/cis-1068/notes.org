#+title: Lecture Notes

* 2020-01-28

  - User input with =Scanner=

  - =if= / =else= statements

  - Relational operators

  - Nested =if= / =else=

  - Logical operators

  - =return= in =if= statements 

* 2020-01-30

  - Last time

    - Scanner
      
      - importing

      - declaring/creating

      - methods

    - tokens

    - conditional execution
      
  - String comparison

    - A String represents the location in memory at which the actual array of characters is stored

* 2020-02-04

  - while loop

  - sentinel: value that signals end of user input

    - sentinel loop: repeats until a sentinel value is seen
      
  - Random numbers
