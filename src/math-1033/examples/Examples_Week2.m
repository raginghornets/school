%% Week 2 Examples
% 
% Author: Eric Nguyen
%

%% Example 1

A = [ 1 2 3
      4 5 6 ];

B = [ 6 6 3
      3 0 1 ];
  
C = [ 1 -1 1
      1  1 1];

a = A + B;
b = A - B;
c = A + 2 * C;
d = A * transpose(B);
e = transpose(B) * A;
f = A .^ 2;
g = dot(A(1,:), A(2,:));
h = sum(A);
i = cumsum(B,2);
j = prod(C,1);

%% Example 2

A = [ 1 0
      0 1 ];
  
B = [ 1 2
      3 4 ];
  
a = A * B;
b = A .* B;
c = A / B;
d = A ./ B;

%% Example 3

v = 1:5;
w = 4:8;
x = v ./ w;

%% Example 4

v = 5:-1:2;

a = 1 ./ (v + v);
b = v .^ v;
c = v ./ sqrt(v);
d = (v .^ 2) ./ (v .^ 5);

%% Example 5

x = 1:0.1:3;

a = x / 2;
b = 1 ./ x;
c = (x .^ 2) - (4 * x);
d = (x + 1) ./ (x + 2);
e = sin(x .^ 2);
f = sin(x) .^ 2;
g = sin(2 * sqrt(x)) + x .* sin(x);
h = max(sin(x .^ 2));
i = min(sin(x) .^ 2);
j = diff(sin(2 * sqrt(x)) + x .* sin(x));

%% Example 6

v = 1:5000;
w = cumsum(1 ./ v .^ 2);
k = w([5 50 500 5000]);
compare = [k(end) ((pi ^ 2) / 6)];

%% Example 7

v = 0:25;
w = cumsum(v ./ factorial(v));
k = w([5 15 25]);
compare = [k(end), exp(1)];

%% Example 8

A = [  1  2  3
       4  5  6
       7  8 10 ];

B = [ 1
      2
      3 ];

x = A \ B;
