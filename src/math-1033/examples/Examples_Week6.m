% Week 6 Examples
%
% Author: Eric Nguyen
%

%% Example 1
for n = 1:10
    fprintf('%d \t %f\n', n, sqrt(n));
end

%% Example 2
s = 0;
for k = 5:25
    s = s + 4 * (2 / 3) ^ k;
end
disp(s);

%% Example 3
s = 0;
p = 1;
for k = 1:40
    s = sqrt(2 + s);
    p = p * s/2;
end
fprintf("%f \t %f\n", (2 / pi), p);

%% Example 4
v = zeros(4,1);
for i = 1:length(v)
    v(i) = input('Number: ');
end
disp(v);

%% Example 5
s = 0;
for k = 1:50
    s = s + (1 / k);
end
disp(s);

%% Example 6

%% Example 7

%% Example 8

%% Example 9
m = 3;
n = 5;

% Rectangle
for i = 1:m
    for j = 1:n
        fprintf('*');
    end
    fprintf('\n');
end

fprintf('\n');

% Triangle
m = 10;

for i = 1:m
    for j = 1:i
        fprintf('*');
    end
    fprintf('\n');
end

%% Example 10

%% Example 11
a = randi([0, 1000]);
b = randi([0, 1000]);
disp(a);
disp(b);
while (b > a)
    a = b;
    b = randi([0, 1000]);
    disp(a);
    disp(b);
end

%% Example 12
v = zeros(100, 1);
v(1) = randi([0, 1000]);
v(2) = randi([0, 1000]);
i = 1;
while (v(i+1) > v(i))
    v(i) = v(i+1);
    v(i+1) = randi([0, 1000]);
    i = i + 1;
end
disp(v);

%% Example 13

%% Example 14

%% Example 1
for n = 1:10
    fprintf('%d \t %f\n', n, sqrt(n));
end

%% Example 2
v = cumsum(4 * (2 / 3) .^ 1:5);
disp(v)
v = cumsum(4 * (2 / 3) .^ 1:25);
disp(v)
