% Week 7 Examples
%
% Author: Eric Nguyen
%

%% Example 1
kilo = 65;
pound = kilo_to_pound(kilo);
disp(pound);

%% Example 2
a = 1;
b = 4;
c = 4;
[x1 x2] = roots(a,b,c);
disp([x1 x2]);

%% Example 3
x = randi([-10, 10], 1, 1);
disp(x);
disp(abs_val(x));

%% Example 4
f = @(x) (x + 1);
disp(f(0:5));
f = @(x) (x .^ 2 + 1);
disp(f(0:5));
f = @(x) (cos(x .^ 2 + 1));
disp(f(0:5));

%% Example 5
V = @(r, h) (pi * (r ^ 2) * h);
r = 2;
h = 10;
fprintf("A cylinder with a radius of %d cm and height of %d cm is %.2f cm^3.\n", r, h, V(r, h));

%% Functions
function pound = kilo_to_pound(kilo)
    pound = kilo / 2.2;
end

function [x1 x2] = roots(a, b, c)
    x1 = (-b + sqrt(b^2 - 4*a*c)) / (2*a);
    x2 = (-b - sqrt(b^2 - 4*a*c)) / (2*a);
end

function y = abs_val(x)
    if (x >= 0)
        y = x;
    else
        y = -x;
    end
end