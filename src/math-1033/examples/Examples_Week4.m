% Week 4 Examples
%
% Author: Eric Nguyen
%

%% Example 1
t = linspace(0, 6 * pi, 1000);
x = sqrt(t) .* cos(2 * t);
y = sqrt(t) .* sin(2 * t);

figure;
plot3(x, y, t);
shg();

%% Example 2
t = 0:(pi / 6):(6 * pi);
x = sqrt(t) .* cos(2 * t);
y = sqrt(t) .* sin(2 * t);

hold on;
plot3(x, y, t, 'or', 'MarkerFace', 'r');
shg();

%% Example 3
x = linspace(-1, 3, 1000);
y = linspace(1, 4, 1000);

[X Y] = meshgrid(x, y);
Z = (X .* (Y .^ 2)) ./ ((X .^ 2) + (Y .^ 2));

%%
figure;
mesh(X, Y, Z);

%%
figure;
surf(X, Y, Z);

%%
figure;
waterfall(X, Y, Z);

shg();

%% Example 4
x = linspace(-10, 10, 100);
y = linspace(-10, 10, 100);

[X Y] = meshgrid(x, y);
Z = X .^ 2 + Y .^ 2;

%%
figure;
surf(X, Y, Z);

%%
figure;
surfc(X, Y, Z);

%%
figure;
contour3(X, Y, Z);

%%
figure;
contour(X, Y, Z);

%% Relational Expressions

a = randi([0, 2], 1, 10);
b = randi([0, 2], 1, 10);
grades = [87 8 0 45 21 13 63 72 84 72 96 25 0 37 64 74 93 51 64 80 36 7 99 54 76 77 4 93 82 22 87];