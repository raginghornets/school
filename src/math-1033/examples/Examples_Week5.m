% Week 5 Examples
%
% Author: Eric Nguyen
%

%% Example 1
% (a)
n = input('Number: ');
m = sqrt(n);
disp(m);

% (b)
if (n < 0)
    disp('Warning');
    disp(sqrt(abs(n)));
end

% (c)
if (n < 0)
    disp('Warning');
else
    disp(sqrt(n));
end

%% Example 2
hrs = input('Number of hours: ');
if (hrs > 40)
    disp("You're getting paid over time");
end

%% Example 3
angle = input('Angle: ');
units = input('Units: ');
if (strcmp(units, 'd'))
    disp(sind(angle))
else
    disp(sin(angle))
end

%% Example 4
x = -3:0.01:4;
y = 4 * (x < -2) + x .^ 2 .* (x >= -2 & x <= 3) + 9 .* (x > 3);
plot(x, y);

%% Example 5
X = input('Input: ');

if (size(X, 1) == 1 && size(X, 2) == 2)
    disp('Scalar');
elseif (size(X, 1) > 1 && size(X, 2) == 1)
    disp('Column vector')
elseif (size(X, 1) == 1 && size(X, 2) > 1)
    disp('Row vector')
else
    disp('Matrix')
end

%% Example 7
year = input('Year: ');
month = input('Month: ');

switch month
  case {4, 6, 9, 11}
    day = 30;
  case {1, 3, 5, 7, 8, 10, 12}
    day = 31;
  case 2
    if (mod(year, 4) == 0)
        day = 29;
    else
        day = 28;
    end
end

disp(day);