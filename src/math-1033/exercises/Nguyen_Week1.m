%% Week 1 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-01-20
%

%% Chapter 1 - Exercise 8
% Converting pounds to kilograms
pounds = 160;
kilos = pounds / 2.2;

%% Chapter 1 - Exercise 9
% Converting Fahrenheit to Celsius
ftemp = 42;
ctemp = (ftemp - 32) * (5 / 9);

%% Chapter 1 - Exercise 30
% Converting polar coordinates to rectangular coordinates
r = 6;
theta = 55;
x = r * cos(theta);
y = r * sin(theta);

%% Chapter 1 - Exercise 31
% The Lorentz factor of an object moving at speed v
c = 3e8;
v = 2e8;
lorentz = 1 / (sqrt(1 - (v^2 / c^2)));

%% Chapter 1 - Exercise 32
% Min and max tolerable weight difference
weight = 34;
N = 2;
min_weight = weight - weight * (N / 100);
max_weight = weight + weight * (N / 100);

%% Chapter 1 - Exercise 34
% Maximum concentration of the pollutant produced at a given distance $x$
x = 78;
A = 902;
C = (A / x) * sqrt(2 / (pi * exp(1)));

%% Chapter 1 - Exercise 35
% Geometric mean
g = (1.15 * 1.50 * 1.30) ^ (1/3);

%% Chapter 1 - Additional Exercise 1
% Determine the monthly payment
P = 450000;
r = 0.042;
n = 1;
M = P * ((r / 12) * ((1 + (r / 12)) ^ (12 * n))) / ((1 + (r / 12)) ^ (12 * n) - 1);

%% Chapter 2 - Exercise 5
% Use the colon operator
v1 = 2:7;
v2 = 1.1:0.2:1.7;
v3 = 8:-2:2;

%% Chapter 2 - Exercise 6
% Use a built-in function to create an equally spaced vector
vec = linspace(-pi, pi, 20);

%% Chapter 2 - Exercise 10
% Vector with random length between 5 and 9 with steps of 3
myend = (9 - 5) * rand + 5;
new_vec = 1:3:myend;

%% Chapter 2 - Exercise 11
% Create a column vector
myvec = transpose(-1:0.5:1);

%% Chapter 2 - Exercise 12
% Get elements with odd indices

% Test vector of odd length
odd_vec = 1:7;
odd_odd_vec = odd_vec(1:2:end);

% Test vector of even length
even_vec = 1:2:16;
odd_even_vec = even_vec(1:2:end);

%% Chapter 2 - Exercise 13
% Modify a matrix
mat = zeros(2,4);
mat(1,:) = 1:4;
mat(:,3) = [1;2];

%% Chapter 2 - Exercise 17
% Make matrices with random values
rmat = rand(2,3);
rmat10 = rand(2,3) * 10;
rimat5t20 = randi([5,20], 2, 3);

%% Chapter 2 - Exercise 18
% Make a matrix of random dimensions
rows = randi(5);
cols = randi(5);
rcmat = zeros(rows, cols);

%% Chapter 2 - Additional Exercise 1
% Combine two row vectors
v = 41:-3:29;
w = 17:4:37;
u = [w v];

%% Chapter 2 - Additional Exercise 2
% Combine two column vectors
x = transpose(5:5:25);
y = transpose(27:2:33);
z = [x; y];

%% Chapter 2 - Additional Exercise 3
% Create a custom matrix by stacking rows
d = [6 -1 4 0 -2 5];
e = [7 5 9 0 1 3];
A = [d(2:4); e(3:5); d(4:6)];

%% Chapter 2 - Additional Exercise 4
% Create a custom matrix by combining columns
B = [transpose(d(2:5)) transpose(e(3:6))];
