%% Week 7 Exercises
%
% Author: Eric Nguyen
%
% Date: 2020-02-27
%

%% Chapter 3 Exercises

%% Exercise 27
% Create a matrix where all of its values are five
rows = randi(5);
cols = randi(5);
fprintf("%d ❌ %d matrix of fives:\n", rows, cols);
disp(fives(rows, cols));

%% Exercise 30
% Determine whether a given triplet of integers is a Pythagorean triple
a = randi(100);
b = randi(100);
c = randi(100);
fprintf("The triplet (%d, %d, %d) is ", a, b, c);
if (~ispythag(a, b, c))
    fprintf("not ");
end
fprintf("a Pythagorean triple.\n\n");

%% Exercise 32
% Get a random element from a vector
x = randi([-10, 10], 1, 10);

fprintf("A random element from the vector [%s] is %d.\n\n",...
        strtrim(sprintf("%d ", x)), pickone(x));

%% Chapter 6 Exercises

%% Exercise 2
% Convert kilometers to miles and US nautical miles
K = randi(100);
[miles us_miles] = kilotomiles(K);
fprintf("%d kilometers can be represented as %.2f miles or %.2f US nautical miles.\n\n",...
        K, miles, us_miles);

%% Exercise 3
% Split a vector into two;
% one containing its positive elements and
% the other containing its negative elements
v = randi([-100, 100], 1, 10);
[v_pos v_neg] = splitem(v);
fprintf("The vector [%s] can be split into two vectors, one containing its positive elements [%s] and negative elements [%s].\n\n",...
        strtrim(sprintf("%d ", v)),...
        strtrim(sprintf("%d ", v_pos)),...
        strtrim(sprintf("%d ", v_neg)))

%% Exercise 9
% Get a random row from a matrix
M = randi([-10, 10], 5);
fprintf("From the matrix\n");
disp(M);
fprintf("a random row is [%s].\n\n", strtrim(sprintf("%d ", randrow(M))));

%% Exercise 10
% Print how many times it happened
count = randi(10);
ithappened(count);

%% Exercise 11
% Plot a sin graph
x = (-2 * pi):(pi / 64):(2 * pi);
min = -pi;
max = pi;
plotsin(x, min, max);

%% Chapter 10 Exercises

%% Exercise 17
% Convert fluid ounces to milliliters
floz = randi(100);
floztoml = @(floz) (29.57 * floz);
fprintf("%d fl oz is equivalent to %.2f ml.\n", floz, floztoml(floz));

%% Exercise 19
% Plot a quadratic function
f = @(x) ((3 .* x .^ 2) - (2 .* x) + (5));
figure;
fplot(f);

%% Functions
%    __    __     __    __
%   |  \__|  |___|  |__|  |
%   \ *   *    *    *   * /
%    \___________________/
%    |./ _/ \_   _/ \_ \`|
%    |`\ \____|_|____/ /.|
%    |+'\|           |/+,|
%    |.'.| FUNCTIONS |.`'|
%    |``/|___________|\,.|
%    |,/ /_  _| |_  _\ \,|
%    |_\___\/_____\/___/_|
%       \ , \     /`  /
%        \   \   / , /
%         \ ` \ / ` /
%          | . |   |
%           |   , |
%            |   |
%            |   |
%            |`  |
%            |   |
%            |   |
%            |   |
%            |  ,|
%            |   |
%           _|   |_
%         .`       `.
%        /  `       ;\
%       ;           ,;;
%        \``       ,;/
%         \`       ,/
%          \`     ./
%           \    ,/
%            \   /
%             \_/
%

%% Chapter 3

%% Exercise 27
function M = fives(rows, columns)
    M = ones(rows, columns) * 5;
end

%% Exercise 30
function p = ispythag(a, b, c)
    p = (a^2 + b^2 == c^2);
end

%% Exercise 32
function y = pickone(x)
    y = x(randi(length(x)));
end

%% Chapter 6

%% Exercise 2
function [miles us_miles] = kilotomiles(K)
    miles = K * 0.621;
    us_miles = K * 1.852;
end

%% Exercise 3
function [v_pos v_neg] = splitem(v)
    v_pos = v(v >= 0);
    v_neg = v(v < 0);
end

%% Exercise 9
function row = randrow(M)
    row = M(randi(length(M)), :);
end

%% Exercise 10
function ithappened(count)
    if (count > 1)
        fprintf("It happened %d times.\n\n", count);
    elseif (count == 1)
        fprintf("It happened %d time.\n\n", count);
    else
        fprintf("Nothing happened.\n\n");
    end
end

%% Exercise 11
function plotsin(x, min, max)
    figure;
    plot(linspace(min, max, length(x)), sin(x));
end
