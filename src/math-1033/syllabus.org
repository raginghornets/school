#+setupfile: ~/.emacs.d/org-templates/level-1.org
#+title: MATH 1033, Computing in MATLAB

* Course Times and Locations
  :PROPERTIES:
  :CUSTOM_ID: course-times-and-locations
  :END:

  TR 11:00 AM -- 12:20 PM.

* Course Instructors
  :PROPERTIES:
  :CUSTOM_ID: course-instructors
  :END:

  Faycal Chaoqui \\
  Wachman 516 \\
  [[mailto:tuk94307@temple.edu][tuk94307@temple.edu]] \\
  Office Hours: To be determined

* Course Textbook
  :PROPERTIES:
  :CUSTOM_ID: course-textbook
  :END:

  Stormy Attaway. /MATLAB: A Practical Introduction to Programming and Problem Solving./
  Butterworth-Heinemann, 4^{th} edition.

* Course Material
  :PROPERTIES:
  :CUSTOM_ID: course-material
  :END:

  Topics include vectors and matrices, graphics, conditional operators, loops, and functions.
  The prerequisite for this course is Math 1022 with a grade of C or higher or the
  co-requisite is Math 1039.
  No prior programming or MATLAB skills are required.

* Course Assessments
  :PROPERTIES:
  :CUSTOM_ID: course-assessments
  :END:

  There will be seven (weekly) assignments throughout the semester.
  Late assignment submissions will not be accepted.

* Course Grades
  :PROPERTIES:
  :CUSTOM_ID: course-grades
  :END:

  Final grades will be calculated using the best six assignment submissions.
  Final letter grades will be assigned according to the following scale:

  | 93--100 | A  |
  |  90--92 | A- |
  |  87--89 | B+ |
  |  83--86 | B- |
  |  80--82 | B- |
  |  77--79 | C+ |
  |  73--76 | C  |
  |  70--72 | C- |
  |  67--69 | D+ |
  |  63--66 | D  |
  |  60--62 | D- |
  |    < 60 | F  |

  Grades are based only on your performance in the course.
  Outside factors (such as the need to graduate, to stay in a major, to maintain
  a scholarship, to play a sport, etc) play no factor in the grade earned, and grades
  will not be arbitrarily changed out of fairness to individual and all students.

  The grade I (an incomplete) is only given if students cannot complete the course
  work due to circumstances beyond their control.
  It is necessary for the student to have completed the majority of the course work
  with a passing average and to sign an incomplete contract which clearly states
  what is left for the student to do and the deadline by which the work must be
  completed.
  The incomplete contract must also include a default grade that will be used
  in case the I grade is not resolved by the agreed deadline.

* OUTDATED Important Registration Dates
  :PROPERTIES:
  :CUSTOM_ID: important-registration-dates
  :END:

  Last day to drop or add a 7-week (7B) course: Monday, October 22 \\
  Last day to withdraw from a 7-week (7B) course: Monday, November 12 \\
  Classes suspended: Monday, November 19 -- Friday, November 23 \\
  Last day of classes: Monday, December 10

  During the Drop/Add period, students may drop a course with no record of the class
  appearing on their transcript.
  Students are not financially responsible for any courses dropped during this period.
  In the following weeks prior to or on the withdrawal date, students may withdraw
  from a course with the grade of W appearing on their transcript.
  After the withdrawal date, students may not withdraw from courses.

* Student Accommodations
  :PROPERTIES:
  :CUSTOM_ID: student-accommodations
  :END:

  Any student who has a need for accommodation based on the impact of a disability
  should contact me privately to discuss the specific situation as soon as possible.
  Contact Disability Resources and Services at 215.204.1280, 100 Ritter Annex, to
  coordinate reasonable accommodations for students with documented disabilities.

* Academic Rights and Responsibilities
  :PROPERTIES:
  :CUSTOM_ID: academic-rights-and-responsibilities
  :END:

  Freedom to teach and freedom to learn are inseparable facets of academic freedom.
  The University has adopted a policy on Student and Faculty Academic Rights and
  Responsibilities (Policy #03.70.02) which can be accessed through the following
  link: [[https://policies.temple.edu/getdoc.asp?policy_no=03.70.02]]
