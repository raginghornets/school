#+TITLE: CIS3223 Review
#+AUTHOR: Eric Nguyen

* Chapter 0

** 0.1(a)

\[\lim_{n \to \infty} \frac{n - 100}{n - 200} = \frac{1}{2} \implies f = \Theta(g)\]

** 0.1(b)

\[\lim_{n \to \infty} \frac{n^{1/2}}{n^{2/3}} = 0 \implies f = O(g)\]

** 0.1(c)

\begin{align*}
\lim_{n \to \infty} \frac{100n + \log{n}}{n + (\log{n})^2} &= \lim_{n \to \infty} \frac{100n + \log{n}}{n + 2\log{n}} \\
&= 100 \implies f = \Theta(g)
\end{align*}

** 0.1(d)

\begin{align*}
\lim_{n \to \infty} \frac{n \log{n}}{10n \log{10n}} &= \lim_{n \to \infty} \frac{\log{n}}{10 \log{10} + 10 \log{n}} \\
&= \frac{1}{10} \implies f = \Theta(g)
\end{align*}

** 0.1(e)

\begin{align*}
\lim_{n \to \infty} \frac{\log{2n}}{\log{3n}} &= \lim_{n \to \infty} \frac{\log{2} + \log{n}}{\log{3} + \log{n}} \\
&= 1 \implies f = \Theta(g)
\end{align*}

** 0.1(f)

\begin{align*}
\lim_{n \to \infty} \frac{10 \log{n}}{\log{(n^2)}} &= \lim_{n \to \infty} \frac{10 \log{n}}{2 \log{n}} \\
&= 5 \implies f = \Theta(g)
\end{align*}

** 0.1(g)

\begin{align*}
\lim_{n \to \infty} \frac{n^{1.01}}{n \log^2{n}} &= \infty \implies f = \Omega(g)
\end{align*}

** 0.1(h)

\begin{align*}
\lim_{n \to \infty} \frac{n^2 / \log{n}}{n(\log{n})^2} &= \lim_{n \to \infty} \frac{n^2}{4n\log{n}} \\
&= \infty \implies f = \Omega(g)
\end{align*}

** 0.1(i)

\begin{align*}
\lim_{n \to \infty} \frac{n^{0.1}}{(\log{n})^{10}} &= \infty \implies f = \Omega(g)
\end{align*}

* Chapter 1

** Problem 1.18

\begin{align*}
210 &= 2 \cdot 5 \cdot 7 \cdot 3 \\
588 &= 2^2 \cdot 7^2 \cdot 3 \\
\implies \gcd(210, 588) &= 2 \cdot 7 \cdot 3 = 42
\end{align*}

\begin{align*}
\gcd(210, 588) &= \gcd(588, 210) \\
&= \gcd(210, 168) \\
&= \gcd(168, 42) \\
&= \gcd(42, 0) \\
&= 42
\end{align*}

** Properties of modular arithmetic

Associativity.
\[x + (y + z) \equiv (x + y) + z \pmod N\]

Commutativity.
\[xy \equiv yx \pmod N\]

Distributivity.
\[x(y + z) \equiv xy + yz \pmod N\]

These properties help modular exponentiation.
\[2^{345} \equiv (2^5)^{69} \equiv 32^{69} \equiv 1^{69} \equiv 1 \pmod{31}\]
\begin{align*}
2^{90} &\equiv (2^5)^{18} \\
&\equiv 32^{18} \\
&\equiv 6^{18} \\
&\equiv (6^2)^9 \\
&\equiv 32^9 \\
&\equiv 10^9 \\
&\equiv (10^3)^3 \\
&\equiv 12^3 \\
&\equiv 12 \pmod{13}
\end{align*}

** Fermat's little theorem

#+begin_src julia
function isprime(N)
    a = rand(1:N)
    return a^(N - 1) % N == 1
end
#+end_src

** RSA

1. Pick any two primes \(p\) and \(q\) and let \(N = pq\).
2. For any \(e\) relatively prime to \((p - 1)(q - 1)\):

   a. The mapping \(x \mapsto x^e \bmod{N}\) is a bijection on \(\{0, 1, \ldots, N - 1\}\).

   b. Moreover, the inverse mapping is easily realized: let \(d\) be the inverse of \(e\) modulo \((p - 1)(q - 1)\).
      Then for all \(x \in \{0, \ldots, N - 1\}\),
      \[(x^e)^d \equiv x \bmod{N}\]

   c. The encryption of a message \(M\) is \(M^e \bmod{N}\).

** RSA Example

Let \(N = 55 = 5 \cdot 11\).
Choose encryption exponent \(e = 3\), which satisfies the condition \(\gcd(e, (p - 1)(q - 1)) = \gcd(3, 40) = 1\).
The decryption exponent is then \(3^{-1} \bmod{40} = 27\).
Now for any message \(x \bmod{55}\), the encryption of \(x\) is \(y = x^3 \bmod{55}\), and the decryption is \(x = y^{27} \bmod{55}\).
So, for example, if \(x = 13\), then \(y = 13^3 = 52 \bmod{55}\) and \(13 = 52^{27} \bmod{55}\).

** Inverse modulo

#+begin_src julia
function myinvmod(A, C)
    for B ∈ 0:(C-1)
        if mod(A*B, C) == 1
            return B
        end
    end
    return A
end
#+end_src
** Modular exponentiation

\begin{align*}
2^{32} \bmod{7} &= (2^3)^{10} \cdot 4 \bmod{7} \\
&= 8^{10} \cdot 4 \bmod{7} \\
&= 1^{10} \cdot 4 \bmod{7} \\
&= 4 \bmod{7} \\
&= 4
\end{align*}

* Chapter 2

** 2.5(a)

\[T(n) = 2T(n/3) + 1\]

\begin{align*}
a &= 2 \\
b &= 3 \\
k &= \log_3{2} \\
f(n) &= 1 \\
T(n) &= \Theta(n^{\log_3{2}})
\end{align*}

** 2.5(b)

\[T(n) = 5T(n/4) + n\]

\begin{align*}
a &= 5 \\
b &= 4 \\
k &= \log_4{5} \\
f(n) &= n \\
T(n) &= \Theta(n^{\log_4{5}})
\end{align*}

** 2.5(c)

\[T(n) = 7T(n/7) + n\]

\begin{align*}
a &= 7 \\
b &= 7 \\
k &= \log_7{7} = 1 \\
f(n) &= n \\
T(n) &= \Theta(n \log{n})
\end{align*}

** 2.5(d)

\[T(n) = 9T(n/3) + n^2\]

\begin{align*}
a &= 9 \\
b &= 3 \\
k &= \log_3{9} = 2 \\
f(n) &= n^2 \\
T(n) &= \Theta(n^2 \log{n})
\end{align*}

** 2.5(e)

\[T(n) = 8T(n/2) + n^3\]

\begin{align*}
a &= 8 \\
b &= 2 \\
k &= \log_2{8} = 3 \\
f(n) &= n^3 \\
T(n) &= \Theta(n^3 \log{n})
\end{align*}

** 2.5(f)

\[T(n) = 49T(n/25) + n^{3/2} \log{n}\]

\begin{align*}
a &= 49 \\
b &= 25 \\
k &= \log_{25}{49} \\
f(n) &= n^{3/2} \log{n} \\
T(n) &= \Theta(n^{3/2} \log{n})
\end{align*}

** 2.5(g)

\[T(n) = T(n - 1) + 2\]

\[T(n) = \Theta(n)\]

** 2.5(h)

\[T(n) = T(n - 1) + n^c \implies T(n) = \Theta(n^{c + 1})\]

** 2.5(i)

\[T(n) = T(n - 1) + c^n \implies T(n) = \Theta(c^n)\]

** 2.5(j)

\[T(n) = 2T(n - 1) + 1 \implies T(n) = \Theta(2^n)\]

* Chapter 3
