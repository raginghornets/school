#+title: Notes
#+author: Eric Nguyen

* 2019-09-11

** Sample Paper #1

   Overall, a passing paper, in the C range.

*** General Notes

    - Just focus on the dang article at the beginning. No one cares about your Merriam-Webster Dictionary definition.
    - Once you state your thesis statement, just focus on that for the rest of the paper.
    - Why are none of your paragraphs related to the thesis statement?
    - Vary your wording up. Stop repeating the same phrases over and over again. e.g (don't write "According to" 10 times)
    - You'll need to refer to other sources than the article to have a strong argument.
    - If it's not cited in the text, it's not cited in the bibliography. 

*** Why this paper is passing

    - General intro sentences
    - Decent summary
    - Eventually goes into argument and does try to prove something
    - Unique argument

*** Things that could be improved

    - Intro too long and repetitive
    - Summary could've been more thorough yet shorter too
    - Not very connected to Orso's article
    - Some statistics

** Peer Review Worksheet

*** Reviewer's Name

*** Writer's Name

*** What is the writer's argument? Where can it be found in the paper (put a start next to it)? What would improve the writer's argument? Does it need to be stated more clearly? Should it be more specific?

*** What is the best piece of evidence that this paper offers to support the writer's argument? Why is it such good evidence?

*** What is the weakest aspect of this paper so far? Where should the writer spend their time when they're revising the paper over the next couple of days?

* 2019-09-16

** [[https://invidio.us/watch?v=BBJTeNTZtGU][PHILOSOPHY - Michel Foucault]]

** [[https://invidio.us/watch?v=AHRPzp09Kqc][Foucault 2: Government Surveillance & Prison | Philosophy Tube]]

   1. Pervasive Power
   2. Obscure Power
   3. Direct Violence Made Structural
   4. Structural Violence Made Profitable

*** Nothing to fear

    If you're under surveillance, it is opposed to your freedom.
    The Panopticon is "A subtle, calculated, technology of subjection."
    Life today is sort of like a Panopticon---everyone is under surveillance and it shapes how we act---making us lose our sense of freedom.
