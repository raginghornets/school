#+title: Argumentative Research Paper

* Requirements

  - Your paper must be 6-8 pages in length, not including your Works Cited page

  - You must use at least one of our required course texts and at least four additional research sources that you find on your own (5+ total)

  - You must cite all sources properly using MLA Style for in-text citation and Works Cited page

  - You must present your argument to the class, either in person or via Canvas, and this presentation must be prepared for a general audience, not an academic one (see directions below)

* Description

  Write a well-researched paper in which you *construct an argument of your own about the public sphere and social change*.
  Your argument should have a narrow focus that is developed through our readings and discussions in Unit III, but all topics related to Unit III are available to you.
  Possible topics might focus on some aspect of social media or on a specific platform that is controversial or about which you have something to say.
  You might also consider artistic expression on the internet:
  what artists are successfully using the internet to broadcast their art?
  What kind of art works well for the digital age?
  You may also wish to research the political uses of the digital public sphere:
  Who is using the internet for political purposes well, and who is using it poorly?
  What kind of activism happens online, and what is the impact of internet activism?
  How do hashtags and memes affect real people’s lives?
  We will spend time each class period brainstorming possible topics for you or your classmates to use.
  The audience for this paper is the same as for your previous papers (an academic audience), but when you present Please note that your audience for this paper is essentially the same as for the other assignments of the course (scholars such as you, your classmates, and your instructor).

  If you wish to carry over a topic from Paper #2 to Paper #3, you may do so as long as you are not repeating yourself or using the exact same sources.
  For example, a Paper #2 on Wikileaks might lead you to a Paper #3 about leaks of government information on Twitter for political purposes.
  They’re similar topics, but they’re not the same.

  Paper #3 draws together all elements of the course, including close reading and analysis of texts (Paper #1) and thorough analysis of the research about a topic (Paper #2).
  But in Paper #3, you will add a third element:
  a persuasive argument of your own that results from the research and thinking you have done.
  You should consult the list of research steps for Paper #2, as it is relevant here again, and you will want to remember that research is not something that is completed in a one-and-done fashion.
  Rather, research, writing, and thinking are all connected processes and it is likely that you will cycle through these steps multiple times as you work on this assignments.

  Writing an argumentative or persuasive paper is a very common task across all courses in all disciplines, and you will almost certainly find yourself writing argumentative or persuasive papers at some point in the next three years.
  You may also find yourself presenting arguments or persuasive speeches, power point presentations, or using other media for a general audience, which is why we also have included a non-academic presentation as an element of this assignment.

* The Required Non-Academic Presentation of Paper #3

  After you have completed a first draft of this paper, you will prepare a brief (3-5 minute) presentation of your argument and evidence to the class.
  Think of your presentation as distilling your paper into a format that could go viral or could be easily understood by someone on social media.
  This presentation will most likely be a short PowerPoint aimed at a non-academic audience, or it could take any one of the following creative forms:

  - A listicle or a Buzzfeed-style article

  - An infographic

  - A very short video or other artistic performance

  - A meme or set of memes

  - A poem

  - A series of photographs

  - A tweet thread

  If you have an idea for a creative form that doesn’t fit the above list, feel free to check in with me for approval.
  I am likely to say yes if you can convince me that your idea will work.
  The presentation is a required element of Paper #3, and I will include it in my grading of your final portfolio.
  The purpose of the presentation is to help us think about our work through multiple genres.
  It is becoming more common than ever to translate academic ideas for a wider audience through less traditional forms of writing, and we want you to leave English 802 having had some practice in doing so in the final portfolio.
