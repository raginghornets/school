#+TITLE: Final Review

* 5.3

** Problem 2

Let \(g(x) = \int_0^x f(t) \;dt\), where \(f\) is the function whose graph is shown.

*** Part A

Evaluate \(g(x)\) for \(x = 0, 1, 2, 3, 4, 5\), and 6.

\begin{align*}
g(0) &= 0 \\
g(1) &= \frac{1}{2} \\
g(2) &= 0 \\
g(3) &= -\frac{1}{2} \\
g(4) &= 0 \\
g(5) &= \frac{3}{2} \\
g(6) &= 4
\end{align*}

*** Part C

Where does \(g\) have a maximum value?
Where does it have a minimum value?

\(g\) has a maximum value at \(x = 1\) and a minimum value at \(x = 3\).

** Problem 3

Let \(g(x) = \int_0^x f(t) \;dt\), where \(f\) is the function whose graph is shown.

*** Part A

Evaluate \(g(0)\), \(g(1)\), \(g(2)\), \(g(3)\), and \(g(6)\).

\begin{align*}
g(0) &= 0 \\
g(1) &= g(0) + 2 = 2 \\
g(2) &= g(1) + 3 = 5 \\
g(3) &= g(2) + 2 = 7 \\
g(4) &= g(3) - \frac{1}{2} = \frac{13}{2} \\
g(5) &= g(4) - \frac{3}{2} = 5 \\
g(6) &= g(5) - 2 = 3
\end{align*}

*** Part B

On what interval is \(g\) increasing?

\[(0, 3)\]

*** Part C

Where does \(g\) have a maximum value?

\[x = 3\]

** Problem 7

\begin{align*}
g(x) &= \int_0^x \sqrt{t + t^3} \;dt \\
g'(x) &= \sqrt{x + x^3}
\end{align*}

** Problem 13

\begin{align*}
h(x) &= \int_1^{e^x} \ln{t} \;dt \\
h'(x) &= \frac{d}{dx} \left[\int_1^{e^x} \ln{t} \;dt\right] \\
&= \frac{d}{du} \left[\int_1^{u} \ln{t} \;dt\right] \cdot \frac{d}{du} \\
&= \ln{u} \cdot \frac{d}{du} \\
&= \ln{(e^x)} \cdot e^x \\
&= xe^x
\end{align*}

** Problem 14

\begin{align*}
h(x) &= \int_1^{\sqrt{x}} \frac{z^2}{z^4 + 1} \;dz \\
h'(x) &= \frac{d}{dx} \left[\int_1^{\sqrt{x}} \frac{z^2}{z^4 + 1} \;dz\right] \\
&= \frac{d}{du} \left[\int_1^{u} \frac{z^2}{z^4 + 1} \;dz\right] \cdot \frac{d}{du} \\
&= \frac{u^2}{u^4 + 1} \cdot \frac{d}{du} \\
&= \frac{x}{x^2 + 1} \cdot \frac{1}{2 \sqrt{x}} \\
&= \frac{\sqrt{x}}{2x^2 + 2}
\end{align*}

** Problem 18

\begin{align*}
y &= \int_{\sin{x}}^1 \sqrt{1 + t^2} \;dt \\
\frac{d}{dx} y &= \frac{d}{dx} \left[\int_{\sin{x}}^1 \sqrt{1 + t^2} \;dt\right] \\
&= \frac{d}{du} \left[\int_{u}^1 \sqrt{1 + t^2} \;dt\right] \cdot \frac{d}{du} \\
&= \frac{d}{du} \left[-\int_1^u \sqrt{1 + t^2} \;dt\right] \cdot \frac{d}{du} \\
&= -\sqrt{1 + u^2} \cdot \frac{d}{du} \\
&= -\sqrt{1 + \sin^2{(x)}} \cdot \cos{(x)}
\end{align*}

** Problem 39

\begin{align*}
\int_{1/\sqrt{3}}^{\sqrt{3}} \frac{8}{1 + x^2} \;dx &= 8 \int_{1/\sqrt{3}}^{\sqrt{3}} \frac{1}{1 + x^2} \\
&= \left.8 \tan^{-1}(x)\right]_{1/\sqrt{3}}^{\sqrt{3}} \\\
&= 8 \left[\tan^{-1}(\sqrt{3}) - \tan^{-1}\left(\frac{1}{\sqrt{3}}\right)\right] \\\
&= 8 \left[\frac{\pi}{3} - \frac{\pi}{6}\right] \\\
&= 8 \left[\frac{2\pi}{6} - \frac{\pi}{6}\right] \\\
&= 8 \left[\frac{\pi}{6}\right] \\
&= \frac{4\pi}{3}
\end{align*}

** Problem 42

\begin{align*}
\int_{1/2}^{1/\sqrt{2}} \frac{4}{\sqrt{1 -  x^2}} \;dx &= \left.4 \sin^{-1}{x}\right]_{1/2}^{1/\sqrt{2}} \\
&= 4 \left[\sin^{-1}{\left(\frac{1}{\sqrt{2}}\right)} - \sin^{-1}{\left(\frac{1}{2}\right)}\right] \\
&= 4 \left[\frac{\pi}{4} - \frac{\pi}{6}\right] \\
&= 4 \left[\frac{3\pi}{12} - \frac{2\pi}{12}\right] \\
&= 4 \left[\frac{\pi}{12}\right] \\
&= \frac{\pi}{3}
\end{align*}

** Problem 61

\begin{align*}
\frac{d}{dx} \left[\int_x^{x^2} e^{t^2} \;dt\right] &= \frac{d}{dx} \left[\int_x^0 e^{t^2} \;dt + \int_0^{x^2} e^{t^2} \;dt\right] \\
&= \frac{d}{dx} \left[-\int_0^x e^{t^2} \;dt + \int_0^{x^2} e^{t^2} \;dt\right] \\
&= -e^{x^2} + e^{{(x^2)}^2} \cdot \frac{d}{dx} (x^2) \\
\implies \frac{d}{dx} \left[\int_x^{x^2} e^{t^2} \;dt\right] &= -e^{x^2} + 2x e^{x^4}
\end{align*}

* 5.5

** Problem 17

\begin{align*}
\int \frac{e^u}{(1 - e^u)^2} \;du &= -\int \frac{1}{v^2} \;dv \\
&= \frac{1}{v} + C \\
&= \frac{1}{1 - e^u} + C
\end{align*}

** Problem 30

\begin{align*}
\int \frac{\sec^2{x}}{\tan^2{x}} \;dx &= \int \frac{1}{u^2} \;du \\
&= -\frac{1}{u} + C \\
&= -\frac{1}{\tan{x}} + C
\end{align*}

** Problem 31

\begin{align*}
\int \frac{(\arctan{x})^2}{x^2 + 1} \;dx &= \int u^2 \;du \\
&= \frac{u^3}{3} + C \\
&= \frac{(\arctan{x})^3}{3} + C
\end{align*}

** Problem 39

\begin{align*}
\int \frac{\sin{2x}}{1 + \cos^2{x}} \;dx &= -\int \frac{1}{u} \;du \\
&= -\ln{(1 + \cos^2{x})} + C
\end{align*}

** Problem 43

\begin{align*}
\int \frac{dx}{\sqrt{1 - x^2} \sin^{-1}{x}} &= \int \frac{du}{u} \\
&= \ln{|u|} + C \\
&= \ln{|\sin^{-1}{x}|} + C
\end{align*}

** Problem 45

\begin{align*}
\int \frac{1 + x}{1 + x^2} \;dx &= \int \frac{1 + \frac{1}{2} du}{u} \\
&= \int \frac{1}{1 + x^2} \;dx + \frac{1}{2} \int \frac{1}{u} \;du \\
&= \tan^{-1}{x} + \frac{1}{2} \ln{(1 + x^2)} + C
\end{align*}

** Problem 69

\begin{align*}
\int_e^{e^4} \frac{dx}{x \sqrt{\ln{x}}} &= \int_e^{e^4} \frac{du}{\sqrt{u}} \\
&= 2 \left.\sqrt{u}\right]_e^{e^4} \\
&= 2 \left.\sqrt{\ln{x}}\right]_e^{e^4} \\
&= 2 (\sqrt{\ln{e^4}} - \sqrt{\ln{e}}) \\
&= 2 (\sqrt{4} - \sqrt{1}) \\
&= 2 (2 - 1) \\
&= 2
\end{align*}

* 6.1

** Problem 1

Find the area of the shaded region.

\begin{align*}
A &= \int_1^8 \sqrt[3]{x} - \frac{1}{x} \;dx \\
&= \left.\frac{3}{4} x^{4/3} - \ln{|x|}\right]_1^8 \\
&= \left(\frac{3}{4} 8^{4/3} - \ln{|8|}\right) - \left(\frac{3}{4} 1^{4/3} - \ln{|1|}\right) \\
&= \frac{3}{4} 2^4 - \ln{(8)} - \frac{3}{4} \\
&= \frac{48}{4} - \ln{(8)} - \frac{3}{4} \\
&= \frac{45}{4} - \ln{(8)}
\end{align*}

** Problem 2

Find the area of the shaded region.

\begin{align*}
A &= \int_0^1 e^x - xe^{x^2} \;dx \\
&= \int_0^1 e^x \;dx - \int_0^1 xe^{x^2} \;dx \\
&= \left.e^x\right]_0^1 - \frac{1}{2} \int_0^1 e^u \;du \\
&= (e - 1) - \frac{1}{2} \left.e^u\right]_0^1 \\
&= e - 1 - \frac{1}{2} \left.e^{x^2}\right]_0^1 \\
&= e - 1 - \frac{1}{2} (e - 1) \\
&= e - 1 - \frac{1}{2} e + \frac{1}{2} \\
&= \frac{1}{2} e - \frac{1}{2}
\end{align*}

** Problem 15

Sketch the region enclosed by the given curves and find its area.

\[y = \sec^2{x}, \; y = 8 \cos{x}, \; -\pi / 3 \leq x \leq \pi / 3\]

\begin{align*}
\sec^{2}(0) &= \frac{1}{\cos^2{(0)}} = 1 < 8 \cos{(0)} = 8 \\
A &= \int_{-\pi / 3}^{\pi / 3} 8 \cos{x} - \sec^2{x} \;dx \\
&= \left.8 \sin{x} - \tan{x}\right]_{-\pi / 3}^{\pi / 3} \\
&= (8 \sin{\pi / 3} - \tan{\pi / 3}) - (8 \sin{-\pi / 3} - \tan{-\pi / 3}) \\
&= (8 (\sqrt{3}/2) - \sqrt{3}) - (8 (-\sqrt{3}/2) - (-\sqrt{3})) \\
&= (4 \sqrt{3} - \sqrt{3}) - (-4 \sqrt{3} + \sqrt{3}) \\
&= 4 \sqrt{3} - \sqrt{3} + 4 \sqrt{3} - \sqrt{3}) \\
&= 6 \sqrt{3}
\end{align*}

** Problem 24

Sketch the region enclosed by the given curves and find its area.

\[y = \cos{x}, \; y = 1 - \cos{x}, \; 0 \leq x \leq \pi\]

\begin{align*}
\cos{x} &= 1 - \cos{x} \\
2 \cos{x} &= 1 \\
\cos{x} &= \frac{1}{2} \\
x &= \frac{\pi}{3} \\
A &= \int_0^{\pi/3} \cos{x} - (1 - \cos{x}) \;dx + \int_{\pi/3}^{\pi} (1 - \cos{x}) - \cos{x} \;dx \\
&= \int_0^{\pi/3} 2\cos{x} - 1 \;dx + \int_{\pi/3}^{\pi} 1 - 2\cos{x} \;dx \\
&= \left.2\sin{x} - x\right]_0^{\pi/3} + \left.x - 2\sin{x}\right]_{\pi/3}^{\pi} \\
&= 2\sin{\left(\frac{\pi}{3}\right)} - \frac{\pi}{3} + \left[\pi - 2 \sin{\pi} - \left(\frac{\pi}{3} - 2\sin{\frac{\pi}{3}}\right)\right] \\
&= \sqrt{3} - \frac{\pi}{3} + \left(\pi - \frac{\pi}{3} + \sqrt{3}\right) \\
&= 2 \sqrt{3} + \frac{\pi}{3}
\end{align*}

* 6.2

** Problem 2

Find the volume of the solid obtained by rotating the region bounded by the given curves about the specified line.
Sketch the region, the solid, and a typical disk or washer.

\[y = 1/x, \; y = 0, \; x = 1, \; x = 4; \; \text{about the } x\text{-axis}\]

\begin{align*}
A(x) &= \pi r^2 \\
&= \pi \left(\frac{1}{x}\right)^2 \\
&= \frac{\pi}{x^2} \\
V &= \int_1^4 A(x) \;dx \\
&= \int_1^4 \frac{\pi}{x^2} \;dx \\
&= \left.-\frac{\pi}{x}\right]_1^4 \\
&= -\frac{\pi}{4} + \pi \\
&= \frac{-\pi + 4\pi}{4} \\
&= \frac{3\pi}{4}
\end{align*}

** Problem 3

\[y = \sqrt{x - 1}, \; y = 0, \; x = 5; \; \text{about the } x\text{-axis}\]

\begin{align*}
A(x) &= \pi r^2 \\
&= \pi (x - 1) \\
V &= \int_1^5 A(x) \;dx \\
&= \pi \int_1^5 (x - 1) \;dx \\
&= \pi \left.\frac{x^2}{2} - x\right]_1^5 \\
&= \pi \left[\frac{5^2}{2} - 5 - \frac{1}{2} + 1\right] \\
&= \pi \left[\frac{25}{2} - 4 - \frac{1}{2}\right] \\
&= \pi \left[\frac{24}{2} - \frac{8}{2}\right] \\
&= 8\pi
\end{align*}

** Problem 7

\[y = x^3, \; y = x, \; x \geq 0; \; \text{about the } x\text{-axis}\]

\begin{align*}
A(x) &= \pi r^2 \\
&= \pi (x - x^3)^2 \\
&= \pi (x^2 - x^6) \\
x^3 &= x \\
x^3 - x &= 0 \\
x(x^2 - 1) &= 0 \implies x = 0, \; x = 1
\end{align*}

\begin{align*}
V &= \int_0^1 A(x) \;dx \\
&= \pi \int_0^1 (x^2 - x^6) \;dx \\
&= \pi \left.\frac{x^3}{3} - \frac{x^7}{7}\right]_0^1 \\
&= \pi \left[\frac{1}{3} - \frac{1}{7}\right] \\
&= \pi \left[\frac{7}{21} - \frac{3}{21}\right] \\
&= \frac{4\pi}{21}
\end{align*}

* 7.1

** Problem 5

\[\int te^{-3t} \;dt\]

\begin{align*}
u &= t \\
dv &= e^{-3t} \;dt \\
du &= dt \\
v &= -\frac{1}{3} e^{-3t}
\end{align*}

\begin{align*}
\int te^{-3t} \;dt &= uv - \int v \;du \\
&= -\frac{1}{3} te^{-3t} + \frac{1}{3} \int e^{-3t} \;dt \\
&= -\frac{1}{3} te^{-3t} - \frac{1}{9} e^{-3t} + C
\end{align*}

** Problem 11

\[\int t^4 \ln{t} \;dt\]

\begin{align*}
u &= \ln{t} \\
du &= \frac{1}{t} \;dt \\
dv &= t^4 \;dt \\
v &= \frac{t^5}{5}
\end{align*}

\begin{align*}
\int t^4 \ln{t} \;dt &= uv - \int v \;du \\
&= \frac{1}{5} t^5 \ln{t} - \frac{1}{5} \int t^5 \cdot \frac{1}{t} \;dt \\
&= \frac{1}{5} t^5 \ln{t} - \frac{1}{25} t^5 + C
\end{align*}

** Problem 23

\[\int_0^{1/2} x \cos{\pi x} \;dx\]

\begin{align*}
u &= x \\
du &= dx \\
dv &= \cos{\pi x} \\
v &= \frac{1}{\pi} \sin{\pi x}
\end{align*}

\begin{align*}
\int_0^{1/2} x \cos{\pi x} \;dx &= uv - \int v \;du \\
&= \left.\frac{x}{\pi} \sin{\pi x}\right]_0^{1/2} - \frac{1}{\pi^2} \int_0^{1/2} \sin{\pi x} \;dx \\
&= \frac{\frac{1}{2}}{\pi} \sin{\left(\frac{\pi}{2}\right)} - \frac{1}{\pi^2} \left[\sin{\left(\frac{\pi}{2}\right)} - \sin{(0)}\right] \\
&= \frac{1}{2\pi} - \frac{1}{\pi^2} \\
&= \frac{\pi - 2}{2\pi^2}
\end{align*}

** Problem 27

\[\int_1^5 \frac{\ln{R}}{R^2} \;dR\]

\begin{align*}
u &= \ln{R} \\
du &= \frac{1}{R} \;dR \\
dv &= \frac{1}{R^2} \\
v &= -\frac{1}{R}
\end{align*}

\begin{align*}
\int_1^5 \frac{\ln{R}}{R^2} \;dR &= uv - \int v \;du \\
&= \left.\ln{R} \cdot -\frac{1}{R}\right]_1^5 + \int_1^5 \frac{1}{R} \cdot \frac{1}{R} \;dR \\
&= -\left.\frac{\ln{R}}{R}\right]_1^5 - \left.\frac{1}{R}\right]_1^5 \\
&= -\left(\frac{\ln{5}}{5} - \frac{\ln{1}}{1}\right) - \left(\frac{1}{5} - \frac{1}{1}\right) \\
&= -\frac{\ln{5}}{5} - \frac{1}{5} + 1 \\
&= \frac{4 - \ln{5}}{5}
\end{align*}

** Problem 57

\[y = x^2 \ln{x}, \quad y = 4 \ln{x}\]

\begin{align*}
x^2 \ln{x} &= 4 \ln{x} \\
x^2 \ln{x} - 4 \ln{x} &= 0 \\
\ln{x}\; (x^2 - 4) &= 0 \\
\implies x = 1&, \; x = 2
\end{align*}

\begin{align*}
A &= \int_1^2 (4 \ln{x} - x^2 \ln{x}) \;dx \\
&= \int_1^2 (4 - x^2) \ln{x} \;dx
\end{align*}

\begin{align*}
u &= \ln{x} \\
du &= \frac{1}{x} \;dx \\
dv &= 4 - x^2 \;dx \\
v &= 4x - \frac{x^3}{3}
\end{align*}

\begin{align*}
\int_1^2 (4 - x^2) \ln{x} \;dx &= uv - \int v \;du \\
&= \left.\ln{x} \left(4x - \frac{x^3}{3}\right)\right]_1^2 - \int_1^2 4 - \frac{x^2}{3} \;dx \\
&= \ln{2} \left(4(2) - \frac{2^3}{3}\right) - \left.4x - \frac{x^3}{9}\right]_1^2 \\
&= \ln{2} \left(8 - \frac{8}{3}\right) - \left[4(2) - \frac{2^3}{9} - 4(1) + \frac{1}{9}\right] \\
&= \ln{2} \left(\frac{24 - 8}{3}\right) - \left(8 - \frac{8}{9} - 4 + \frac{1}{9}\right) \\
&= \ln{2} \left(\frac{16}{3}\right) - \left(4 - \frac{7}{9}\right) \\
&= \frac{16}{3} \ln{2} - \frac{29}{9}
\end{align*}

** Problem 58

\[y = x^2 e^{-x}, \; y = xe^{-x}\]

\begin{align*}
x^2 e^{-x} &= xe^{-x} \\
x^2 e^{-x} - xe^{-x} &= 0 \\
xe^{-x} (x - 1) &= 0 \\
\implies x = 0&, \; x = 1
\end{align*}

\begin{align*}
A &= \int_0^1 (xe^{-x} - x^2 e^{-x}) \;dx \\
&= \int_0^1 xe^{-x} (1 - x) \;dx
\end{align*}

\begin{align*}
u &= 1 - x \\
du &= -dx \\
dv &= xe^{-x} \;dx \\
v &= \int xe^{-x} \;dx
\end{align*}

\begin{align*}
u_1 &= x \\
du_1 &= dx \\
dv_1 &= e^{-x} \;dx \\
v_1 &= -e^{-x} \\
\implies v &= u_1 v_1 - \int v_1 \;du_1 \\
&= -xe^{-x} + \int e^{-x} \;dx \\
&= -xe^{-x} - e^{-x}
\end{align*}

\begin{align*}
\int_0^1 xe^{-x} (1 - x) \;dx &= uv - \int v \;du \\
&= \left.(1 - x) (-xe^{-x} - e^{-x})\right]_0^1 - \int_0^1 xe^{-x} + e^{-x} \;dx \\
&= -e^{0} - \left[-xe^{-x} - e^{-x} - e^{-x}\right]_0^1 \\
&= -e^{0} - \left[\left(-e^{-1} - e^{-1} - e^{-1}\right) - \left(-e^{0} - e^{0}\right)\right] \\
&= -1 - \left[-3e^{-1} - 2\right] \\
&= \frac{3}{e} + 1
\end{align*}

** Problem 65

Calculate the volume generated by rotating the region bounded by the curves \(y = \ln{x}\), \(y = 0\), and \(x = 2\) about each axis.

*** WRONG Part A

The \(y\)-axis

\[x = e^y\]

\begin{align*}
e^y &= 2 \\
y &= \ln{2}
\end{align*}

\begin{align*}
A(y) &= \pi (2 - e^y)^2 \\
V &= \pi \int_0^{\ln{2}} (2 - e^y)^2 \;dy
\end{align*}

\begin{align*}
u &= (2 - e^y)^2 \\
du &= -2e^y(2 - e^y) \;dx \\
dv &= dy \\
v &= y
\end{align*}

\begin{align*}
\int_0^{\ln{2}} (2 - e^y)^2 \;dy &= uv - \int v \;du \\
&= \left.y(2 - e^y)^2\right]_0^{\ln{2}} + 2 \int_0^{\ln{2}} y e^y (2 - e^y) \;dx
\end{align*}

\begin{align*}
u_1 &= ye^y \\
du_1 &= e^y \;dy \\
dv_1 &= (2 - e^y) \;dx \\
v_1 &= 2y - e^y
\end{align*}

\begin{align*}
\int_0^{\ln{2}} ye^y (2 - e^y) \;dx &= u_1 v_1 - \int v_1 \;du_1 \\
&= \left.ye^y (2y - e^y)\right]_0^{\ln{2}} - \int_0^{\ln{2}} (2y - e^y) e^y \;dy
\end{align*}

\begin{align*}
u_2 &= 2y - e^y \\
du_2 &= 2 - e^y \;dy \\
dv_2 &= e^y \;dy \\
v_2 &= e^y
\end{align*}

\begin{align*}
\int_0^{\ln{2}} (2y - e^y) e^y \;dy &= u_2 v_2 - \int v_2 \;du_2 \\
&= \left.(2y - e^y) e^y\right]_0^{\ln{2}} - \int_0^{\ln{2}} e^y (2 - e^y) \;dy \\
&= 2 \ln{2} - 2 + 1 - \int_0^{\ln{2}} w \;dw \\
&= 2 \ln{2} - 1 - \left.w\right]_0^{\ln{2}} \\
&= 2 \ln{2} - 1 - \left.2 - e^y\right]_0^{\ln{2}} \\
&= 2 \ln{2} - 1 - \left[2 - 2 - 2 + 1\right] \\
&= 2 \ln{2}
\end{align*}

\begin{align*}
\int_0^{\ln{2}} ye^y (2 - e^y) \;dx &= 2 \ln{(2)} (2 \ln{(2)} - 2) - 2 \ln{(2)} \\
&= 2 \ln{(2)} [(2 \ln{(2)} - 2) - 1] \\
&= 2 \ln{(2)} [2 \ln{(2)} - 3]
\end{align*}

\begin{align*}
\int_0^{\ln{2}} (2 - e^y)^2 \;dy &= 4 \ln{(2)} [2 \ln{(2)} - 3] \\
V &= 4 \pi \ln{(2)} [2 \ln{(2)} - 3]
\end{align*}

*** Part B

The \(x\)-axis

\begin{align*}
A(x) &= \pi (\ln{x})^2 \\
V &= \pi \int_1^2 (\ln{x})^2 \;dx
\end{align*}

\begin{align*}
u &= (\ln{x})^2 \\
du &= \frac{2 \ln{x}}{x} \;dx \\
dv &= dx \\
v &= x
\end{align*}

\begin{align*}
\int_1^2 (\ln{x})^2 \;dx &= uv - \int v \;du \\
&= \left.x (\ln{x})^2\right]_1^2 - 2 \int_1^2 \ln{x} \;dx \\
&= 2 (\ln{2})^2 - 2 \left.(x \ln{(x)} - x)\right]_1^2 \\
&= 2 (\ln{2})^2 - 2 \left[2 \ln{(2)} - 2 + 1\right] \\
&= 2 (\ln{2})^2 - 4 \ln{(2)} + 2
\end{align*}

\begin{align*}
V &= 2\pi [(\ln{2})^2 - 2 \ln{2} + 1]
\end{align*}

* 7.2

** Problem 1

\begin{align*}
\int \sin^2{x} \cos^3{x} \;dx &= \int \sin^2{x} (1 - \sin^2{x}) \cos{x} \;dx \\
&= \int u^2 (1 - u^2) \;du \\
&= \int (u^2 - u^4) \;du \\
&= \frac{u^3}{3} - \frac{u^5}{5} + C \\
&= \frac{\sin^3{x}}{3} - \frac{\sin^5{x}}{5} + C
\end{align*}

** Problem 4

\begin{align*}
\int_0^{\pi/2} \sin^7{\theta} \cos^5{\theta} \;d\theta &= \int_0^{\pi/2} \sin^7{\theta} \cos^4{\theta} \cos{\theta} \;d\theta \\
&= \int_0^{\pi/2} \sin^7{\theta} (1 - \sin^2{\theta})^2 \cos{\theta} \;d\theta \\
&= \int_0^{\pi/2} u^7 (1 - u^2)^2 \;du \\
&= \int_0^{\pi/2} u^7 (1 - 2u^2 + u^4) \;du \\
&= \int_0^{\pi/2} (u^7 - 2u^9 + u^{11}) \;du \\
&= \left.\frac{u^8}{8} - \frac{2u^{10}}{10} + \frac{u^{12}}{12}\right]_0^{\pi/2} \\
&= \left.\frac{\sin^8{x}}{8} - \frac{2\sin^{10}{x}}{10} + \frac{\sin^{12}{x}}{12}\right]_0^{\pi/2} \\
&= \frac{\sin^8{\frac{\pi}{2}}}{8} - \frac{2\sin^{10}{\frac{\pi}{2}}}{10} + \frac{\sin^{12}{\frac{\pi}{2}}}{12} \\
&= \frac{1}{8} - \frac{2}{10} + \frac{1}{12} \\
&= \frac{1}{120}
\end{align*}

** Problem 7

\begin{align*}
\int_0^{\pi/2} \cos^2{\theta} \;d\theta &= \frac{1}{2} \int_0^{\pi/2} (1 - \cos{2\theta}) \;d\theta \\
&= \frac{1}{2} \left[\int_0^{\pi/2} d\theta - \int_0^{\pi/2} \cos{2\theta} \;d\theta\right] \\
&= \frac{1}{2} \left[\left.\theta\right]_0^{\pi/2} - \frac{1}{2} \int_0^{\pi/2} \cos{u} \;du\right] \\
&= \frac{1}{2} \left[\frac{\pi}{2} - \frac{1}{2} \left.\sin{u}\right]_0^{\pi/2}\right] \\
&= \frac{1}{2} \left[\frac{\pi}{2} - \frac{1}{2} \left.\sin{2\theta}\right]_0^{\pi/2}\right] \\
&= \frac{1}{2} \left(\frac{\pi}{2}\right) \\
&= \frac{\pi}{4}
\end{align*}

** Problem 26

\begin{align*}
\int_0^{\pi/4} \sec^6{\theta} \tan^6{\theta} \;d\theta &= \int_0^{\pi/4} \sec^4{\theta} \tan^6{\theta} \sec^2{\theta} \;d\theta \\
&= \int_0^{\pi/4} (1 + \tan^2{\theta})^2 \tan^6{\theta} \sec^2{\theta} \;d\theta \\
&= \int_0^{\pi/4} (1 + u^2)^2 u^6 \;du \\
&= \int_0^{\pi/4} (1 + 2u^2 + u^4) u^6 \;du \\
&= \int_0^{\pi/4} (u^6 + 2u^8 + u^{10}) \;du \\
&= \left.\frac{u^7}{7} + \frac{2u^9}{9} + \frac{u^{11}}{11}\right]_0^{\pi/4} \\
&= \left.\frac{\tan^7{x}}{7} + \frac{2\tan^9{x}}{9} + \frac{\tan^{11}{x}}{11}\right]_0^{\pi/4} \\
&= \frac{\tan^7{\frac{\pi}{4}}}{7} + \frac{2\tan^9{\frac{\pi}{4}}}{9} + \frac{\tan^{11}{\frac{\pi}{4}}}{11} \\
&= \frac{1}{7} + \frac{2}{9} + \frac{1}{11} \\
&= \frac{316}{693}
\end{align*}

** Problem 57

\[y = \sin^2{x}, \; y = \sin^3{x}, \; 0 \leq x \leq \pi\]

\begin{align*}
\int_0^{\pi} \sin^2{x} - \sin^3{x} \;dx &= \int_0^{\pi} \sin^2{x} \;dx - \int_0^{\pi} \sin^3{x} \;dx \\
&= \frac{1}{2} \int_0^{\pi} (1 - \cos{2x}) \;dx - \int_0^{\pi} (1 - \cos^2{x}) \sin{x} \;dx \\
&= \frac{1}{2} \left[\int_0^{\pi} dx - \int_0^{\pi} \cos{2x} \;dx\right] + \int_0^{\pi} (1 - u^2) \;du \\
&= \frac{1}{2} \left[\pi - \frac{1}{2} \int_0^{\pi} \cos{u} \;du\right] + \left.u - \frac{u^3}{3}\right]_0^{\pi} \\
&= \frac{1}{2} \left[\pi - \frac{1}{2} \left.\sin{u}\right]_0^{\pi}\right] + \left.\cos{x} - \frac{\cos^3{x}}{3}\right]_0^{\pi} \\
&= \frac{1}{2} \left[\pi - \frac{1}{2} \left.\sin{2x}\right]_0^{\pi}\right] + \left[\cos{\pi} - \frac{\cos^3{\pi}}{3} - \cos{(0)} + \frac{\cos^3{0}}{3}\right] \\
&= \frac{1}{2} \left(\pi\right) + \left(-1 + \frac{1}{3} - 1 + \frac{1}{3}\right) \\
&= \frac{\pi}{2} + \left(-2 + \frac{2}{3}\right) \\
&= \frac{\pi}{2} - 2 + \frac{2}{3} \\
&= \frac{\pi}{2} - \frac{4}{3}
\end{align*}

** Problem 58

\[y = \tan{x}, \; y = \tan^2{x}, \; 0 \leq x \leq \pi / 4\]

\begin{align*}
\int \tan^2{x} - \tan{x} \;dx &= \int \tan^2{x} \;dx - \int \tan{x} \;dx \\
&= \int (\sec^2{x} - 1) \;dx - \int \frac{\sec{x} \tan{x}}{\sec{x}} \;dx \\
&= \tan{x} - x - \ln{|\sec{x}|} + C
\end{align*}

** Problem 61

\[y = \sin{x}, \; y = 0, \; \pi/2 \leq x \leq \pi; \; \text{about the } x\text{-axis}\]

\begin{align*}
A(x) &= \pi \sin^2{x} \\
V &= \pi \int_{\pi/2}^{\pi} \sin^2{x} \;dx \\
&= \frac{\pi}{2} \int_{\pi/2}^{\pi} (1 - \cos{2x}) \;dx \\
&= \frac{\pi}{2} \left[\int_{\pi/2}^{\pi} dx - \int_{\pi/2}^{\pi} \cos{2x}) \;dx\right] \\
&= \frac{\pi}{2} \left[\frac{\pi}{2} - \frac{1}{2} \int_{\pi/2}^{\pi} \cos{u} \;du\right] \\
&= \frac{\pi}{2} \left[\frac{\pi}{2} - \frac{1}{2} \left.\sin{2x}\right]_{\pi/2}^{\pi}\right] \\
&= \frac{\pi}{2} \left(\frac{\pi}{2}\right) \\
&= \frac{\pi^2}{4}
\end{align*}

* 7.4

** Problem 9

\[\int \frac{5x + 1}{(2x + 1)(x - 1)} \;dx\]

\begin{align*}
\frac{5x + 1}{(2x + 1)(x - 1)} &= \frac{A}{2x + 1} + \frac{B}{x - 1} \\
5x + 1 &= A(x - 1) + B(2x + 1) \\
5x + 1 &= Ax - A + 2Bx + B \\
5x + 1 &= (A + 2B)x + (-A + B)
\end{align*}

\begin{align*}
A + 2B &= 5 \\
A &= 5 - 2B \\
-A + B &= 1 \\
-5 + 2B + B &= 1 \\
3B &= 6 \\
B &= 2
\end{align*}

\begin{align*}
-A + 2 &= 1 \\
A &= 1
\end{align*}

\begin{align*}
\int \frac{5x + 1}{(2x + 1)(x - 1)} \;dx &= \int \frac{1}{2x + 1} + \frac{2}{x - 1} \;dx \\
&= \frac{1}{2} \ln{|2x + 1|} + 2 \ln{|x - 1|} + C
\end{align*}

** Problem 19

\[\int_0^1 \frac{x^2 + x + 1}{(x + 1)^2 (x + 2)} \;dx\]

\begin{align*}
\frac{x^2 + x + 1}{(x + 1)^2 (x + 2)} &= \frac{A}{x + 1} + \frac{B}{(x + 1)^2} + \frac{C}{x + 2} \\
x^2 + x + 1 &= A(x + 1)(x + 2) + B(x + 2) + C(x + 1)^2 \\
x^2 + x + 1 &= A(x^2 + 3x + 2) + Bx + 2B + C(x^2 + 2x + 1) \\
x^2 + x + 1 &= Ax^2 + 3Ax + 2A + Bx + 2B + Cx^2 + 2Cx + C \\
x^2 + x + 1 &= (A + C)x^2 + (3A + B + 2C)x + (2A + 2B + C)
\end{align*}

\begin{align*}
A + C &= 1 \\
3A + B + 2C &= 1 \\
2A + 2B + C &= 1
\end{align*}

\begin{align*}
A &= 1 - C \\
3(1 - C) + B + 2C &= 1 \\
3 - 3C + 2C &= 1 - B \\
C &= 2 + B \\
2(1 - C) + 2B + (2 + B) &= 1 \\
2 - 2C + 3B &= -1 \\
-2(2 + B) + 3B &= -3 \\
-4 + -2B + 3B &= -3 \\
B &= 1
\end{align*}

\begin{align*}
C &= 2 + 1 = 3 \\
A &= 1 - 3 = -2
\end{align*}

\begin{align*}
\int_0^1 \frac{x^2 + x + 1}{(x + 1)^2 (x + 2)} \;dx &= \int_0^1 \frac{-2}{x + 1} + \frac{1}{(x + 1)^2} + \frac{3}{x + 2} \;dx \\
&= \left.-2 \ln{|x + 1|} - \frac{1}{x + 1} + 3 \ln{|x + 2|}\right]_0^1 \\
&= \left(-2 \ln{|1 + 1|} - \frac{1}{1 + 1} + 3 \ln{|1 + 2|}\right) - \left(-2 \ln{|0 + 1|} - \frac{1}{0 + 1} + 3 \ln{|0 + 2|}\right) \\
&= -2 \ln{(2)} - \frac{1}{2} + 3 \ln{(3)} - 1 - 3 \ln{(2)} \\
&= \frac{1}{2} - 5 \ln{(2)} + 3 \ln{(3)}
\end{align*}

** Problem 23

\[\int \frac{10}{(x - 1)(x^2 + 9)} \;dx\]

\begin{align*}
\frac{10}{(x - 1)(x^2 + 9)} &= \frac{A}{x - 1} + \frac{Bx + C}{x^2 + 9} \\
10 &= A(x^2 + 9) + (Bx + C)(x - 1) \\
10 &= Ax^2 + 9A + Bx^2 - Bx + Cx - C \\
10 &= (A + B)x^2 + (-B + C)x + (9A - C)
\end{align*}

\begin{align*}
A + B &= 0 \\
-B + C &= 0 \\
9A - C &= 10
\end{align*}

\begin{align*}
B &= -A \\
C &= -A \\
9A + A &= 10 \\
A &= 1 \\
B &= -1 \\
C &= -1
\end{align*}

\begin{align*}
\int \frac{10}{(x - 1)(x^2 + 9)} \;dx &= \int \frac{1}{x - 1} + \frac{-x - 1}{x^2 + 9} \;dx \\
&= \int \frac{1}{x - 1} + \frac{-x - 1}{x^2 + 9} \;dx \\
&= \ln{|x - 1|} + \left[-\int \frac{x}{x^2 + 9} \;dx - \int \frac{1}{x^2 + 9} \;dx\right] + C \\
&= \ln{|x - 1|} + \left[-\frac{1}{2} \ln{(x^2 + 9)} - \frac{1}{9} \tan^{-1}{\left(\frac{x}{3}\right)}\right] + C \\
&= \ln{|x - 1|} -\frac{1}{2} \ln{(x^2 + 9)} - \frac{1}{3} \tan^{-1}{\left(\frac{x}{3}\right)} + C
\end{align*}

** Problem 64

\[\int_1^2 \frac{1}{x^3 + x} \;dx\]

\begin{align*}
\frac{1}{x^3 + x} &= \frac{1}{x(x^2 + 1)} \\
\frac{1}{x(x^2 + 1)} &= \frac{A}{x} + \frac{Bx + C}{x^2 + 1} \\
1 &= A(x^2 + 1) + (Bx + C)(x) \\
1 &= Ax^2 + A + Bx^2 + Cx \\
1 &= (A + B)x^2 + Cx + A
\end{align*}

\begin{align*}
A + B &= 0 \\
C &= 0 \\
A &= 1 \implies B = -1
\end{align*}

\begin{align*}
\int_1^2 \frac{1}{x^3 + x} \;dx &= \int \frac{1}{x} + \frac{-x}{x^2 + 1} \;dx \\
&= \left.\ln{|x|} - \frac{1}{2} \ln{(x^2 + 1)}\right]_1^2 \\
&= \left[\ln{(2)} - \frac{1}{2} \ln{(2^2 + 1)}\right] - \left[\ln{(1)} - \frac{1}{2} \ln{(1^2 + 1)}\right] \\
&= \ln{(2)} - \frac{1}{2} \ln{(5)} + \frac{1}{2} \ln{(2)} \\
&= \frac{3}{2} \ln{(2)} - \frac{1}{2} \ln{(5)} \\
&= \ln{(2)^{3/2}} - \ln{(5)^{1/2}} \\
&= \frac{1}{2} \ln{\frac{8}{5}}
\end{align*}

* 11.2

** Problem 22

\begin{align*}
\sum_{n = 1}^{\infty} \frac{5}{\pi^n} &= \frac{5}{\pi} \sum_{n = 1}^{\infty} \left(\frac{1}{\pi}\right)^n \\
\sum ar^{n-1} &\implies \frac{a}{1 - r} \\
a &= \frac{5}{\pi} \\
r &= \frac{5}{\pi^2} \cdot \frac{\pi}{5} \\
&= \frac{1}{\pi} \\
\sum a_n &= \frac{5}{\pi \left(1 - \frac{1}{\pi}\right)} \\
&= \frac{5}{\pi - 1}
\end{align*}

** Problem 23

\begin{align*}
\sum_{n = 1}^{\infty} \frac{(-3)^{n-1}}{4^n} &= \sum_{n = 1}^{\infty} \frac{1}{3} \cdot \frac{3}{4} \cdot \left(\frac{-3}{4}\right)^{n - 1} \\
a &= \frac{1}{4} \\
r &= -\frac{3}{4} \\
\sum a_n &= \frac{1}{4 \left(1 + \frac{3}{4}\right)} \\
&= \frac{1}{7}
\end{align*}

** Problem 29

\[\sum_{n=1}^{\infty} \frac{2+n}{1-2n}\]

\begin{align*}
\lim_{n \to \infty} a_n &= \lim_{n \to \infty} \frac{\frac{2}{n} + 1}{\frac{1}{n} - 2} \\
&= \frac{1}{2} \\
\lim_{n \to \infty} a_n \neq 0 &\implies \sum a_n \to (D)
\end{align*}

** Problem 33

\[\sum_{n=1}^{\infty} \frac{1}{4 + e^{-n}}\]

\begin{align*}
\lim_{n \to \infty} a_n &= \lim_{n \to \infty} \frac{1}{4 + \frac{1}{e^n}} \\
&= \frac{1}{4} \\
\lim_{n \to \infty} a_n \neq 0 &\implies \sum a_n \to (D)
\end{align*}

** Problem 39

\[\sum_{n=1}^{\infty} \arctan{n}\]

\begin{align*}
\lim_{n \to \infty} a_n \neq 0 &\implies \sum a_n \to (D)
\end{align*}

* 11.4

** Problem 7

\[\sum_{n=1}^{\infty} \frac{9^n}{3 + 10^n}\]

\begin{align*}
\frac{9^n}{3 + 10^n} &\leq \frac{9^n}{10^n} \\
|r| = \frac{9}{10} < 1 &\implies (C)
\end{align*}

** Problem 9

\[\sum_{k=1}^{\infty} \frac{\ln{k}}{k}\]

\begin{align*}
\ln{k} &= 1 \\
k &= e \\
\sum_{k=1}^{\infty} \frac{\ln{k}}{k} &= \sum_{k=1}^e \frac{\ln{k}}{k} + \sum_{k=e}^{\infty} \frac{\ln{k}}{k}
\end{align*}

\begin{align*}
\frac{\ln{k}}{k} &\geq \frac{1}{k} \; \forall k \geq e \\
\sum a_n > \sum \frac{1}{k} &\implies (D) \; \text{by harmonic series } (D)
\end{align*}

** Problem 10

\[\sum_{k=1}^{\infty} \frac{k \sin^2{k}}{1 + k^3}\]

\begin{align*}
\frac{k \sin^2{k}}{1 + k^3} &\leq \frac{k}{k^3} \\
&\leq \frac{1}{k^2} \\
p = 2 > 1 &\implies (C)
\end{align*}

** Problem 13

\[\sum_{n=1}^{\infty} \frac{1 + \cos{n}}{e^n}\]

\begin{align*}
\frac{1 + \cos{n}}{e^n} &\leq \frac{1}{e^n} \\
|r| = \frac{1}{e} < 1 &\implies (C)
\end{align*}

** Problem 15

\[\sum_{n=1}^{\infty} \frac{4^{n+1}}{3^n - 2}\]

\begin{align*}
\frac{4^{n+1}}{3^n - 2} &= 4 \cdot \frac{4^n}{3^n - 2} \\
\frac{4^n}{3^n - 2} &\geq \frac{4^n}{3^n} \\
|r| = \frac{4}{3} > 1 &\implies (D)
\end{align*}

** Problem 17

\[\sum_{n=1}^{\infty} \frac{1}{\sqrt{n^2 + 1}}\]

\begin{align*}
\frac{1}{\sqrt{n^2 + 1}} &\geq \frac{1}{\sqrt{n^2 + n^2}} \\
&\geq \frac{1}{\sqrt{2n^2}} \\
&\geq \frac{1}{n\sqrt{2}} \\
\sum a_n > \sum \frac{1}{n} &\implies (D) \; \text{by harmonic series } (D)
\end{align*}

** Problem 19

\[\sum_{n=1}^{\infty} \frac{n + 1}{n^3 + n}\]

\begin{align*}
\frac{n + 1}{n^3 + n} &\leq \frac{2n}{n^3} \\
&\leq \frac{2}{n^2} \\
p = 2 > 1 &\implies (C)
\end{align*}

** Problem 20

\[\sum_{n=1}^{\infty} \frac{n^2 + n + 1}{n^4 + n^2}\]

\begin{align*}
\frac{n^2 + n + 1}{n^4 + n^2} &\leq \frac{3n^2}{n^4} \\
&\leq \frac{3}{n^2} \\
p = 2 > 1 &\implies (C)
\end{align*}

** Problem 22

\[\sum_{n=3}^{\infty} \frac{n + 2}{(n + 1)^3}\]

\begin{align*}
\frac{n + 2}{(n + 1)^3} &\leq \frac{3n}{(n + 1)(n^2 + 2n + 1)} \\
&\leq \frac{3n}{n^3 + 2n^2 + n + n^2 + 2n + 1} \\
&\leq \frac{3n}{n^3 + 3n^2 + 3n + 1} \\
&\leq \frac{3n}{n^3} \\
&\leq 3 \cdot \frac{1}{n^2} \\
p > 1 &\implies \sum_{n=3}^{\infty} \frac{n + 2}{(n + 1)^3} \to (C)
\end{align*}

** Problem 23

\[\sum_{n=1}^{\infty} \frac{5 + 2n}{(1 + n^2)^2}\]

\begin{align*}
\frac{5 + 2n}{(1 + n^2)^2} &\leq \frac{5n + 2n}{(1 + n^2)^2} \\
&\leq \frac{7n}{1 + 2n^2 + n^4} \\
&\leq \frac{7n}{n^4} \\
&\leq 7 \cdot \frac{1}{n^3} \\
p > 1 &\implies \sum_{n=1}^{\infty} \frac{5 + 2n}{(1 + n^2)^2} \to (C)
\end{align*}

* 11.5

** Problem 7

\[\sum_{n=1}^{\infty} (-1)^n \frac{3n - 1}{2n + 1}\]

\begin{align*}
b_n &= \frac{3n - 1}{2n + 1} \\
b_{n + 1} &< b_n \\
\frac{3(n + 1) - 1}{2(n + 1) + 1} &< \frac{3n - 1}{2n + 1} \\
\frac{3n + 3 - 1}{2n + 2 + 1} &< \frac{3n - 1}{2n + 1} \\
\frac{(3n - 1) - 3}{(2n + 1) + 2} &< \frac{3n - 1}{2n + 1} \\
\end{align*}

\begin{align*}
\lim_{n \to \infty} \frac{3n - 1}{2n + 1} &= \lim_{n \to \infty} \frac{3 - \frac{1}{n}}{2 + \frac{1}{n}} \\
&= \frac{3}{2} \\
\lim_{n \to \infty} b_n \neq 0 &\implies (D) \text{ by Divergence Test}
\end{align*}

** Problem 14

\[\sum_{n=1}^{\infty} (-1)^{n-1} \arctan{n}\]

\begin{align*}
\arctan{(n + 1)} &> \arctan{(n)} \\
\lim_{n \to \infty} \arctan{n} = \frac{\pi}{2} \neq 0 &\implies (D)
\end{align*}

** Problem 17

\[\sum_{n=1}^{\infty} (-1)^n \sin{\left(\frac{\pi}{n}\right)}\]

\begin{align*}
\sin{\left(\frac{\pi}{n + 1}\right)} &< \sin{\left(\frac{\pi}{n}\right)} \\
\frac{\pi}{n + 1} &< \frac{\pi}{n}
\end{align*}

\begin{align*}
\lim_{n \to \infty} \sin{\left(\frac{\pi}{n}\right)} &= \sin{(0)} = 0
\end{align*}

\begin{align*}
\sum (-1)^n \sin{\left(\frac{\pi}{n}\right)} &\to (C) \text{ by the Alternating Series Test}
\end{align*}

** Problem 18

\[\sum_{n=1}^{\infty} (-1)^n \cos{\left(\frac{\pi}{n}\right)}\]

\begin{align*}
\cos{\left(\frac{\pi}{n + 1}\right)} &< \cos{\left(\frac{\pi}{n}\right)} \\
\frac{\pi}{n + 1} &< \frac{\pi}{n}
\end{align*}

\begin{align*}
\lim_{n \to \infty} \cos{\left(\frac{\pi}{n}\right)} &= \cos{(0)} \\
\cos{(0)} = 1 \neq 0 &\implies (D) \text{ by Divergence Test}
\end{align*}

* 11.6

** Problem 4

\begin{align*}
\sum_{n=1}^{\infty} \left|\frac{(-1)^n}{n^3 + 1}\right| &< \sum_{n=1}^{\infty} \frac{1}{n^3} \\
&\implies (AC)
\end{align*}

** Problem 5

\begin{align*}
\sum_{n=1}^{\infty} \left|\frac{\sin{n}}{2^n}\right| &< \sum_{n=1}^{\infty} \frac{1}{2^n} \\
|r| = \frac{1}{2} < 1 &\implies (AC)
\end{align*}

** Problem 7

\[\sum_{n=1}^{\infty} \frac{n}{5^n}\]

\begin{align*}
\lim_{n\to\infty} \left|\frac{a_{n+1}}{a_n}\right| &= \lim_{n\to\infty} \left|\frac{n+1}{5^{n+1}} \frac{5^n}{n}\right| \\
&= \lim_{n\to\infty} \left|\frac{n+1}{5n}\right| \\
&= \lim_{n\to\infty} \left|\frac{1+\frac{1}{n}}{5}\right| \\
&= \frac{1}{5} < 1 \implies (AC) \implies (C)
\end{align*}

** Problem 9
** Problem 10
** Problem 14
** Problem 19
** Problem 25

\[\sum_{n=1}^{\infty} \left(\frac{n^2+1}{2n^2 + 1}\right)^n\]

\begin{align*}
\lim_{n\to\infty} \left|\sqrt[n]{a_n}\right| &= \lim_{n\to\infty} \left|\frac{n^2 + 1}{2n^2 + 1}\right| \\
&= \lim_{n\to\infty} \left|\frac{1 + \frac{1}{n^2}}{2 + \frac{1}{n^2}}\right| \\
&= \frac{1}{2} < 1 \implies (AC) \implies (C)
\end{align*}

** Problem 26
** Problem 30
** Problem 32

\[\sum_{n=1}^{\infty} \left(\frac{1-n}{2+3n}\right)^n\]

\begin{align*}
\lim_{n\to\infty} \left|\sqrt[n]{a_n}\right| &= \lim_{n\to\infty} \left|\frac{1 - n}{2 + 3n}\right| \\
&= \lim_{n\to\infty} \left|\frac{\frac{1}{n} - 1}{\frac{2}{n} + 3}\right| \\
&= \frac{1}{3} < 1 \implies (AC) \implies (C)
\end{align*}

** Problem 36
** Problem 37

* 11.7

** Problem 5

\[\sum_{n=1}^{\infty} \frac{e^n}{n^2}\]

\begin{align*}
\lim_{n \to \infty} \frac{e^n}{n^2} \cdot \frac{1}{e^n} &= \lim_{n \to \infty} \frac{1}{n^2} \\
&= 0 \\
e^n \to (D) &\implies \frac{e^n}{n^2} \to (D)
\end{align*}

** Problem 6

\[\sum_{n=1}^{\infty} \frac{n^{2n}}{(1 + n)^{3n}}\]

\begin{align*}

\end{align*}

** Problem 14
** Problem 18

* 11.8

** Problem 7

\[\sum_{n=0}^{\infty} \frac{x^n}{n!}\]

\begin{align*}
\lim_{n \to \infty} \left|\frac{a_{n+1}}{a_n}\right| &= \lim_{n \to \infty} \left|\frac{x^{n+1}}{(n+1)!} \cdot \frac{n!}{x^n}\right| \\
&= \lim_{n \to \infty} \left|\frac{x^{n} x}{(n+1)n!} \cdot \frac{n!}{x^n}\right| \\
&= \lim_{n \to \infty} \left|\frac{x}{n+1}\right| \\
&= 0 \\
&\implies (C) \; \forall x \in \mathbb{R} \\
&\implies R = \infty, \; I = (-\infty, \infty)
\end{align*}

** Problem 11

\[\sum_{n=1}^{\infty} \frac{(-1)^n 4^n}{\sqrt{n}} x^n\]

\begin{align*}
\lim_{n\to\infty} \left|\frac{a^{n+1}}{a^n}\right| &= \lim_{n\to\infty} \left|\frac{(-1)^{n+1}4^{n+1}x^{n+1}}{\sqrt{n+1}} \cdot \frac{\sqrt{n}}{(-1)^n 4^n x^n}\right| \\
&= \lim_{n\to\infty} \left|\frac{-4x \sqrt{n}}{\sqrt{n+1}}\right| \\
&= 4x \lim_{n\to\infty} \frac{\sqrt{n}}{\sqrt{n+1}} \\
&= 4x \lim_{n\to\infty} \frac{\frac{1}{\sqrt{n}} \sqrt{n}}{\frac{1}{\sqrt{n}} \sqrt{n+1}} \\
&= 4x \lim_{n\to\infty} \frac{1}{\sqrt{1 + \frac{1}{n}}} \\
&= 4x \\
&\implies -1 < 4x < 1 \\
&\implies -\frac{1}{4} < x < \frac{1}{4} \\
&\implies R = \frac{1}{4}, \; I = \left(-\frac{1}{4}, \frac{1}{4}\right)
\end{align*}

** Problem 18

\[\sum_{n=1}^{\infty} \frac{\sqrt{n}}{8^n} (x + 6)^n\]

\begin{align*}
\lim_{n\to\infty} \left|\sqrt[n]{a_n}\right| &= \lim_{n\to\infty} \left|\frac{\sqrt[n]{\sqrt{n}}(x + 6)}{8}\right| \\
&= \frac{x + 6}{8} \lim_{n\to\infty} \left|\sqrt[n]{\sqrt{n}}\right| \\
&\implies -1 < \frac{x+6}{8} < 1 \\
&\implies -8 < x + 6 < 8 \\
&\implies -14 < x < 2 \\
&\implies R = 8, \; I = (-14, 2)
\end{align*}

** Problem 19
** Problem 20
** Problem 29
** Problem 30

* 11.9

** Problem 3

\[f(x) = \frac{1}{1 + x}\]

\begin{align*}
f(x) &= \frac{1}{1 - (-x)} \\
&= \sum_{n=0}^{\infty} (-x)^n \\
&= \sum_{n=0}^{\infty} (-1)^n x^n
\end{align*}

\begin{align*}
|-x| < 1 &\implies R = 1, \; I = (-1, 1)
\end{align*}

** Problem 4

\[f(x) = \frac{5}{1 - 4x^2}\]

\begin{align*}
f(x) &= 5 \cdot \frac{1}{1 - (4x^2)} \\
&= 5 \sum_{n=0}^{\infty} (4x^2)^n \\
&= 5 \sum_{n=0}^{\infty} 4^n x^{2n}
\end{align*}

\begin{align*}
|4x^2| < 1 &\implies x^2 < \frac{1}{4} \\
&\implies x < \frac{1}{2} \\
&\implies R = \frac{1}{2}, \; I = \left(-\frac{1}{2}, \frac{1}{2}\right)
\end{align*}

** Problem 16

\[f(x) = x^2 \tan^{-1}(x^3)}\]

\begin{align*}
f(x) &= x^2 \cdot \sum_{n=0}^{\infty} (-1)^n \frac{(x^3)^{2n+1}}{2n+1} \\
&= x^2 \cdot \sum_{n=0}^{\infty} (-1)^n \frac{x^{6n+3}}{2n+1} \\
&= \sum_{n=0}^{\infty} (-1)^n \frac{x^{6n+5}}{2n+1} \\
&\implies R = 1, \; I = (-1, 1)
\end{align*}

** Problem 25

\[\int \frac{t}{1 - t^8} \;dt\]

\begin{align*}
F(x) &= \int t \sum_{n=0}^{\infty} (t^8)^n \;dt \\
&= \int \sum_{n=0}^{\infty} t^{8n+1} \;dt \\
&= C + \sum_{n=0}^{\infty} \frac{t^{8n+2}}{8n+2} \\
|t^8| < 1 &\implies R = 1
\end{align*}

** Problem 27

\[\int x^2 \ln{(1 + x)} \;dx\]

\begin{align*}
F(x) &= \int \left[x^2 \sum_{n=1}^{\infty} (-1)^{n+1} \frac{x^n}{n}\right] dx \\
&= \sum_{n=1}^{\infty} \left[\frac{(-1)^{n+1}}{n} \int x^{n + 2} \;dx\right] \\
&= \sum_{n=1}^{\infty} \left[\frac{(-1)^{n+1}}{n} \cdot \frac{x^{n + 3}}{n + 3}\right] \\
&= C + \sum_{n=1}^{\infty} \left[\frac{(-1)^{n+1}x^{n + 3}}{n(n + 3)}\right] \\
|x| < 1 &\implies R = 1
\end{align*}

* 11.10

** Problem 4

\[f^{(n)}(4) = \frac{(-1)^n n!}{3^n (n + 1)}\]

\begin{align*}
T_n(x) &= \sum_{n=0}^{\infty} \frac{f^{(n)}(4)}{n!} (x - 4)^n \\
&= \sum_{n=0}^{\infty} \frac{(-1)^n}{3^n (n + 1)} (x - 4)^n
\end{align*}

\[R = 1\]

** Problem 7

\begin{align*}
f(x) &= \sqrt[3]{x}, \; a = 8 \\
f'(x) &= \frac{1}{3 \sqrt[3]{x^2}} \\
T_4{(8)} &= f(8) + \frac{f'(8)}{1!} (x - 8) + \frac{f''(8)}{2!} (x - 8)^2 + \frac{f'''(8)}{3!} (x - 8)^3 \\
&= 2 + \frac{1}{12} (x - 8) - \frac{1}{288} (x - 8)^2 + \frac{5}{20736} (x - 8)^3
\end{align*}

** Problem 9

\[f(x) = \sin{x}, \; a = \pi/6\]

** Problem 35

\[f(x) = \arctan{(x^2)}\]

\begin{align*}
f(x) &= \sum_{n=0}^{\infty} (-1)^n \frac{(x^2)^{2n+1}}{2n+1} \\
&= \sum_{n=0}^{\infty} (-1)^n \frac{x^{4n+2}}{2n+1} \\
&\implies R = 1
\end{align*}

** Problem 37

\[f(x) = x \cos{2x}\]

\begin{align*}
f(x) &= x \sum_{n=0}^{\infty} (-1)^n \frac{(2x)^{2n}}{(2n)!} \\
&= \sum_{n=0}^{\infty} (-1)^n \frac{2^{2n} x^{2n + 1}}{(2n)!}
\end{align*}

\begin{align*}
\lim_{n\to\infty} \left|\frac{a_{n+1}}{a_n}\right| &= \lim_{n\to\infty} \left|\frac{2^{2(n+1)} x^{2(n+1) + 1}}{(2(n+1))!} \cdot \frac{(2n)!}{2^{2n} x^{2n+1}}\right| \\
&= \lim_{n\to\infty} \left|\frac{2^{2n+2} x^{2n+3}}{(2n+2)!} \cdot \frac{(2n)!}{2^{2n} x^{2n+1}}\right| \\
&= \lim_{n\to\infty} \left|\frac{4x^2}{(2n+2)(2n+1)}\right| \\
&= 0 \\
&\implies R = \infty
\end{align*}

** Problem 39
** Problem 40
** Problem 54
** Problem 56
