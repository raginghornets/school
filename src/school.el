(defun school-delete-ltximg ()
  (interactive)
  (setq school-ltximg
      '("~/school/src/math-1041/ltximg"
        "~/school/src/cis-1166/ltximg"))
  (mapcar (lambda (dir)
            (if (file-directory-p dir)
                (delete-directory dir t)))
          school-ltximg))

(defun school-sitemap-format-entry (entry _style project)
  (when (string-match "index.org\\'" entry)
    (format "@@html:<span class=\"archive-item\">@@[[file:%s][%s]]@@html:</span>@@"
            entry
            (org-publish-find-title entry project))))

(defun school-sitemap-function (title list)
  (concat "#+title: " title "\n"
          "#+include: \"../README.org\""
          "\n#+begin_archive\n"
          (mapconcat (lambda (li)
                       (format "@@html:<li>@@%s@@html:</li>@@" (car li)))
                     (seq-filter #'car (cdr list))
                     "\n")
          "\n#+end_archive\n"))

(setq org-publish-project-alist
      `(("school-orgfiles"
         :base-directory "~/school/src"
         :recursive t
         :publishing-directory "~/school/docs"
         :publishing-function org-html-publish-to-html

         :lang "en"
         :with-toc t
         :title "airicbear.github.io"
         :email "tuk94307@temple.edu"
         :with-headline-numbers t
         :with-date nil
         :with-title t
         :time-stamp-file nil
         :section-numbers t
         :table-of-contents t
         :with-email nil
         :with-smart-quotes t
         :auto-preamble t
         :html-postamble nil

         :html-doctype "html5"
         :html-head-include-scripts nil
         :html-head-include-default-style nil
         :html-html5-fancy t
         :html-validation-link nil
         :html-link-home "index.html"
         :html-link-up ".."

         ;; Sitemap settings
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "Index"
         :sitemap-style list
         :sitemap-format-entry school-sitemap-format-entry
         :sitemap-function school-sitemap-function
         )

        ("school-static"
         :base-directory "~/school/src/"
         :base-extension "html\\|css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory "~/school/docs"
         :recursive t
         :publishing-function org-publish-attachment)

        ("school" :components ("school-orgfiles" "school-static"))))
