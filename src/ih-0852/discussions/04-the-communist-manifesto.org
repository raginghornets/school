#+TITLE: The Communist Manifesto
#+AUTHOR: Eric Nguyen

In Karl Marx's /The Communist Manifesto/, he introduces two social classes in modern society---the bourgeoisie and the proletariat.
These are analogous to social classes of the past, "Freeman and slave, patrician and plebeian, lord and serf, guild-master and journeyman, in a word, oppressor and oppressed" (9).
Respectively, the bourgeoisie are the oppressor exploiting the oppressed proletarians.
The unique aspect that separates the bourgeoisie and proletariat from previous social classes is that "it has simplified class antagonisms" which apply to society as a whole rather than localized groups of people.
This aspect emerged from an exponential growth in demand which could not be met by the feudal manufacturing system---leading to the Modern Industry and capitalism.
Marx may have chose to start the manifesto with this brief history for several reasons explaining (1) the rapid establishment of capitalism (2) the lack of consideration of class struggles in the development of capitalism and (3) the unnatural way of life that capitalism imposes on people.

Marx goes on to admit that capitalism has some desirable features including the rapid development of the means of production.
He does this to show that there are indeed some good qualities in capitalism and that a new communist society should maintain these qualities.
Indeed, he then asserts that capitalism is not without its faults, in an example, accusing it of the epidemic of over-production.
The epidemic of over-production can be described as the cycle produced by capitalism where excess capital must be destroyed or repurposed to restore profitability which consequently either reduces the consumer's ability to contribute to the demand or externalizes the excess capital somewhere else, continuing the cycle.
This implies that capitalism thrives off of mass destruction.
