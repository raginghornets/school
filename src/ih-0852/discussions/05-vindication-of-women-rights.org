#+TITLE: A Vindication of the Rights of Women
#+AUTHOR: Eric Nguyen

I imagine that the common healthcare metrics in the U.S. (e.g., infant mortality, life expectancy, mortality of pregnant women) perform worse than other countries due to social inequality.
The U.S. is one of the most socially diverse countries in the world and this diversity comes in several forms including wealth, race, age, gender, etc.
Through the lens of intersectionality, a diverse range of people may lead to varying magnitudes of discrimination against each individual.
For example, an intellectually impaired, obese, older woman of an ethnic minority may be exposed to higher amounts of discrimination than say, a brilliant, physically fit, young woman of the ethnic majority.
Indeed, the characteristics which may factor into a woman's risk of mortality when birthing would involve their physical and medical condition, their socioeconomic status, and their level of education.

Unfortunately, the distinction between men and women also creates unequal opportunities between them in many cases.
Women have historically been regarded to be dependent on men due to an ongoing narrative which they have been educated to conform with, as Wollstonecraft mentions, "...slavery which chains the very soul of woman, keeping for ever under the bondage of ignorance" (149).
Especially with the advent of feminism, the idea that women are obedient and dependent on men has gradually diverged, allowing for more equal opportunities between men and women.
However, the effects of these unnatural distinctions are still noticeable in today's society.
To support this argument, it is necessary to look at the gender pay gap in the U.S.
From the U.S. Bureau of Labor Statistics[1], we are shown that women in the U.S. earn roughly 82% of that of what men earn in the U.S.
With a significant gender pay gap, healthcare becomes generally less affordable for women, thus leading to poorer healthcare metrics.
This same principle can be used with intersectionality to determine the healthcare opportunities for people with different identity factors such as wealth, disability, race, etc.

[1] https://www.bls.gov/opub/ted/2017/womens-median-earnings-82-percent-of-mens-in-2016.htm
