#+TITLE: Prompt 4: Infrastructure vs. Superstructure and Dialectical Materialism
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
#+LATEX_HEADER: \usepackage[doublespacing]{setspace}
#+LATEX_HEADER: \frenchspacing
#+LATEX_CLASS_OPTIONS: [12pt]
#+EXPORT_FILE_NAME: Prompt04_NguyenEric

* Infrastructure: the internet

First, I will discuss upon the internet and telecommunication infrastructure and how it affects society.
I chose to discuss this topic as it apparently greatly influences my day-to-day life as it does for most people I know of, for example it is integral in my education.
Especially given the current circumstances of the ongoing pandemic, the internet and communication infrastructure is more important than ever as people around the world are discouraged to socialize in-person in an effort to prevent the spread of the dreadful coronavirus.
This infrastructure enables people to communicate with one another at great distances and especially with the internet, allows them to reliably transfer and receive large amounts of information at incredible speeds.
In conjunction with information technology infrastructure, this information may be persisted for later use and distributed on demand via the internet and telecommunication infrastructure.
With unprecedented levels of information and communication we are able to, for example, communicate with other people across the world in real-time with synchronized and high quality video, audio, and text, download and engage in immersive computer games and simulations with other people in real-time, and precisely retrieve information at any moment from the vast pool of information that is the internet.

* Superstructure: communism

Now, I will write about the superstructure of communism.
Communism is a type of governmental structure that does away with the class antagonisms present in, for example, capitalism and feudalism, by abolishing private property.
Under this superstructure, the means of production is equally owned by each individual member of the state and each person is rewarded according to their needs rather than their material wealth.
In theory, communism is anticipated by German philosopher Karl Marx to be inevitable and prevail as the ultimate form of government once capitalism eventually devours itself, "What the bourgeoisie therefore produces, above all, are its own grave-diggers. Its fall and the victory of the proletariat are equally inevitable" (21).
However, as it remains today, capitalism continues to stand strong while communism has proven to be infeasible as society is unable to cooperate together on such a large scale without some form of leadership and material incentive.
Many efforts in the establishment of communist governments end up under corrupt dictatorships whereby the members of the state are oppressed and their freedoms constrained.
A prime example of this extreme form of government would be the Chinese Communist Party (CCP) which emerged from Soviet efforts to realize and propagate Marxist ideas throughout China.
This development led to several violent revolutions in China, eventually resulting in the victory of the communists and the creation of the People's Republic of China (PRC) which is still governed by the CCP today, however their ideologies have since shifted towards more socialist ideals.
The Chinese government has been heavily criticized for its extensive censorship of the internet so much so that their strict, yet technologically advanced policies came known to be "The Great Firewall of China".
In more recent news, the Chinese government has garnered international condemnation for its appalling mistreatment and genocide of an ethnic minority known as the Uyghurs in a process apparently motivated by ethnic cleansing.

* The relationship between the internet and communism

In Marx's ideal vision of a communist society, the internet would help drive the economy by greatly accelerating the rate of improvement of the means of production.
However, communism and the internet have clearly proven to be incompatible in practice due to the desire of eliminating any presence of capitalism in society by communist leaders.
This conflict explains one reason why communist governments do not work well in modern society where the interenet dominates the world, yet communist efforts often involve the severe restriction of internet access and usage as a means to prevent the rise of capitalism from groups organized via the internet.
Indeed, from this example I would agree with Marx that the infrastructure of a society precedes and leads to its superstructure as it is apparent that the dominant internet infrastructure demands such a superstructure that allows for freedom of speech and expression.
Capitalism is successful in this regard as it satisfies the conditions required by the modern infrastructure in addition to being a powerfully productive system thus explaining how it came to be one of the most common superstructures in today's world.
