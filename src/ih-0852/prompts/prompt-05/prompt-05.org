#+TITLE: Prompt 5: /A Vindication of the Rights of Woman/
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
#+LATEX_HEADER: \usepackage[doublespacing]{setspace}
#+LATEX_HEADER: \frenchspacing
#+LATEX_CLASS_OPTIONS: [12pt]
#+EXPORT_FILE_NAME: Prompt05_NguyenEric

In the ninth chapter of /The Vindication of the Rights of Woman/, Mary Wollstonecraft describes how the inequality between the men and women in society lead to the various social consequences and oppressive measures which women must deal with throughout their life.
She then goes on to describe how there are certain classes in society, "...dividing the world between voluptuous tyrants, and cunning envious dependents..." (149), that is, the classes in a position of authority over the oppressed, dependent classes.
This framework of authority and dependents can be applied to the relationship between man and woman, where the man would take the position of authority while the woman would be the obedient dependent of the man.
In this scenario, the woman loses the capacity to attain neither virtue nor authority as she is constrained to the will of the man, unable to freely think nor act for herself.
This inescapable oppression is detrimental to the woman, as her self-worth is degraded to a mere object of the man and of no influence to society.
Her potential to be greater person is lost, "How many women thus waste life away, the prey of discontent, who might have practised as physicians, regulated a farm, managed a shop, and stood erect..." (153).
Then the woman is unable to contribute to neither her family nor society, and as a result both parties lose her potential; yet she still must be supported, creating an unnecessary amount of pressure and responsibility on her family and society.
Indeed, this signifies that women are oppressed regardless of their class, as even at the highest levels of respectability, they still must ultimately submit to men without any say in their actions.

Intersectionality can be defined as an understanding of a person's identity based on each social category (e.g., race, class, gender) they may fall into and how such an identity relates to how they are discriminated against.
This term shows that people can be discriminated against for certain characteristics about themselves and depending on how many undesirable characteristics they have, they may be exposed to varying amounts of discrimination.
A person can hold multiple kinds of identities, for example a person could be both an African-American and a woman.
Both of these facts can then be used to discriminate against them, where a woman might be discriminated when applying for a job and an African-American might be discriminated when trying to applying to a university simply due to the color of their skin.
Combined together, that person could struggle to find a job in addition to struggling to get accepted into the university they want to attend, causing undue pressure on them compared to folks who are otherwise in a similar position as them.
Perhaps if they only had one of those characteristics, or even none, then they would not have to deal with these inherent disadvantages and could have the same and equal opportunities as everyone else.
From this example, we can see that a person can face higher amounts of discrimination as they fall into a larger number of social categories that are discriminated against.
Indeed, this ties into Wollstonecraft's critique of class.
She states that people of higher class are respected more than those of the lower class, even if they do not deserve that respect.
She then goes on to suggest that people (particularly women) should earn their respect based on their merit, rather than the socioeconomic status, "How much more respectable is the woman who earns her own bread by fulfilling any duty, than the most accomplished beauty!" (153).
Indeed, this train of thought falls squarely in line with Marx's principles on class conflict and the abolition of classes.
These two ideas converge with one another as they both agree upon the oppressive nature of a class-based society and how the people who deserve respect the most are unable to earn it.
Taking into consideration Wollstonecraft's argument, we see that there is another set of classes which divide men and women.
Her advocacy to eliminate this separation between the men and women is similar to Marx's idea of a classless society.
