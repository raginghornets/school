#+TITLE: Part 1: Summary of /Lakota Woman/, Chapter 16
#+AUTHOR: Eric Nguyen
#+OPTIONS: toc:nil num:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
#+LATEX_HEADER: \usepackage[doublespacing]{setspace}
#+LATEX_HEADER: \frenchspacing
#+LATEX_CLASS_OPTIONS: [12pt]
#+EXPORT_FILE_NAME: FinalProject_NguyenEric

In the first half of /Lakota Woman/, Mary Brave Bird, also known as Mary Crow Dog, describes through a series of personal accounts and anecdotes her difficult upbringing and experience as a iyeska (half-blood) woman of the Lakota Native American tribe growing up in South Dakota during the 1970s.
Specifically, she emphasizes the poor conditions which she and her family lived with (p. 18), the influence of white culture and religion on the traditions and practices of Native Americans, the racial discrimination and injustice she and her people faced, the influence of drugs and alcohol in her family and her own life (p. 44), and the dilemma of being a half-blood Indian and the lack of sense of belonging.
With the cumulative series of accounts describing her life, she finally brings up her discovery of the American Indian Movement (AIM) and how joining the Indian civil rights group became a turning point in her life, at last providing her a sense of belonging and purpose in life, as well as where she would end up finding a man by the name of Leonard Crow Dog who would later become her husband (pp. 72, 105).
On February 27, 1973, she and a large group of AIM members bravely set out for Wounded Knee (a town in South Dakota of great historical significance where many Lakota Indians had been massacred in 1890, acting as a mass grave for the ancestors of Brave Bird), passing through the Pine Ridge Reservation even as U.S. marshals and government snipers watched from their rooftops (pp. 124-125).
In occupying Wounded Knee, the AIM members had made twenty demands from the U.S. government which were rejected accordingly by government negotiators, leading to the siege of Wounded Knee after which Brave Bird would marry Crow Dog who would come to be arrested and charged for his involvement in coordinating the occupation of Wounded Knee, among other various false charges (pp. 88, 216, 220).
Crow Dog would be sentenced for a total of twenty-three years, but fortunately would only go on to spend just over a year in prison after his lawyer is finally able to get him released after protesting to the judge, realizing that there was no other way that Crow Dog would promptly get his justice (p. 239).

In Chapter 16, Brave Bird writes about the sentimental feelings and emotions she and her family experienced after Crow Dog's release, as well as her experience at the Sun Dance (the most important Lakota Indian ritual which occurred annually at the peak of each summer) which followed his release (pp. 243, 253).
During the time Crow Dog had spent incarcerated in a maximum-security prison in Lewisburg, Pennsylvania, Brave Bird had resided in New York for nearly a year with her little son Pedro (who she recently gave birth to at Wounded Knee) so that she would be closer to her husband (pp. 112, 243).
Once Crow Dog was released and came back with Brave Bird and little Pedro to their home at Rosebud Sioux Reservation, they found themselves tremendously welcomed by the tribe---however things have changed drastically since they have been gone and they also had to adapt to the poorer living conditions.
Crow Dog's daughters Ina and Bernadette had grown from being little girls to young women and his sons Richard and Quanah could barely recognize their father, while the elderly had become too weak to be able to properly nurture the children.
Brave Bird, having only seen her husband on a few occasions up until then, found herself acting awkward and shy being around him, but also getting into less arguments and fights with him, noticing that he became more understanding and tolerant of her and of women in general (pp. 243, 246).
In an attempt to make her feel better, Crow Dog would go on to convince Brave Bird about her role as a Lakota Woman, telling her of the legends of the First Woman and First Man, as well as the legend of Ptesan Win, the White Buffalo Woman, the greatest hero of Lakota (pp. 66, 246-247).

The epigraph for Chapter 16 tells us of the importance of the White Buffalo Woman.
"Many times he also told us the story of the White Buffalo Woman who brought to our people the most sacred of all things, the ptehincala-huhu-chanunpa, the sacred pipe around which our faith revolves" (p. 247).
The ptehincala-huhu-chanunpa, or the White Buffalo Calf Pipe, is a holy pipe made of actual buffalo legbone and as Brave Bird explains, is the core of the faith of the Lakota people---it is extremely important to their identity (p. 248).
For the next Sun Dance, Brave Bird was instructed by Crow Dog that she would stand with the pipe, acting as the White Buffalo Woman, in accordance with Crow Dog's vision that men and women have their own separate roles in the Sun Dance (p. 246).
In remembrance of the 1890 Wounded Knee massacre, Crow Dog and a few other medicine men would choose the site of the next Sun Dance to be at Wounded Knee (p. 254).
Upon arrival they would begin their Sun Dance, only to be promptly interrupted by a band of tribal police who would take the dancers to court and have them fined on the basis that they were disturbing another nearby, commerical Sun Dance going on in Pine Ridge---that is, a non-traditional Sun Dance analogous to a circus (pp. 254-255).
Crow Dog would not let this interruption stop his Sun Dance, however, and surely enough the dancers would find a way to continue the Sun Dance, albeit not at Pine Ridge Reservation, but instead back at Crow Dog's home at the Rosebud Reservation (p. 255).

This raised a challenge as they would need a two-forked cottonwood tree to act as the sacred Sun Dance pole standing in the center of the Sun Dance circle.
However, with great luck, a large truck manned by several young white wen had arrived after they had overheard of the dancers' troubles, offering to transport the tree for the dancers (p. 255).
While Crow Dog would make an objection against having the tree transported by truck, saying that tradition required it to be carried by foot in a sacred manner, another medicine man by the name Bill Eagle Feathers would convince him to allow it for this occasion, seeing that it would be infeasible to carry the tree eighty miles by foot (pp. 255-256).
Upon arriving at Crow Dog's home, they prepared the Sun Dance circle once again, planting the tree in a hole filled with buffalo fat and constructing two figures using buffalo hide---one representing a man and the other representing a buffalo bull (p. 256).
The following morning, Bill Eagle Feathers would call upon the eagles using his eagle-bone whistle and indeed an eagle flew in from the east, signaling that the dance was blessed, and soon after the Sun Dance would be finished in a traditional manner as Crow Dog had intended (p. 256).

In the years going forward, the tribe would continue to perform the traditional Sun Dance at Crow Dog's home (p. 257).
While Brave Bird would not fully understand the value of the Sun Dance, it grew on her and she eventually learned to embrace the ritual (p. 257).
As a part of tradition, the ritual would involve flesh offerings and piercings which were quite painful and agonizing, but acted as sacrifices to all of the brothers and sisters who had died before them (p. 258).
Brave Bird would come to see some of her fellow dancers perform some of the most unimaginable acts of self-torture (p. 258).
She would go on to have herself pierced, gazing into the sky and experiencing incredible spiritual power, "It was at that moment that I, a white-educated half-blood, became wholly Indian" (p. 260), feeling a great surge of happiness, she cried to the sky.

* Part 2, Option C: The Sun Dance

#+CAPTION: Mary Brave Bird participating in a Sun Dance ritual
#+ATTR_LATEX: :width 13cm
[[./sundance2.png]]

At the height of each summer, presumably on a bright and sunny day, many Native Americans gather their tribes to perform the most important ritual of their tradition: the Sun Dance.
During the Sun Dance, "participants pierce their flesh with skewers to help someone dear to them" (p. 253)---this is analogous to how Christians worship Jesus Christ's suffering for the people of the world, yet the Native Americans would find the Christians to act very hostile towards the Native American practices for being "barbaric, superstitious, and preventing the Indians from becoming civilized" (p. 253).
John Lame Deer (an influential Lakota medicine man among the same group as Leonard Crow Dog, titled as the Seeker of Visions), better known as Lame Deer, explains the key difference between the Native American faith and Christian faith being that for Christians, all of the suffering is inflicted upon Jesus, however the Indians in their religious practices would distribute that suffering across its many people (including young children and women) rather than a singular entity (p. 253).
However, having been banned for half a century, the Sun Dances would have to be conducted privately and discreetly, away from the rest of the population, because if any person were caught sun-dancing or even participating in any tribal ceremony, they risked getting arrested by government officials and sent to jail for an indeterminable amount of time.
In Brave Bird's case, she would participate in Sun Dances in the comfort and privacy of her husband Crow Dog's home.

The drawing depicts Mary Brave Bird participating in a Sun Dance on a beautiful summer day.
She has both of her arms pierced by skewers with rawhide ropes attaching her flesh to the sacred Sun Dance pole, a two-forked cottonwood tree which stood at the center of the Sun Dance circle.
She is gazing into sky towards the clouds and the sun, channeling spiritual energy within her, listening to the sun speak out divine words.
She feels a flood of happiness throughout her body as she sees the faces of her loved ones who have passed away, hearing their voices.
She, who is a white-educated half-blooded Indian woman, in this moment becomes a whole Indian in every aspect.
