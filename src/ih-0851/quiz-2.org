#+title: SECOND QUIZ---IH851---SPRING 2020---MIDDLESWORTH

* Notice

  We are not able because of the coronavirus quarantine meet in class
  in person in order for you to take the second quiz as a closed
  book one.
  So we must be resilient and adapt to the new circumstances.
  I've already notified everyone through Canvas that I've designed
  an alternate format:
  To provide you with a direct quotation that you will need to
  connect to our final three texts:
  /Love's Alchemy: Poems from the Sufi Tradition,/
  Franz Kafka's short story /The Judgment/
  (included in the text /The Sons/),
  plus Part 1 of Han Kang's /The Vegetarian./

* Submission

  *PLEASE NOTE:*
  Your response consisting of a full one and a half to two pages
  is due this Monday, April 13 submitted to me on Canvas.
  NOT as an attachment to my TU email.

* Description

  Please select one of the following two approaches to implement.

  1. You are a Sufi guidance counselor who has been called in to guide
     the two main characters in the short story /The Judgment/
     (the young merchant Georg Bendemann and his elderly father)
     and the two main characters (Yeong-hye and her husband Mr. Cheong)
     in Part 1 of the novel /The Vegetarian./
     Imagine yourself talking with them to try to help them resolve
     their conflicts and offer comfort by way of the teachings in
     one of the Sufi poems.
     What do you say to them using as your basis one of the poems
     that you've read and maybe especially liked in our text?
     See this as a monologue or dialogue, providing the words
     that you would use talking with the characters.
     Like a play or movie.

     OR ALTERNATELY

  2. You are one of the characters in these two stories talking in a
     monologue with the other main character in that story, attempting
     to solve their problems by way of the wisdom teachings of one of
     the poems in /Love's Alchemy./
     OR This could be in the form of a dialogue in which the two
     characters talk with each other trying to come to a resolve
     in their troubled relationship by way of referencing and
     discussing one of the poems.

  In either case, whether you select option 1 or 2, you will need to
  include a very brief short direct quote from the poem that you
  select to include in your approach.

* Example references

  Your choice of the poem/s you wish to reference is up to you. Here
  though following are some to consider using:

** Remember the Truth

   #+begin_quote
   The soul is love and affection--- \\
   so know the soul. \\

   But the Truth is ecstasy, forgetting yourself.

   When “I” and “you” are present, \\
   you're thinking from \\
   the ego's place.

   If you hear those words, \\
   then remember the Truth. \\
   \\

   /Saghir Isfahani/
   #+end_quote

** Their Number Is Few

   #+begin_quote
   The sea is full of pearls \\
   but the number \\
   of pearl-divers \\
   is few.

   The world is full of music, \\
   but the number \\
   of dancers \\
   is few.

   Now I'm afraid \\
   that no prayer \\
   will be answered--- \\
   since those \\
   who pray truly \\
   are really quite few. \\
   \\

   /Imad al-Din Faqih Kirmani/
   #+end_quote

** Losing Your Head

   #+begin_quote
   The first step in love \\
   is losing your head.

   After the petty ego, \\
   you then give up your life \\
   and bear the calamity.

   With this behind you, proceed: \\
   Polish the ego's rust \\
   from the mirror \\
   of yourself. \\
   \\

   /Fakhr al-Din Iraqi/
   #+end_quote

** That Which Frees You

   #+begin_quote
   The bird that sings \\
   pain's son \\
   is love.

   The messenger skilled \\
   in the language \\
   of the unseen world \\
   is love.

   It is love that speaks to you, \\
   calling you beyond the limits \\
   of this created realm.

   That which frees you \\
   from your tiny self \\
   also is love. \\
   \\

   /Khaqani Shirwani/
   #+end_quote

   There are many other poems in the collection /Love's Alchemy/ that you
   can select and that can work very well in your response.
   These are just a few.
