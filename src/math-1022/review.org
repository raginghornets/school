#+title: Final Exam Review
#+subtitle: Math 1022 - Precalculus
#+author: Eric Nguyen
* 2.1
** 23
$$\begin{align}
f(x) &= x^2 + 2x \\
f(0) &= (0)^2 + 2(0) \\
&= 0 + 0 \\
&= 0 \\
f(3) &= (3)^2 + 2(3) \\
&= 9 + 6 \\
&= 15 \\
f(-3) &= (-3)^2 + 2(-3) \\
&= 9 - 6 \\
&= 3 \\
f(a) &= (a)^2 + 2(a) \\
&= a^2 + 2a \\
f(-x) &= (-x)^2 + 2(-x) \\
&= x^2 - 2x \\
f{\left(\frac{1}{a}\right)} &= {\left(\frac{1}{a}\right)}^2 + 2 {\left(\frac{1}{a}\right)} \\
&= \frac{1^2}{a^2} + {\left(\frac{2}{a}\right)} \\
&= \frac{1}{a^2} + \frac{2}{a} \\
\end{align}$$
** 33
$$\begin{align}
f(x) &= \begin{cases}
x^2 + 2x &\quad \text{if } x \leq - 1 \\
x &\quad \text{if } -1 < x \leq 1 \\
-1 &\quad \text{if } x > 1
\end{cases} \\\\
f(-4) &= (-4)^2 + 2(-4) \\
&= 16 - 8 \\
&= 8 \\
f{\left(-\frac{3}{2}\right)} &= {\left(-\frac{3}{2}\right)}^2 + 2{\left(-\frac{3}{2}\right)} \\
&= {\left(\frac{3^2}{2^2}\right)} - 3 \\
&= \frac{9}{4} - 3 \\
&= \frac{9}{4} - \frac{12}{4} \\
&= -\frac{3}{4} \\
f(-1) &= (-1)^2 + 2(-1) \\
&= 1 - 2 \\
&= -1 \\
f(0) &= 0 \\
f(25) &= -1
\end{align}$$
** 47
$$\begin{align}
f(x) &= \frac{x}{x + 1} \\
f(a) &= \frac{a}{a + 1} \\
f(a + h) &= \frac{(a + h)}{(a + h) + 1} \\
\frac{f(a + h) - f(a)}{h} &= \frac{{\left(\frac{(a + h)}{(a + h) + 1}\right)} - {\left(\frac{a}{a + 1}\right)}}{h} \\
&= \frac{(a + h) (a + 1) - a (a + h + 1)}{h (a + h + 1) (a + 1)} \\
&= \frac{a^2 + a + ha + h - a^2 - ha - a}{h (a + h + 1) (a + 1)} \\
&= \frac{1}{(a + h + 1) (a + 1)}
\end{align}$$
** 49
$$\begin{align}
f(x) &= 3 - 5x + 4x^2 \\
f(a) &= 3 - 5a + 4a^2 \\
f(a + h) &= 3 - 5(a + h) + 4(a + h)^2 \\
&= 3 - 5a - 5h +4(a^2 + 2ah + h^2) \\
&= 3 - 5a - 5h + 4a^2 + 8ah + 4h^2 \\
\frac{f(a + h) - f(a)}{h} &= \frac{(3 - 5a - 5h + 4a^2 + 8ah + 4h^2) - (3 - 5a + 4a^2)}{h} \\
&= \frac{3 - 5a - 5h + 4a^2 + 8ah + 4h^2 - 3 + 5a - 4a^2}{h} \\
&= \frac{-5h + 8ah + 4h^2}{h} \\
&= -5 + 8a + 4h
\end{align}$$
** 57
$$\begin{align}
f(x) &= \frac{x + 2}{x^2 - 1} \\
x^2 - 1 &\neq 0 \\
(x + 1) (x - 1) &\neq 0 \\
D_f &= \{x \mid x \neq \pm 1\}
\end{align}$$
** 64
$$\begin{align}
g(x) &= \sqrt{x^2 - 4} \\
x^2 - 4 &> 0 \\
(x + 2) (x - 2) &> 0 \\
D_g &= (-\infty, -2) \cup (2, \infty)
\end{align}$$
** 65
$$\begin{align}
g(x) &= \frac{\sqrt{2 + x}}{3 - x} \\
3 - x &\neq 0 \\
x &\neq 3 \\
2 + x &\geq 0 \\
x &\geq -2 \\
D_g &= [-2, 3) \cup (3, \infty)
\end{align}$$
** 66
$$\begin{align}
g(x) &= \frac{\sqrt{x}}{2x^2 + x - 1} \\
2x^2 + x - 1 &\neq 0 \\
(2x - 1) (x + 1) &\neq 0 \\
2x - 1 &\neq 0 \\
2x &\neq 1 \\
x &\neq \frac{1}{2} \\
x + 1 &\neq 0 \\
x &\neq -1 \\
x &\geq 0 \\
D_g &= \left[0, \frac{1}{2}\right) \cup \left(\frac{1}{2}, \infty\right)
\end{align}$$
** 69
$$\begin{align}
f(x) &= \frac{3}{\sqrt{x - 4}} \\
x - 4 &> 0 \\
x &> 4 \\
D_f &= (4, \infty)
\end{align}$$
* 2.2
** 45
$$\begin{align}
f(x) &= \begin{cases}
4 &\quad \text{if } x < -2 \\
x^2 &\quad \text{if } -2 \leq x \leq 2 \\
-x + 6 &\quad \text{if } x > 2
\end{cases}
\end{align}$$

| $x$ | $y$ |
|-----+-----|
|  -4 |   4 |
|  -3 |   4 |
|  -2 |   4 |
|  -1 |   1 |
|   0 |   0 |
|   1 |   1 |
|   2 |   4 |
|   3 |   3 |
|   4 |   2 |

#+caption: Plot of $f(x)$
[[file:figures/REVIEW_0202_45.png]]
* 2.3
** 8
*** a)
$$\begin{align}
g(-4) &= 3 \\
g(-2) &= 2 \\
g(0) &= -2 \\
g(2) &= 1 \\
g(4) &= 0
\end{align}$$
*** b)
$$\begin{align}
D_g &= [-4, 4] \\
R_g &= [-2, 3]
\end{align}$$
*** c)
$$\begin{align}
x &= 3
\end{align}$$
*** d)
$$\begin{align}
1
\end{align}$$
** 31
*** a)
*** b)
** 43
*** a)
*** b)
* 3.7
** 3
** 17
* 2.6
** 9a
** 31
** 45
** 46
** 63
** 85
** 87
** 88
* 3.6
** 13
** 33
** 35
** 41
* 2.7
** 51
** 55
** 63
** 67
* 2.8
** 49
** 57
** 65
** 67
** 89
* 4.1
** 27
** 39
* 4.2
** 7
** 11
* 4.3
** 33
** 65
** 73
** 75
* 4.4
** 37
** 39
** 41
** 51
** 53
* 4.5
** 13
** 29
** 39
** 45
** 51
** 53
** 65
** 68
* 6.2
** 23
** 27
* 6.3
** 25
** 26
** 27
** 28
** 47
** 51
** 53
* Ch. 6 Review
** 45
** 55
** 57
** 58
** 59
* 5.3
** 13
** 15
** 19
** 29
* 5.5
** 3
** 5
** 7
** 9
** 37
** 38
** 41
** 43
** 45
** 47
* 7.1
** 13
** 15
** 25
** 33
** 53
** 89
* 7.3
** 51
** 53
** 75
* 7.4
** 5
** 25
** 27
** 45
* 7.5
** 9
** 43
* 8.1
** 31
** 33
** 37
** 38
