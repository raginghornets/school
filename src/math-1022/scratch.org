#+title: Scratch Paper

* 2019-09-01
$g(x) = \frac{8 - x}{8 + x}$

$$\begin{aligned}
g\left(\frac{1}{2}\right) &= \frac{8 - \frac{1}{2}}{8 + \frac{1}{2}} \\
&= \frac{\frac{16}{2} - \frac{1}{2}}{\frac{16}{2} + \frac{1}{2}} \\
&= \frac{\frac{15}{2}}{\frac{17}{2}} \\
&= \frac{15}{2} \times \frac{2}{17} \\
&= \boxed{\frac{15}{17}}
\end{aligned}$$

$f(x) = 2x^2 + 3$

$$\begin{aligned}
f(x + 2) &= 2(x + 2)(x + 2) + 3 \\
&= 2(x^2 + 4x + 4) + 3 \\
&= \boxed{2x^2 + 8x + 11}
\end{aligned}$$

$$\begin{aligned}
f(x) + f(2) &= 2x^2 + 3 + 2(2)^2 + 3 \\
&= 2x^2 + 3 + 8 + 3 \\
&= \boxed{2x^2 + 14}
\end{aligned}$$

$f(x) = \frac{5}{x + 3}$

$$\begin{aligned}
f(a) &= \boxed{\frac{5}{a + 3}}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= \boxed{\frac{5}{a + h + 3}}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{\frac{5}{a + h + 3} - \frac{5}{a + 3}}{h} \\
&= \frac{\frac{5(a + 3)}{(a + h + 3)(a + 3)} - \frac{5(a + h + 3)}{(a + h + 3)(a + 3)}}{h} \\
&= \frac{5a + 15 - 5a - 5h - 15}{h(a + h + 3)(a + 3)} \\
&= \boxed{\frac{-5h}{h(a + h + 3)(a + 3)}}
\end{aligned}$$

$f(x) = 6 - 7x + 3x^2$

$$\begin{aligned}
f(a) &= \boxed{6 - 7a + 3a^2}
\end{aligned}$$

$$\begin{aligned}
f(a + h) &= 6 - 7(a + h) + 3(a + h)^2 \\
&= 6 - 7a - 7h + 3(a^2 + 2ah + h^2) \\
&= \boxed{6 - 7a - 7h + 3a^2 + 6ah + 3h^2}
\end{aligned}$$

$$\begin{aligned}
\frac{f(a + h) - f(a)}{h} &= \frac{(6 - 7a - 7h + 3a^2 + 6ah + 3h^2) - (6 - 7a + 3a^2)}{h} \\
&= \frac{6 - 7a - 7h + 3a^2 + 6ah + 3h^2 - 6 + 7a - 3a^2}{h} \\
&= \frac{-7h + 6ah + 3h^2}{h} \\
&= \boxed{-7 + 6a + 3h}
\end{aligned}$$
* 2019-09-11
** Solving inequalities graphically
*** a)
$$\begin{aligned}
2x^2 + 3 &= 5x + 6  \\
2x^2 - 5x - 3 &= 0 \\
(2x + 1)(x - 3) &= 0 \\
x_1 = - \frac{1}{2}, &\quad x_2 = 3
\end{aligned}$$


