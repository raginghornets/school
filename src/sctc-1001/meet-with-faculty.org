#+title: Meet with Faculty
#+author: Eric Nguyen
#+date: December 6, 2019
#+options: toc:nil num:nil
#+latex_class_options: [12pt]
#+latex_header: \usepackage[margin=1in]{geometry}

* Synopsis

  For my assignment of meeting with a faculty member, I chose to meet with my MATH 1022 (Precalculus) instructor, Dr.\nbsp{}Peiwan Tan.
  I specifically chose to meet her out of the all the other professors that I could have potentially met simply because I enjoy her math class and it is the least stressful class for me.
  I am very comfortable with the class and am confident in my ability to succeed in that class.
  As such, it made it most comfortable for me to meet her as opposed to other professors.

  To meet with her, I contacted her via email beforehand, sending the following email:
  #+begin_quote
  Hi Professor Tan,

  I would like to meet you during your office hours today to review the quiz and discuss about my career.

  -- Eric
  #+end_quote
  On Monday, December 2, just after Fall Break, I met with her in her office on the 10th floor of Wachman Hall in room 1029.
  The process of meeting her was relaxed and laid back and I had no trouble meeting her.

  I will admit I was feeling slightly nervous as it would be my first time ever visiting a professor during office hours.
  I am happy to say that the nerves shook off once Dr.\nbsp{}Tan arrived to her office and we were able to generate good conversation from there on.

  Going into her office, I was initially planning on reviewing the weekly quiz on inverse trigonometric functions and discussing my career.
  So, after I sat down, I told her about how I did not feel confident on the last quiz.
  She told about how two quizzes can be withdrawn, so I would not need to worry about it too much.

  Afterwards, she praised my performance in her class, noting that I was one of her top students.
  She further elaborated on how the other students who sit in the front row also perform well and notices how I interact with them.
  Surprised, I thanked her and told her I am very happy to hear such news, as precalculus had been a subject I had struggled with back in high school.

  However, one criticism she did have for me was my quiet personality and how it is better to solve problems on the board when the opportunity comes up.
  She talked with me how, whether or not I know how to solve the problem on the board, even if I do not know how to solve it, I will learn from my mistakes.
  This would be especially reinforcing as the entire class would be watching me, making it so that if were to make a mistake, I would never make the mistake again

  From there, I then asked about her life and experience.
  I learned how she has been an outstanding scholar throughout her life, starting from 5 years old.
  She has always achieved a GPA of 4.0, no less---not even a single grade less than 95%.
  Her parents raised her very well along with her sisters and they would study with each other all the time.
  She never had time to chit-chat, as she would always be studying.
  Because she excelled so well in school, she received a scholarship along with the financial support from her parents, so she never had to work during school.

  She always aspired to be a scientist at a world-class lab since she was young.
  Accordingly, she studied very much and very hard, accumulating various science degrees under her belt including a master's degree in chemical engineering and a postdoc in chemistry at a world-renowned university in Belgium.
  One of the problems she faced in doing this was noticing that her health was deteriorating.
  Despite getting invited to become a scientist, she decided that, for the best of her well-being, it would be better for her to be a professor at university and stay near her husband.
  She told me how, as a professor, it is very hard work.
  She explained how she must constantly produce papers, teach classes, and finally, must be there to support her students.
  It takes a mental as well as physical toll on her and indeed, she disclosed to me that she has no time to exercise.
  She argues that, while it is a great amount of effort, it will pay off and she will be able to live happy and well afterwards.

  After telling me all about herself, we continued to talk about my career, why I chose data science, which classes I will be taking, and a number of other things that I won't go into detail about.
  She recalled from last class about the course I had signed up for, MATH 1033 (Computing in MATLAB), letting me know she had mistaken it for MATH 1039 (Lab for Calculus I) back when she was discouraging me from registering for the course.
  The conversation pretty much ended at that point, as she had work to get done, such as attendance.
  As I left, she let me know that I can come on Wednesday if I want to further discuss about any questions, concerns, or anything.

  In having met with her, I felt more understanding of her and where she comes from.
  Knowing that she always had perfect scores and studied at highly prestigious universities, I understand why she sometimes feels the way she does when students fail to solve what are seemingly trivial problems.
  Indeed, knowing this, I am also more motivated to study harder and make academics a higher priority in life.
  This meeting was a great experience and I would gladly appoint another meeting with her again if I ever had anything to talk about with her.
  It has shown me how office hours can be highly beneficial in building relationships with faculty.
