%% Equilibrium Distribution
%
% Author:  Eric Nguyen
% Date:    2020-04-24
% Section: 007
%
% Written in MATLAB 2019b.
% You can run this code using the MATLAB desktop
% application or you can visit https://matlab.mathworks.com/
% for an online version of MATLAB.
%
% Alternatively, you can use the GPL licensed
% GNU/Octave, available at https://www.gnu.org/software/octave/
%

%% Program Description
%
% Find the equilibrium distribution of
% any regular Markov chain using the
% power method as described in Exercise 14 (c).
%
% Note:
% User input for the matrix preferably should
% be in the format "[a b c; d e f; g h i]"
%               or "[a,b,c; d,e,f; g,h,i]"
% for a matrix
%                         [a b c
%                          d e f
%                          g h i]
%
% where columns are delimited by spaces and
% rows are delimited by semicolons.
%

%% Using the power to compute the equilibrium distribution of a regular Markov chain

% Assume k is sufficiently large for the given regular Markov chain.
k = 1000;

% The only input required is the matrix of transition probabilities
% since any initial probability distribution will work as shown in
% Exercise 14 (a).
T = input('Enter a regular matrix of transition probabilities: ');
Tk = T^k;

% All we have to do is return any row from the kth power of the
% matrix of transition probabilities since we know that for large
% k, each row of T^k will be very close to the equilibrium distribution.
%
% We choose row one for the sake of simplicity.
fprintf('The equilibrium distribution is Qe = ( ');
fprintf('%0.3f ',Tk(1,:));
fprintf(').\n');
