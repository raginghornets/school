#+title: Finite Markov Chains
#+author: Eric Nguyen
#+latex_header: \usepackage{multirow}
#+latex_header: \renewcommand\theequation{\thepart\roman{equation}}
#+latex_header: \usepackage{amsthm}
#+latex_header: \newtheorem{theorem}{Theorem}
#+latex_header: \newtheorem{corollary}{Corollary}
#+latex_header: \theoremstyle{definition}
#+latex_header: \newtheorem{definition}{Definition}
#+latex_header: \theoremstyle{remark}
#+latex_header: \newtheorem*{remark}{Remark}

* Description

  For the final project of my discrete math class,
  I was given the option to choose one of the following
  chapters to read and complete its exercises:

  1. The Apportionment Problem

  2. Finite Markov Chains

  3. Rational Election Procedures

  4. Godel's Undecidability Theorem

  5. Coding Theory

  6. Arrangements With Forbidden Positions

  I chose chapter 2, Finite Markov Chains by Eric Rieders.

* Submission

  The project is due by April 24, 2020.
  The answers to the exercises should be submitted as
  =.pdf= or =.docx= files.
  The programs are to be submitted as =.txt= files.
  Each file should contain your name including your programs
  as comments.

* Topic Description

  Finite Markov Chains:
  A random process is any phenomenon which evolves in time and whose
  evolution is governed by some random (i.e. chance) mechanism.
  Questions relating to the likelihood that the gambler reaches a
  preset goal (before going broke), the response time of the computer,
  or the chance that one of your grandchildren will have green eyes
  all may be addressed by analyzing the underlyling random process.
  In this chapter, we will examine an important kind of random process
  known as a Markov chain.
  The foregoing examples of random processes may in fact be modeled
  as Markov chains.
  The basic property of a Markov chain is that in making the best
  possible predictions about the future behavior of the random process,
  given the information yielded by a sequence of observations up to the
  present, only the most recently observed outcome need be considered.

* Chapter Outline

** Introduction

*** Introduction

    There are probabilities can be determined using past experiences.
    These past experiences can be viewed as sequences of consecutive
    observations such that the sequence of outcomes cannot be
    predicted in advance.

*** Definition of a random process

    A *random process* is any phenomenon which evolves in time and
    whose evolution is governed by some random (i.e. chance) mechanism.

*** Basic properties of Markov chains

    /Markov chains/ are a memoryless random process.
    The basic property of a Markov chain is that in making the best
    possible predictions about the future behavior of the random
    process, given the information yielded by a sequence of
    observations up to the present, only the most recently observed
    outcome need be considered.
    Prior observations yield no additional information useful for the
    purposes of prediction.

*** Andrei Markov

    Markov chains were first studied systematically by the Russian
    mathematician Andrei Markov.
    In the course of his investigations in probability theory,
    Markov wished to extend the investigation of the properties of
    sequences of /independent/ experiments

** The State Space

*** States and state space

    Suppose that a sequence of "experiments" is to be performed and
    that each experiment will result in the observation of exactly
    one outcome from a finite set \(S\) of possible outcomes.
    The set \(S\) is called the *state space* associated with the
    experiments, and the elements of \(S\) are called *states*.

*** Notation of outcomes

    - The sequence of observed outcomes is denoted as
      \(X_1, X_2, \ldots\) (which presumably we do not know in advance).

    - The outcome of the experiment \(k\) is denoted as \(X_k\).
      Note that \(X_k \in S\).

    - The initial state is denoted \(X_0\).

*** Example 1 (Transition probability)

**** Description

     Suppose a gambler has $2.
     Each time he guesses the result of a flipped coin,
     he either wins $1 or loses $1 until he is broke
     or has $4.

     \(X_0 = 2\) is his initial state and \(X_k\) is his
     balance after \(k\) flips such that \(X_k \in S\)
     where \(S\) is the state space \(S = \{0,1,2,3,4\}\).

**** Solution

     #+begin_latex
     \begin{align*}
     p(X_2 = X_1 + 1) &= 0.5 \\
     p(X_2 = X_1 - 1) &= 0.5 \\
     p(X_k = j \mid X_{k - 1} = i) &= \begin{cases}
     1 &\text{if } i = j = 0 \text{ or } i = j = 4 \\
     \frac{1}{2} &\text{if } 0 \leq j = i - 1 \leq 3 \text{ or } 1 \leq j = i + 1 \leq 4 \\
     0 &\text{otherwise}
     \end{cases} \tag{1}
     \end{align*}
     #+end_latex

     where the gambler will have \(\$j\) after the first
     \((k + 1)\)st coin toss, given that the gambler had
     \(\$i\) after \(k\) tosses.

     The last probability is known as a *transition probability*.
     A transition probability is associated with each pair of
     states in the state space.
     It represents the probability of first state of the pair to
     move to the second state.

*** Example 2 (Initial probability distribution)

**** Description

     Suppose a gambler is given a choice of five envelopes
     containing \(0,1,\ldots,4\) dollars, respectively.
     Find the probability distribution of \(X_0\).

**** Solution

     Assuming the gambler is equally likely to choose any of the
     proffered envelopes, we have that \(p(X_0 = i) = 0.2, \; 0 \leq i \leq 4\).

     More generally, we might have

     \[p(X_0 = i - 1) = q_i, \quad 1 \leq i \leq 5\]

     where \(q_i\) is the probability that the gambler has \(i - 1\)
     dollars to start with, and \(\sum_{i = 1}^5 q_i = 1\).
     The \(1 \times 5\) matrix

     \[Q = \begin{pmatrix}q_1&q_2&q_3&q_4&q_5\end{pmatrix}\]

     is called the *initial probability distribution*.
     This describes the initial probability for each state
     in the state space \(S\) where \(S = \{s_1,s_2,s_3,s_4,s_5\}\).
     
*** Example 3 (Transition probabilities)

**** Description

     Suppose that we have 1000 seeds of a plant.
     The plant lives one year, bearing a flower
     that is either red, yellow, or orange.
     Before dying, it produces an offspring
     whose flower depends (randomly) on the
     flower of its parent.
     Use Table 1 to model the color of the flower
     during succeeding years as a Markov chain.

     #+begin_latex
     \begin{table}
     \begin{center}
     \begin{tabular}{cccccc}
     \multicolumn{3}{c}{} & \multicolumn{3}{c}{\textbf{Offspring}} \\
     \\ [-1.5ex]
     & & & Red & Yellow & Orange \\
     & & & \(s_1\) & \(s_2\) & \(s_3\) \\
     \multirow{3}{*}{\textbf{Parent}} & Red & \(s_1\) & 0.3 & 0.5 & 0.2 \\
     & Yellow & \(s_2\) & 0.4 & 0.4 & 0.2 \\
     & Orange & \(s_3\) & 0.5 & 0.5 & 0 \\
     \end{tabular}
     \caption{Transition Probabilities}
     \end{center}
     \end{table}
     #+end_latex

**** Solution

     \(S = \{\text{red}, \text{yellow}, \text{orange}\}\)

     For example, the quantity \(p_{1,2}\) is the
     probability of a transition from state \(s_1\)
     to state \(s_2\) (i.e., the probability that a
     red flower will produce a yellow flower)

*** Example 4 (Probability of future states)
    
**** Description
     
     In the preceding example, suppose you choose
     a random seed where there are 300, 400, and 300 seeds
     for red, yellow, and orange flowers, respectively.

**** Solution

     The initial probability distribution is

     \[Q = \begin{pmatrix}0.3 & 0.4 & 0.3\end{pmatrix}.\]

     Given \(X_0 = s_1\) (red) and \(X_1 = s_3\) (orange),
     the table tells us that \(P(X_1 = s_3 \mid X_0 = s_1) = 0.2\).
     Recall that

     \[p(X_1 = s_3 \mid X_0 = s_1) = \frac{p(X_0 = s_1, X_1 = s_3)}{p(X_0 = s_1)}\]

     where \(p(X_0 = s_1, X_1 = s_3)\) is the probability that
     the sequence of colors red, orange is observed in the first
     two years.
     It is the probability of the /intersection/ of the two events
     Rewriting this expresion, we then have

     \[\begin{align}
     p(X_0 = s_1, X_1 = s_3) &= p(X_0 = s_1) \cdot p(X_1 = s_3 \mid X_0 = s_1) \\
     &= 0.3 \cdot 0.2 \\
     &= 0.06
     \end{align}\]

     Using Theorem 1 and the fact that the random process
     is a Markov chain, we can deduce the probability of
     future states.

     Suppose \(X_0, X_1, X_2, \ldots\) are observations
     made of a random process (including the initial state)
     with state space \(S = \{s_1, s_2, \ldots, s_N\}\).
     We denote the probability of observing a particular
     sequence of states \(s_{i_0}, s_{i_1}, \ldots, s_{i_k}\),
     beginning with the initial one, by

     \[p(X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}).\]

     The (conditional) probability of observing the state
     \(s_{i_{k+1}}\) as the \((k + 1)\)st outcome, given that
     the first \(k\) outcomes observed are
     \(s_{i_0}, s_{i_1}, \ldots, s_{i_k}\), is given by

     \[\begin{align}
     p(X_{k+1} = s_{i_{k+1}} &\mid X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) = \\
     &\frac{p(X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}, X_{k + 1} = s_{i_{k+1}})}
     {p(X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k})}.
     \end{align}\]

*** Example 5 (Probability expression)

**** Description

    With reference to Example 1, find an expression which
    gives the probability that the gambler has $1 after
    2 rounds, given that the gambler started with $1 and
    then won the first round.

**** Solution

     We have \(s_{i_0} = 1\), \(s_{i_1} = 2\), and \(s_{i_2} = 1\).
     The desired probability is then given by

     \[p(X_2 = s_{i_2} \mid X_0 = s_{i_0}, X_1 = s_{i_1})\]

** Markov Chains

*** Definition 1 (Markov chain)

    #+begin_definition
    A random process with state space \(S = \{s_1, s_2, \ldots, s_N\}\)
    and observed outcomes \(X_0, X_1, X_2, \ldots\) is called a
    /Markov chain/ with initial probability distribution
    \(Q = \begin{pmatrix}q_1&q_2&\cdots&q_N\end{pmatrix}\) if
    
    \begin{equation}
    \begin{align}
    p(X_{k + 1} = s_{i_{k + 1}} \mid X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) \\
    = p(X_{k + 1} = s_{i_{k + 1}} \mid X_k = s_{i_k}) \quad \text{for } k = 1, 2, \ldots
    \end{align}
    \end{equation}

    \begin{equation}
    p(X_{k + 1} = s_j \mid X_k = s_i) = p_{ij} \quad \text{for } k = 0,1,2,\ldots
    \end{equation}

    \begin{equation}
    p(X_0 = i) = q_i \quad \text{for } i = 1,2,\ldots,N.
    \end{equation}
    #+end_definition

**** The Markov property

     The numbers \(p_{i,j}\) are the transition probabilities of
     the Markov chain.
     The *Markov property*, (i), says that we need only use the most
     recent information, namely, \(X_k = s_{i_k}\), to determine the
     probability of observing the state \(s_{i_{k + 1}}\) as the
     outcome of the experiment number \(k + 1\), given that the
     sequence of states \(s_{i_1},\ldots,s_{i_k}\) were the outcomes
     of the first \(k\) experiments (and the initial state is \(s_{i_0}\)).

**** Stationary transition probabilities

     Property (ii) requires that the underlying random mechanism
     governing the chance behavior of the random process does not
     change; the probability of moving from one particular state
     to another is always the same regardless of when this happens.
     (Sometimes Markov chains are defined to be random processes
     satisfying condition (i).
     Those that also satisfy (ii) are said to have
     *stationary transition probabilities.*)

**** Computing probabilities with an unknown initial state

     It might happen that the initial state of the random
     process is itself determined by chance, such as in
     Examples 2 and 3.
     To compute probabilities associated with a Markov chain
     whose initial state is unknown, one needs to know the
     probability of observing a particular state as the
     initial state.
     These are the probabilities given in (iii).
     Note that if the initial state is known, as in Example 1,
     we still can express this in terms of an initial distribution.
     For example, if the Markov chain is known to be initially in
     state \(s_k\), then we have \(q_k = 1\) and \(q_i = 0\) for
     \(i \neq k\).

**** Arrangement of transition probabilities

     It is very useful to arrange the transition probabilities
     \(p_{i,j}\) in an \(N \times N\) matrix \(T\)
     (\(N\) is the number of elements in the state space), so
     that the \((i,j)\)th entry will be \(p_{i,j}\).

*** Example 6 (Transition matrix)

**** Description

     Find the matrix of transition probabilities for the
     Markov chain of Example 3.

**** Solution

     \[T = \begin{bmatrix}
     0.3 & 0.5 & 0.2 \\
     0.4 & 0.4 & 0.2 \\
     0.5 & 0.5 & 0
     \end{bmatrix}\]
     
     For example, \(p_{1,2} = 0.5\), is the probability
     that a red flower will produce a yellow flower.
     Since it's impossible for an orange flower to
     produce an orange flower, \(p_{3,3} = 0\).

     We will show that knowning the transition probabilities
     \(p_{i,j}\) and the initial probability distribution
     \(Q\) of a Markov chain suffices for determining all
     probabilities of interest in connection with the Markov chain.
     Indeed, all such probabilities can be computed if we know the
     probability of observing any specific sequence of outcomes;
     any other events of interest are made up of such sequences.

*** Example 7 (Probability of a given sequence)

**** Description

     In Example 2, find the probability of starting out
     with 3 dollars, and losing the first two rounds,
     i.e., having 1 dollar left after flipping the coin twice.

**** Solution

     We must compute \(p(X_0 = 3, X_1 = 2, X_2 = 1)\).
     Using the definition of conditional probability, we have

     \[\begin{align}
     p(&X_0 = 3, X_1 = 2, X_2 = 1) \\
     &= p(X_0 = 3, X_1 = 2) \cdot p(X_2 = 1 \mid X_0 = 3, X_1 = 2) \\
     &= p(X_0 = 3) \cdot p(X_1 = 2 \mid X_0 = 3) \cdot p(X_2 = 1 \mid X_0 = 3, X_1 = 2) \\
     &= p(X_0 = 3) \cdot p(X_1 = 2 \mid X_0 = 3) \cdot p(X_2 = 1 \mid X_1 = 2) \\
     &= q_4 p_{4,3} p_{3,2} \\
     &= 0.05
     \end{align}\]
     
     Note that, for example, \(p_{4,3}\) is the probability of
     going from state \(s_4\), where the gambler has 3 dollars,
     to state \(s_3\), where the gambler has 2 dollars.

*** Theorem 1 (Probability of an observation)
    
    #+begin_theorem
    If \(X_0, X_1, \ldots, X_k\) denote the first \(k\)
    observed outcomes of the Markov chain with initial
    probability distribution
    \(Q = \begin{pmatrix}q1 & q2 & \cdots & q_N\end{pmatrix}\) then
    
    \[\begin{align}
    p(&X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) \\
    &= p(X_0 = s_{i_0}) \cdot p(X_1 = s_{i_1} \mid X_0 = s_{i_0}) \times \cdots \times p(X_k = s_{i_k} \mid X_{k-1} = s_{i_{k-1}}) \\
    &= q_{i_0} p_{i_0, i_1} p_{i_1, i_2} \cdots p_{i_{k-1}, i_k}
    \end{align}\]
    #+end_theorem

*** Theorem 1 Proof

    #+begin_proof
    We will demonstrate this using mathematical induction.

    /Basis step:/ The case \(k = 1\) is an immediate consequnce
    of the definition of conditional probability.

    /Induction step:/ Assuming the truth of the result for some
    particular \(k\), we must then deduce its truth for \(k + 1\).
    We have
    
    \[\begin{align}
    p(&X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}, X_{k+1} = s_{i_{k+1}}) \\
    &= p(X_{k+1} = s_{i_{k+1}} \mid X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) \\
    &\qquad \times p(X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) \\
    &= p(X_{k+1} = s_{i_{k+1}} \mid X_0 = s_{i_0}, X_1 = s_{i_1}, \ldots, X_k = s_{i_k}) \\
    &\qquad p(X_0 = i_0) \cdot p(X_1 = s_{i_1} \mid X_0 = s_{i_0}) \times \cdots \times p(X_k = s_{i_k} \mid X_{k-1} = s_{i_{k-1}}) \\
    &= p(X_{k+1} = s_{i_{k+1}} \mid X_k = s_{i_k}) \cdot p(X_0 = i_0) \cdot p(X_1 = s_{i_1} \mid X_0 = s_{i_0}) \times \cdots \\
    &\qquad \times p(X_k = s_{i_k} \mid X_{k-1} = s_{i_{k-1}}),
    \end{align}\]

    which is the result.
    (The definition of conditional probability was used at the
    first step, the induction assumption at the second step, and
    property (i) at the third step.)
    #+end_proof

*** Example 8 (Apply Theorem 1 on Example 1)

**** Description

     Compute the probability of the sequence of outcomes
     described in Example 1
     \((X_0, = 2, X_1 = 3, X_2 = 2, X_3 = 1, X_4 = 0)\)
     using Theorem 1.

**** Solution

     Note that \(q_3 = 1\) (state \(s_3\) occurs when the gambler
     has $2), since we are certain that we begin with $2.
     We have

     \[\begin{align}
     p(X_0 = 2, X_1 = 3, X_2 = 2, X_3 = 1, X_4 = 0) &= q_3 p_{3,4} p_{4,3} p_{3,2} p_{2,1} \\
     &= 1 \cdot 0.5 \cdot 0.5 \cdot 0.5 \cdot 0.5 \\
     &= 0.0625.
     \end{align}\]

*** Example 9 (Apply Theorem 1 on Example 4)

**** Description

     With reference to Example 4, what is the probability that,
     having planted one of the seeds (chosen at random), we
     observe the sequence of flower colors "red, red, yellow, red"
     over the first four years?

**** Solution

     \[\begin{align}
     p(X_0 = s_1, X_1 = s_1, X_2 = s_2, X_3 = s_1) &= q_1 p_{1,1} p_{1,2} p_{2,1} \\
     &= 0.3 \cdot 0.3 \cdot 0.5 \cdot 0.4 \\
     &= 0.018.
     \end{align}\]

*** Example 10 (Example 9 Extended)

**** Description

     Referring again to Example 4, compute the probability that
     after having planted a randomly chosen seed, we must wait
     3 years for the first /red/ flower.

**** Solution

     This means that one of the following sequences of outcomes
     must have occurred:

     \[s_2 s_2 s_1 \quad s_2 s_3 s_1 \quad s_3 s_2 s_1 \quad s_3 s_3 s_1.\]

     As in Example 9, we can use Theorem 1 to compute the
     probabilities of observing these sequences of outcomes;
     they are (respectively)

     \[0.064, 0.04, 0.06, 0.\]

     Thus, the "event" that the first red flower is observed
     during the third year is the sum of these, 0.164.
     Notice that knowledge of the initial distribution is
     essential for computing the desired probability.
     In this computation, we wish to suggest how knowledge of
     the probability that a particular sequence is observed
     allows one to compute the probability of a particular event
     relation to the random process, by simply adding up the
     probabilities of all the sequences of outcomes which give
     rise to the event.

** Long-term Behavior

*** Introduction

    By observing random processes in the long-term, we can find
    that probabilities tend to certain equilibrium values.

    Examples 1 and 3 are very different in their long-term behavior.
    The gambler's long-term chances highly depend on the initial
    distribution whereas Example 3 is not affected at all by it.

    We first determine the conditional probability that the random
    process is in state \(s_j\) after \(k\) experiments, given that
    it ws initially in state \(s_i\), i.e., we compute
    \(p(X_k = s_j \mid X_0 = s_i)\).
    Theorem 2 shows that these probabilities may be found by
    computing the appropriate power of the matrix of transition
    probabilities.

*** Theorem 2 (Transition matrix exponentiation)

    #+begin_theorem
    Let \(T = [p_{i,j}]\) be the \(N \times N\) matrix whose
    \((i, j)\)th entry is the probability of moving from state
    \(s_i\) to state \(s_j\).
    Then

    \[T^k = [p_{i,j}^{(k)}],\]

    where \(p_{i,j}^{(k)} = p(X_k = s_j \mid X_0 = s_i)\).
    #+end_theorem

    #+begin_remark
    Note that \(p_{i,j}^{(k)}\) is /not/ the \(k\)th
    power of the quantity \(p_{i,j}\).
    It is rather the \((i,j)\)th entry of the matrix \(T^k\).
    #+end_remark

*** Theorem 2 Proof

    #+begin_proof
    We will prove the Theorem for the case \(n = 2\).
    (The case \(n = 1\) is immediate from the definition of \(T\).)
    The general proof may be obtained using mathematical induction
    (see Exercise 5).
    First, note that

    \[p(X_2 = s_j, X_0 = s_i) = \sum_{n = 1}^N p(X_2 = s_j, X_1 = s_n, X_0 = s_i)\]

    since \(X_1\) must equal exactly one of the elements of the
    state space.
    That is, the \(N\) events
    \(\{X_2 = s_j, X_1 = s_n, X_0 = s_i\}, (1 \leq n \leq N)\),
    are disjoint events whose union is \(\{X_2 = s_j, X_0 = s_i\}\).
    Using this fact, we compute

    \begin{align*}
    p_{i,j}^{(2)} &= p(X_2 = s_j \mid X_0 = s_i) \\
    &= \frac{p(X_2 = s_j, X_0 = s_i)}{p(X_0 = s_i)} \\
    &= \sum_{n = 1}^N \frac{p(X_2 = s_j, X_1 = s_n, X_0 = s_i)}{p(X_0 = s_i)} \tag{addition rule for probabilities} \\
    &= \sum_{n = 1}^N p(X_2 = s_j \mid X_1 = s_n) \frac{p(X_1 = s_n, X_0 = s_i)}{p(X_0 = s_i)} \tag{by property (i)} \\
    &= \sum_{n = 1}^N p(X_2 = s_j \mid X_1 = s_n) \cdot p(X_1 = s_n \mid X_0 = s_i) \\
    &= \sum_{n = 1}^N p_{i,n} p_{n,j}. \tag{by property (ii)}
    \end{align*}

    This last expression is the \((i, j)\)th entry of the matrix
    \(T^2\).
    #+end_proof

*** Example 11 (Apply Theorem 2 on Example 1)

**** Description

     What happens in the long run to the gambler in Example 1?
     In particular, give the long term probabilities of the
     gambler either reaching the $4 goal or going broke.

**** Solution

     From (1), the matrix of transition probabilities is

     #+attr_latex: :mode math :environment bmatrix :math-prefix T =
     |   1 |   0 |   0 |   0 |   0 |
     | 0.5 |   0 | 0.5 |   0 |   0 |
     |   0 | 0.5 |   0 | 0.5 |   0 |
     |   0 |   0 | 0.5 |   0 | 0.5 |
     |   0 |   0 |   0 |   0 | 1   |

     According to Theorem 2, computing the powers of \(T\) will
     provide the probabilities of finding the gambler in specific
     states, given the gambler's initial state.

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^2 =
     |    1 |    0 |   0 |    0 |    0 |
     |  0.5 | 0.25 |   0 |  0.5 |    0 |
     | 0.25 |    0 | 0.5 |    0 | 0.25 |
     |    0 | 0.25 |   0 | 0.25 |  0.5 |
     |    0 |    0 |   0 |    0 |    1 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^3 =
     |     1 |    0 |    0 |    0 |     0 |
     | 0.625 |    0 | 0.25 |    0 | 0.125 |
     |  0.25 | 0.25 |    0 | 0.25 |  0.25 |
     | 0.125 |    0 | 0.25 |    0 | 0.625 |
     |     0 |    0 |    0 |    0 |     1 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^{20} =
     |     1 |     0 |     0 |     0 |     0 |
     | 0.749 |     0 | 0.001 |     0 | 0.250 |
     | 0.499 | 0.001 |     0 | 0.001 | 0.499 |
     | 0.250 |     0 | 0.001 |     0 | 0.749 |
     |     0 |     0 |     0 |     0 |     1 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^{20} =
     |     1 | 0 | 0 | 0 |     0 |
     | 0.750 | 0 | 0 | 0 | 0.250 |
     | 0.500 | 0 | 0 | 0 | 0.500 |
     | 0.250 | 0 | 0 | 0 | 0.750 |
     |     0 | 0 | 0 | 0 |     1 |

     (We have rounded the entries to three decimal places.)
     The matrices \(T^k\) for \(k > 50\) will be the same as
     \(T^{50}\) (at least up to 3 decimal place accuracy).
     According to Theorem 2, the probability of the gambler
     going broke after three rounds, given that he starts
     with $1 is the entry of \(T^3\) in the second row and
     first column, 0.625.
     If we want to know the probability that, starting with $2,
     we will have $3 after 20 rounds (i.e. we want to compute
     \(p_{3,4}^{(20)}\), we need only look at the entry in the
     third row and fourth column of the matrix \(T^{20}\), which
     is 0.001.
     Notice that the probability of reaching the goal of $4
     (after many rounds) depends upon the state in which the
     gambler is initially observed.
     Thus, it is with probability 0.75 that the goal is reached
     if the gambler starts with $3, 0.5 if he starts with $2 and
     0.25 if he starts with $1.
     These are the probabilities that comprise the fifth column
     of the matrix \(T^k\), for large \(k\) (\(k = 50\), say).
     Similar statements concerning the chances of the gambler
     eventually going broke can easily be made.
     
     The following result may be deduced as a corollary of Thereom 2.
     It describes how to compute the probability that the Markov
     chain will be any particular state after \(k\) experiments, thus
     allowing one to make predictions about the future behavior of
     the Markov chain.

*** Corollary 1 (Future probability distribution)

    #+begin_corollary
    Suppose that a Markov chain has initial probability distribution
    \(Q\) and \(N \times N\) matrix \(T\) of transition probabilities.
    Then the \(1 \times N\) matrix \(QT^k\), denoted \(Q_k\), has the
    \(i\)th entry the probability of observing the Markov chain in
    state \(s_i\) after \(k\) experiments.
    #+end_corollary

*** Example 12 (Long-term behavior of Example 1)

**** Description

     Suppose now that the gambler initially gets to pick one
     of ten envelopes, 6 of them containing $1, and the other
     four, $2.
     Find the probabilities of the gambler being each possible
     state after one round of gambling, and estimate the
     long-term chances of the gambler reaching the $4 goal.

**** Solution

     The initial probability distribution is

     #+attr_latex: :mode math :environment pmatrix
     | 0 | 0.6 | 0.4 | 0 | 0 |

     i.e., the gambler has a probability of 0.6 of starting with
     $1 and 0.4 of starting with $2.
     By Corollary 1,

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q_1 = QT = 
     | 0.2 | 0.3 | 0.2 | 0.3 | 0 |

     This means that after the first round of gambling, the
     probabilities that the gambler has 0,1,2,3 or $4 are,
     respectively, 0.2, 0.3, 0.2, 0.3, 0.
     The probability is 0 that the goal of $4 has been reached.
     
     We have observed that for large \(k\), \(T^{(k)}\) will be
     almost exactly equal to \(T^{(50)}\).
     This means that the long-term chances of the gambler reaching
     the $4 goal (or going broke) are approximately equal to the
     probabilities of being in these states after 50 coin tosses.
     These probabilities will depend on the initial distribution.
     Using Corollary 1, if
     
     #+attr_latex: :mode math :environment pmatrix :math-prefix Q = 
     | 0 | 0.6 | 0.4 | 0 | 0 |

     we obtain

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q_{50} = 
     | 0.6 | 0 | 0 | 0 | 0.4 |
     
     This means that with this initial distribution, the gambler's
     long-term chances of going broke are 0.6 and are 0.4 of
     reaching the $4 goal.

     In the previous example, if the initial distribution is
     changed to

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q = 
     | 0 | 0 | 0.4 | 0.6 | 0 |

     then we find that

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q_{50} = 
     | 0.35 | 0 | 0 | 0 | 0.65 |
     
     and if the initial distribution were

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q = 
     | 0 | 0.4 | 0 | 0.6 | 0 |
     
     then

     #+attr_latex: :mode math :environment pmatrix :math-prefix Q_{50} = 
     | 0.45 | 0 | 0 | 0 | 0.55 |

     Thus, we see that the probability of this Markov chain after
     many rounds of play depends very much on its initial distribution.
     This should be contrasted to the behavior of the Markov chain
     of Example 3 (see Example 13).

     The entries in the matrices \(T^{20}\) and \(T^{50}\) suggest
     that in the long run the probability is (essentially) 0 that
     the gambler will not have either gone broke or reached his goal,
     /irrespective/ of his initial state.
     In this setting, this means that the game cannot last
     indefinitely; eventually, the gambler will reach $4 or
     go broke trying.
     
*** Example 13 (Long-term behavior of Example 3)

**** Description

     Analyze the long-term behavior of the Markov chain of Example 3.

**** Solution

     #+attr_latex: :mode math :environment bmatrix :math-prefix T = 
     | 0.3 | 0.5 | 0.2 |
     | 0.4 | 0.4 | 0.2 |
     | 0.5 | 0.5 |   0 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^2 = 
     | 0.39 | 0.45 | 0.16 |
     | 0.38 | 0.46 | 0.16 |
     | 0.35 | 0.45 |  0.2 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^3 = 
     | 0.377 | 0.455 | 0.168 |
     | 0.378 | 0.454 | 0.168 |
     | 0.385 | 0.455 | 0.160 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^4 = 
     | 0.379 | 0.455 | 0.166 |
     | 0.379 | 0.455 | 0.166 |
     | 0.378 | 0.455 | 0.168 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^{19} = 
     | 0.379 | 0.454 | 0.167 |
     | 0.379 | 0.454 | 0.167 |
     | 0.379 | 0.454 | 0.167 |

     #+attr_latex: :mode math :environment bmatrix :math-prefix T^{20} = 
     | 0.379 | 0.454 | 0.167 |
     | 0.379 | 0.454 | 0.167 |
     | 0.379 | 0.454 | 0.167 |
     
     In this case, the Markov chain "settles down" in the sense
     that the probability of observing a particular state remains
     essentially constant after an initial period of time.
     
     The states of a Markov chain are typically classified according
     to a criterion suggested by the last two examples.

*** Definition 2 (Definition of recurrent and transient)

    #+begin_definition
    A state \(s_i\) of a Markov chain is called /recurrent/ if,
    given that the chain is in this state at some time, it will
    return to this state infinitely often with probability 1, i.e.

    \[p(X_n = s_i \text{ for infinitely many } n > k \mid X_k = s_i) = 1.\]

    States that are not recurrent are called *transient*; these
    are the states which will not be observed after enough
    experiments have been performed.
    #+end_definition

**** Definition 2 in the Examples

    None of the states in Example 3 are transient, whereas in
    Example 1, states \(s_2\), \(s_3\), and \(s_4\) are transient.

    In Example 1 there are two states (\(s_1\) and \(s_5\))
    which are recurrent, and the remaining three are transient.
    The probability of reaching a particular recurrent state
    depends upon the initial state of the random process.

    In Example 2, all states are recurrent.
    The transition probabilities remain the same after
    enough experiments, regardless of the initial state.

    The random process of Example 3 belongs to a special
    class of Markov chain which we now define (Definition 3).

*** Definition 3 (Regular Markov chain)

    #+begin_definition
    A Markov chain with matrix of transition probabilities \(T\)
    is called /regular/ if for some \(k\), \(T^k\) contains all
    positive entries.
    #+end_definition

    Regular Markov chains have quite different long-term behavior
    than those with transient states.
    Irrespective of the initial distribution \(Q\), the entries of
    \(Q_k\) tend toward specific values as \(k\) gets large,
    resulting in \(Q_k\) becoming an /equilibrium distribution/,
    denoted as \(Q_e\).

*** Definition 4 (Equilibrium distribution)

    #+begin_definition
    A Markov chain with matrix \(T\) of transition probabilities
    is said to have an /equilibrium distribution/ \(Q_e\)
    if \(Q_e T = Q_e\).
    #+end_definition

    Note that if \(Q_e T = Q_e\), then

    \[Q_e T^2 = (Q_e T) T = Q_e T = Q_e\]

    and in general (by the principle of mathematical induction),

    \[Q_e T^k = Q_e\]

    for all positive integers \(k\).
    A regular Markov chain will only have one \(Q_e\).

*** Theorem 3 (Unique equilbrium distribution)
    
    #+begin_theorem
    Every regular Markov chain has a unique equilibrium distribution.

    Thus, the analysis of the long-term behavior of regular Markov
    chains can always be carried out by computing the equilibrium
    distribution.
    #+end_theorem
