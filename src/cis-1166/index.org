#+title: CIS 1166 (Mathematical Concepts in Computing I)
#+subtitle: Section 007

- [[file:connect.org][Connect]]

- [[file:notes.org][Lecture Notes]]

- [[file:syllabus.org][Syllabus]]

- [[file:timeline.org][Timeline]]
